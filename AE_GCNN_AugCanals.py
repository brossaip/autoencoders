# Vull provar d'augmentar els bits augmentant els canals en comptes de combinacions lineals
# He partit del AE_GCNN.py

# Third Party
import torch
import torch.nn as nn
from torch.nn import BCELoss
import math

############
# COMPONENTS
############


class Encoder(nn.Module):
    # He fet pel cas 9/27
    def __init__(self):
        super(Encoder, self).__init__()

        # La convolució ha de ser amb diferents canals i la longitud L (N, C_in, L_in) on N és el tamany de batch, C_in són els canals d'entrada i L_in la longitud. A la fórmula de la documentació no veig L, però sí C_in i C_out, L potser està en la convolució del pes per l'input.
        self.cnn1a = nn.Conv1d(
            in_channels=1,
            out_channels=3,
            kernel_size=2,  # Kernel_size can't be greater than input size
            stride=1)
        self.cnn1b = nn.Conv1d(in_channels=1,
                               out_channels=3,
                               kernel_size=2,
                               stride=1)
        self.cnn2a = nn.Conv1d(in_channels=3,
                               out_channels=6,
                               kernel_size=3,
                               stride=1)
        self.cnn2b = nn.Conv1d(in_channels=3,
                               out_channels=6,
                               kernel_size=3,
                               stride=1)
        self.cnn3a = nn.Conv1d(in_channels=6,
                               out_channels=3,
                               kernel_size=2,
                               stride=1)
        self.cnn3b = nn.Conv1d(in_channels=6,
                               out_channels=3,
                               kernel_size=2,
                               stride=1)
        # L_out = \frac{L_in + 2*padding -dilation*(kernel_size-1)-1}{stride} + 1
        # L_out del primer és 9-2+1-1+1=8, segon és 8-3+1=6 i el tercer és 5.
        # Com que la meva xarxa no és deep, no afegeixo el residual que diuen
        self.lin2 = nn.Linear(15, 27)

    def forward(self, x):
        # He de provar algun dia que en lloc de concatenar ho utilitzi com a 3 canals
        x1 = self.cnn3a(self.cnn2a(self.cnn1a(x)))
        x2 = self.cnn3b(self.cnn2b(self.cnn1b(x)))  # Longitud és 5
        x2 = torch.sigmoid(x2)
        x = torch.mul(x1, x2)
        x = x.view(x1.shape[0],1,x1.shape[1]*x1.shape[2])
        x = self.lin2(x)
        return (x)


class Decoder(nn.Module):
    def __init__(self):
        super(Decoder, self).__init__()

        self.cnn1a = nn.Conv1d(
            in_channels=3,
            out_channels=6,
            kernel_size=2,  # Kernel_size can't be greater than input size
            stride=1)
        self.cnn1b = nn.Conv1d(in_channels=3,
                                        out_channels=6,
                                        kernel_size=2,
                                        stride=1)
        self.cnn2a = nn.Conv1d(in_channels=6,
                                        out_channels=3,
                                        kernel_size=3,
                                        stride=1)
        self.cnn2b = nn.Conv1d(in_channels=6,
                                        out_channels=3,
                                        kernel_size=3,
                                        stride=1)
        self.cnn3a = nn.Conv1d(in_channels=3,
                                        out_channels=1,
                                        kernel_size=2,
                                        stride=1)
        self.cnn3b = nn.Conv1d(in_channels=3,
                                        out_channels=1,
                                        kernel_size=2,
                                        stride=1)

        self.lin1 = nn.Linear(5, 9)

    def forward(self, x):
        x = x.view(x.shape[0],3,int(x.shape[2]/3))
        x1 = self.cnn3a(self.cnn2a(self.cnn1a(x)))
        x2 = self.cnn3b(self.cnn2b(self.cnn1b(x)))  # Longitud és x.shape[2]/3-4 = 9-4 = 5
        x2 = torch.sigmoid(x2)
        x = torch.mul(x1, x2)
        x = self.lin1(x)
        x = torch.sigmoid(10 * x)
        return x


class AutoEncoder(nn.Module):
    def __init__(self):
        super(AutoEncoder, self).__init__()

        self.enc = Encoder()
        self.dec = Decoder()

        bits_entrada = 9
        bits_sortida = 27
        Rm = math.log2(2)
        Rc = bits_entrada * 1.0 / bits_sortida
        EbN0 = 10**(2 / 10)
        self.noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

    def forward(self, x):
        mig = self.enc(x)
        mig = 2 * torch.sigmoid(100 * mig) - 1
        # Aquest factor de 100 l'hauria de fer més flexible, hauria de calcular el mínim valor de x i fer-ho adaptable, ja que si tot ja són propers a 1 no cal aquest factor.
        mig_noise = mig + self.noiseSigma * torch.randn(mig.shape)
        x = self.dec(mig_noise.float())
        return x, mig, mig_noise


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')

    bits_entrada = 9
    bits_mig = 27

    torch.manual_seed(0)
    # mod_enc = Encoder(bits_entrada, 6)
    # mod_enc.to(device)
    # mod_dec = Decoder(6, num_etiquetes)
    # mod_dec.to(device)
    autoenc = AutoEncoder()
    num_etiquetes = 2**bits_entrada
    #optim_enc = torch.optim.SGD(mod_enc.parameters(), lr=1e-3, weight_decay=1e-5)
    #optim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-5)
    optim_auto = torch.optim.SGD(autoenc.parameters(),
                                 lr=1e-3,
                                 weight_decay=1e-5)

    criterion = BCELoss()
    loss_detall = BCELoss(reduction='none')

    ordre = list(range(num_etiquetes))
    # Per cada etiqueta he de fer el seu vector d'entrada
    entrada = []
    for element in ordre:
        st = ''
        # He d'afegir tants 0 al davant com longitud
        for k in format(element, "b").zfill(bits_entrada):
            st = st + k + ','
        entrada.append(eval('[' + st + ']'))
    entrada_torch = torch.Tensor(entrada) * 2 - 1
    entrada_torch = entrada_torch.view([num_etiquetes, 1, bits_entrada])

    f = open("debugant.log", "w")
    for epoch in range(167500):
        autoenc.train()

        if (epoch % 50) == 0:
            f.write("\n**************\nEpoch: " + str(epoch) +
                    "\n**************\n")

        # optim_enc.zero_grad()
        # optim_dec.zero_grad()
        optim_auto.zero_grad()

        seq_true = (entrada_torch.view(num_etiquetes, 9) + 1) / 2
        seq_pred, mig, mig_noise = autoenc(entrada_torch)

        loss = criterion(seq_pred.view(num_etiquetes, 9), seq_true)
        # L'entrada al BCE és (N,*) on N és el tamany del batch

        loss.backward()
        # optim_enc.step()
        # optim_dec.step()
        optim_auto.step()

        if (epoch % 50) == 0:
            f.write("\n***\nSeq. entrada: " + str(seq_true.detach().numpy()))
            f.write("\nMig: " + str(mig.detach().numpy()))
            f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            f.write("\nSeq. predita: " + str(seq_pred.detach().numpy()))
            perdua = loss_detall(seq_pred.view(num_etiquetes, 9), seq_true)
            f.write("\nPèrdua: " + str(perdua))
            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:' +
                        str(round(torch.cuda.memory_allocated(0) /
                                  1024**3, 1)) + 'GB')
                f.write('\nCached:   ' +
                        str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                        'GB')

            print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
    f.close()

    torch.save(
        {
            'epoch': epoch,
            'model_state_dict': autoenc.state_dict(),
            'optimizer_state_dict': optim_auto.state_dict(),
            'loss': loss,
            'bits_entrada': bits_entrada,
            'bits_mig': bits_mig,
        }, "AutoEnc_conc.pth")
