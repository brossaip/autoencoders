# Programa que calcula el Word Error Rate per un Repeat Code
# També calculem el BER
# Paraula de 3
import torch
import math

input = torch.tensor([[-1., -1., -1.,-1., -1., -1.,-1., -1., -1.],
                      [-1., 1., -1.,-1., 1., -1.,-1., 1., -1.],
                      [1., -1., -1.,1., -1., -1.,1., -1., -1.],
                      [1., 1., -1.,1., 1., -1.,1., 1., -1.],
                      [-1., -1., 1.,-1., -1., 1.,-1., -1., 1.],
                      [-1., 1., 1.,-1., 1., 1.,-1., 1., 1.],
                      [1., -1., 1.,1., -1., 1.,1., -1., 1.],
                      [1., 1., 1.,1., 1., 1.,1., 1., 1.]])

bits_entrada = 3
bits_sortida = 9
Rm = math.log2(2)
Rc = bits_entrada * 1.0 / bits_sortida
EbN0 = 10**(2 / 10)
noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

errors = 0
errors_bit = 0
total = 0
for epoch in range(10000):
    entrada = input + noiseSigma * torch.randn(input.shape)

    rebuda = (entrada>0)*2-1
    # Corregim l'error de la repetició
    # per cadascuna he de fer per fileres i dins la filera cada 3 i comparar després amb els 3 primers
    fileres, columnes = input.shape

    for fil in range(fileres):
        pos_0 = 0
        pos_1 = 0
        pos_2 = 0
        for col in range(3):
            pos_0 = pos_0 + rebuda[fil][3*col]
            pos_1 = pos_1 + rebuda[fil][3*col+1]
            pos_2 = pos_2 + rebuda[fil][3*col+2]
        # comptem errors
        orig = input[fil][0:3]
        reb = torch.tensor([pos_0,pos_1,pos_2])
        reb_corregit = (reb>0)*2-1
        total = total + 1
        if sum(orig == reb_corregit) != 3:
            errors = errors + 1
        errors_bit = errors_bit + sum(orig!=reb_corregit)
print("Errors: " + str(errors) + "; Total: " + str(total) + "; %: " + str(errors/total))
print("BER: " + str(errors_bit) + "; Total: " + str(total*3) + "; %: " + str(errors_bit.detach().numpy()/(total*3)))
