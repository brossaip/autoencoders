# Vull provar que si repeteixo molt els codis inicials, després puc basar-me en una reducció de dimensionalitat com fan els autoencoders per reconstruir el gran repeat code i poder corregir els errors

# Third Party
import torch
import torch.nn as nn
from torch.nn import BCELoss
import math


############
# COMPONENTS
############
def distancia(codi):
    maxi = 0
    pos_max = []
    mini = 1e9
    pos_min = []
    for k in range(len(codi)):
        for j in range(len(codi) - k - 1):
            dist = torch.sum(torch.abs(codi[k] - codi[j + k + 1]))
            dist = dist.detach().numpy()
            if dist > maxi:
                maxi = dist
                pos_max = [k, j + k + 1]
            if dist < mini:
                mini = dist
                pos_min = [k, j + k + 1]

    return ([maxi, pos_max, mini, pos_min])


class Encoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida):
        super(Encoder, self).__init__()

        self.bits_entrada = bits_entrada
        self.bits_sortida = bits_sortida
        self.relu = nn.ReLU()

        self.conv1 = nn.Conv2d(1,
                               1,
                               kernel_size=3,
                               stride=1,
                               padding=1,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(
            num_features=1)  # num_features=C on (N,C,H,W)
        self.conv2 = nn.Conv2d(1,
                               1,
                               kernel_size=3,
                               stride=2,
                               padding=1,
                               bias=False)
        self.bn2 = nn.BatchNorm2d(1)
        self.conv3 = nn.Conv2d(1,
                               1,
                               kernel_size=3,
                               stride=1,
                               padding=1,
                               bias=False)
        self.bn3 = nn.BatchNorm2d(1)
        self.conv4 = nn.Conv2d(1,
                               1,
                               kernel_size=3,
                               stride=2,
                               padding=1,
                               bias=False)
        self.bn4 = nn.BatchNorm2d(1)
        self.actcnv = nn.Conv1d(1, 1, 1, bias=False)
        self.actcnv.weight.data = torch.Tensor([[[1]]])
        self.lin_fin = nn.Linear(48, bits_sortida)

    def forward(self, x):
        # Aquí entrarà el supervector que farem en forma de mtariu 2D per entrar en el nostre procés
        # Així el BCELoss serà del supervector que és el que jo vull mirar de recuperar bé per poder tenir menys errors.
        # Per poder posar el BCELoss de paraula curta, hauria de fer un descodificador que permetès derivar i quan binaritzava trencava el gradient
        x_orig = x.clone()
        # print("**** x-enc: " + str(x_origc.shape))
        x_conc4D = x.view([x.shape[0], x.shape[1], 3 * 16, 16])
        conv1 = self.relu(self.bn1(self.conv1(x_conc4D)))
        conv2 = self.relu(self.bn2(self.conv2(conv1)))
        conv3 = self.relu(self.bn3(self.conv3(conv2)))
        conv4 = self.relu(self.bn4(self.conv4(conv3)))
        # print("**** conv4: " + str(conv4.shape))
        conv4 = conv4.view(
            [conv4.shape[0], conv4.shape[1], conv4.shape[2] * conv4.shape[3]])
        x = conv4 + self.actcnv(torch.narrow(x_orig,2,0,48))
        x = self.lin_fin(conv4)
        return (x)


class Decoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida):
        super(Decoder, self).__init__()

        # Els bits entrada i sortida són de més gran a més petit, al revés que l'encoder
        self.bits_entrada = bits_entrada
        self.bits_sortida = bits_sortida

        self.relu = nn.ReLU()
        self.lin_ini = nn.Linear(bits_entrada, 48)
        self.convTranspose1 = nn.ConvTranspose2d(1,
                                                 1,
                                                 kernel_size=3,
                                                 stride=1,
                                                 padding=1,
                                                 bias=False)
        self.bn1 = nn.BatchNorm2d(
            num_features=1)  # num_features=C on (N,C,H,W)
        self.convTranspose2 = nn.ConvTranspose2d(1,
                                                 1,
                                                 kernel_size=3,
                                                 stride=2,
                                                 padding=1,
                                                 bias=False)
        self.bn2 = nn.BatchNorm2d(1)
        self.convTranspose3 = nn.ConvTranspose2d(1,
                                                 1,
                                                 kernel_size=3,
                                                 stride=1,
                                                 padding=1,
                                                 bias=False)
        self.bn3 = nn.BatchNorm2d(1)
        self.convTranspose4 = nn.ConvTranspose2d(1,
                                                 1,
                                                 kernel_size=3,
                                                 stride=2,
                                                 padding=1,
                                                 bias=False)
        self.bn4 = nn.BatchNorm2d(1)
        self.lin_fin = nn.Linear(45 * 13, 16 * 16 * 3)

    def forward(self, x):
        x = self.lin_ini(x)
        x = x.view([x.shape[0], x.shape[1], 12, 4])
        convTranspose1 = self.relu(self.bn1(self.convTranspose1(x)))
        convTranspose2 = self.relu(
            self.bn2(self.convTranspose2(convTranspose1)))
        convTranspose3 = self.relu(
            self.bn3(self.convTranspose3(convTranspose2)))
        convTranspose4 = self.relu(
            self.bn4(self.convTranspose4(convTranspose3)))
        # print("**** convTranspose4: " + str(convTranspose4.shape))
        x = self.lin_fin(
            convTranspose4.view([
                convTranspose4.shape[0], convTranspose4.shape[1],
                convTranspose4.shape[2] * convTranspose4.shape[3]
            ]))
        x = torch.sigmoid(10 * x)
        return (x)


class AutoEncoder(nn.Module):
    def __init__(self, bits_entrada, bits_mig):
        super(AutoEncoder, self).__init__()

        self.enc = Encoder(bits_entrada, bits_mig)
        self.dec = Decoder(bits_mig, bits_entrada)

        #bits_entrada = 3
        #bits_sortida = 9
        Rm = math.log2(2)
        Rc = bits_entrada * 1.0 / bits_mig
        EbN0 = 10**(2 / 10)
        self.noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

    def forward(self, x):
        mig = self.enc(x)
        mig = 2 * torch.sigmoid(100 * mig).to(device) - 1
        # Aquest factor de 100 l'hauria de fer més flexible, hauria de calcular el mínim valor de x i fer-ho adaptable, ja que si tot ja són propers a 1 no cal aquest factor.
        mig_noise = mig + self.noiseSigma * torch.randn(mig.shape).to(device)
        mig = (mig+1)/2
        x = self.dec(mig_noise.float())
        return x, mig, mig_noise


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')

    bits_entrada = 3
    bits_mig = 9
    forma2D = 16

    torch.manual_seed(0)
    # mod_enc = Encoder(bits_entrada, 6)
    # mod_enc.to(device)
    # mod_dec = Decoder(6, num_etiquetes)
    # mod_dec.to(device)
    autoenc = AutoEncoder(bits_entrada, bits_mig).to(device)

    num_etiquetes = 2**bits_entrada
    #optim_enc = torch.optim.SGD(mod_enc.parameters(), lr=1e-3, weight_decay=1e-5)
    #optim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-5)
    optim_auto = torch.optim.SGD(autoenc.parameters(),
                                 lr=1e-3,
                                 weight_decay=1e-5)

    criterion = BCELoss()
    loss_detall = BCELoss(reduction='none')

    ordre = list(range(num_etiquetes))
    # Per cada etiqueta he de fer el seu vector d'entrada
    entrada = []
    for element in ordre:
        st = ''
        # He d'afegir tants 0 al davant com longitud
        for k in format(element, "b").zfill(bits_entrada):
            st = st + k + ','
        entrada.append(eval('[' + st + ']'))
    seq_true = torch.Tensor(entrada).to(device)
    entrada_torch = torch.Tensor(entrada) * 2 - 1 # TRactaré sempre 0 i 1 menys a l'afegir al soroll que l'escalaré per ser coherent l'EbN0. Però si intento forçar un repeat code inicial, amb el 0 és el neutre i no varia. Hauré de tornar a posar-lo simètric a 0
    entrada_torch = seq_true.view([num_etiquetes, 1, bits_entrada])
    # Creem la matriu del supervector
    entrada_torch = torch.cat([entrada_torch] * (forma2D * forma2D), dim=2).to(device)

    f = open("debugant.log", "w")
    for epoch in range(7500):
        autoenc.train()

        if (epoch % 50) == 0:
            f.write("\n**************\nEpoch: " + str(epoch) +
                    "\n**************\n")

        # optim_enc.zero_grad()
        # optim_dec.zero_grad()
        optim_auto.zero_grad()

        seq_pred, mig, mig_noise = autoenc(entrada_torch)

        # Ara he de passar de la seq_pred que conté un supervector a la descodificació d'un repeat code de només 3 bits
        # yapf: disable
        # print("*** " + str(seq_true.shape) + " : " + str(seq_pred.shape))
        seq_pred_decode = torch.zeros_like(seq_true).view(seq_true.shape[0],1,seq_true.shape[1]).to(device)
        # seq_pred = seq_pred*2-1 Ja estarà entre -1 i 1
        for k in range(seq_pred.shape[0]):
            for l in range(seq_pred.shape[1]):
                for m in range(int(seq_pred.shape[2] / seq_true.shape[1])):
                    seq_pred_decode[k][l][0] = seq_pred_decode[k][l][0] + seq_pred[k][l][3 * m]
                    seq_pred_decode[k][l][1] = seq_pred_decode[k][l][1] + seq_pred[k][l][3 * m + 1]
                    seq_pred_decode[k][l][2] = seq_pred_decode[k][l][2] + seq_pred[k][l][3 * m + 2]
        seq_pred_decode_sigmoid = (torch.sigmoid(seq_pred_decode) + 1) / 2  # El poso també entre 0 i 1
        # yapf: enable
        loss = criterion(
            seq_pred_decode_sigmoid.view(seq_true.shape[0], seq_true.shape[1]).to(device),
            seq_true)
        # L'entrada al BCE és (N,*) on N és el tamany del batch

        loss.backward()
        # optim_enc.step()
        # optim_dec.step()
        optim_auto.step()

        if (epoch % 50) == 0:
            f.write("\n***\nSeq. entrada: " + str(seq_true.cpu().detach().numpy()))
            f.write("\nMig: " + str(mig.cpu().detach().numpy()))
            # f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            dist = distancia(mig.cpu())
            f.write("\nDist codi: " + str(dist))
            f.write("\nSeq. predita: " + str(seq_pred.cpu().detach().numpy()))
            perdua = loss_detall(
                seq_pred_decode_sigmoid.view(seq_true.shape[0], seq_true.shape[1]), seq_true)
            f.write("\nPèrdua: " + str(perdua))
            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:' +
                        str(round(torch.cuda.memory_allocated(0) /
                                  1024**3, 1)) + 'GB')
                f.write('\nCached:   ' +
                        str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                        'GB')

            print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
    f.close()

    torch.save(
        {
            'epoch': epoch,
            'model_state_dict': autoenc.state_dict(),
            'optimizer_state_dict': optim_auto.state_dict(),
            'loss': loss,
            'bits_entrada': bits_entrada,
            'bits_mig': bits_mig,
        }, "VAE.pth")

    # # Calculem l'error
    # errors = 0
    # iteracions = int(1e4)
    # for k in range(iteracions):
    #     seq_pred, mig, mig_noise = autoenc(entrada_torch)
    #     res = (seq_pred > 0.5) * 2 - 1
    #     errors = errors + torch.sum(entrada_torch != res)
    #     if k % 1000 == 0:
    #         print(str(errors) + " : " + str(k))
    # print("Error total " + str(errors.cpu().detach().numpy() /
    #                            (num_etiquetes * bits_entrada * iteracions)))
