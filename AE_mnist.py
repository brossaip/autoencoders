# Començaré per un cas que funciona d'imatges, un autoencoder senzill, com el del tutorial https://github.com/justincharney/Autoencoder_And_Denoiser_Pytorch/blob/main/Autoencoder.ipynb
# i després continuaré fent proves per decréixer el tamany fins a imatges de 3x3 ó 9x9 partint de les mateixes 28x28 que és el tamany del MNIST
# Ara passa a 7x7 que ja és bon rati. He de generar les imatges binàries a veure què tal funciona repetint els codis

import torch as torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torchvision import datasets
import torchvision.transforms as transforms

class AutoEncoder(nn.Module):
  def __init__(self):
    super(AutoEncoder,self).__init__()
    #Encoder
    self.conv1 = nn.Conv2d(1,16, kernel_size=3, padding=1)
    self.conv2 = nn.Conv2d(16,4, kernel_size=3,padding=1)
    self.pool = nn.MaxPool2d(2,2)

    #Decoder
    self.t_conv1 = nn.ConvTranspose2d(4,16,kernel_size=2,stride=2)
    self.t_conv2 = nn.ConvTranspose2d(16,1,kernel_size=2,stride=2)

  def forward(self, x):
    # print(" *** tamany inicial: " + str(x.shape))
    x = F.relu(self.conv1(x))
    x = self.pool(x)
    x = F.relu(self.conv2(x))
    x = self.pool(x)
    # print(" *** tamany mig: " + str(x.shape))
    x = F.relu(self.t_conv1(x))
    x = torch.sigmoid(self.t_conv2(x))
    return x
  
model_2 = AutoEncoder()


#defining some parameters
num_epochs = 1
batch_size = 1

criterion2 = nn.MSELoss()
optimizer2 = torch.optim.Adam(model_2.parameters(), lr = 0.001)

# getting and preparing data

transform = transforms.ToTensor()

train_data = datasets.MNIST(root = "data", train = True, download=True, 
                            transform=transform)
test_data = datasets.MNIST(root='data', train=False,
                                  download=True, transform=transform)
num_workers = 0
train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, num_workers=num_workers)
test_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, num_workers=num_workers)

for epoch in range(num_epochs):
  train_loss = 0
  for data in train_loader:
    images, _ = data

    optimizer2.zero_grad()
    outputs = model_2(images)
    loss2 = criterion2(outputs, images)
    loss2.backward()
    optimizer2.step()
    train_loss += loss2.item()*images.size(0)

  train_loss = train_loss/len(train_loader)
  print("Epoch {} \t Training Loss {:.6f}".format (epoch, train_loss))
