# Cada bit entrarà per un canal, repetit 3 vegades i anirem fent cel·les GCNN
# He pensat que el repetiré 9 vegades i l'aniré reduint amb les convolucions.
# Mantindré el número de canals constants i després ja el modificaré. A la implementació dels GLUBlocks https://github.com/DavidWBressler/GCNN/blob/master/GCNN.ipynb va canviant el tamany
# He partit del AE_GCNN.py

# Després d'algunes lectures he entès que el residual és afegir el vector sense transformar perquè al principi tindrà més pes aquest i esquivar el vanishing effect

# Third Party
import torch
import torch.nn as nn
from torch.nn import BCELoss
import math

############
# COMPONENTS
############


class Encoder(nn.Module):
    # He fet pel cas 9/27
    def __init__(self):
        super(Encoder, self).__init__()
        self.repeticio = 21

        # Faré una combinació lineal de tots els canals, ja que sinó feia sempre el mateix, el conv1d no barreja canals, potser podria aplicar el conv2d en la duplicació
        self.lin1 = nn.Linear(1, self.repeticio + 1)
        # La convolució ha de ser amb diferents canals i la longitud L (N, C_in, L_in) on N és el tamany de batch, C_in són els canals d'entrada i L_in la longitud. A la fórmula de la documentació no veig L, però sí C_in i C_out, L potser està en la convolució del pes per l'input.
        self.cnn1a = nn.Conv1d(in_channels=bits_entrada,
                               out_channels=2 * bits_entrada,
                               kernel_size=2,
                               stride=1)
        self.cnn1b = nn.Conv1d(in_channels=bits_entrada,
                               out_channels=2 * bits_entrada,
                               kernel_size=2,
                               stride=1)
        self.cnn2a = nn.Conv1d(in_channels=2 * bits_entrada,
                               out_channels=3 * bits_entrada,
                               kernel_size=4,
                               stride=1)
        self.cnn2b = nn.Conv1d(in_channels=2 * bits_entrada,
                               out_channels=3 * bits_entrada,
                               kernel_size=4,
                               stride=1)
        self.cnn3a = nn.Conv1d(in_channels=3 * bits_entrada,
                               out_channels=4 * bits_entrada,
                               kernel_size=6,
                               stride=1)
        self.cnn3b = nn.Conv1d(in_channels=3 * bits_entrada,
                               out_channels=4 * bits_entrada,
                               kernel_size=6,
                               stride=1)
        # L_out = \frac{L_in + 2*padding -dilation*(kernel_size-1)-1}{stride} + 1
        # L_out del primer és L_in-2+1-1+1=L_in-kernel+1

        self.lin2 = nn.Linear(13, 13)
        self.cnn1a_2 = nn.Conv1d(in_channels=4 * bits_entrada,
                                 out_channels=3 * bits_entrada,
                                 kernel_size=2,
                                 stride=1)
        self.cnn1b_2 = nn.Conv1d(in_channels=4 * bits_entrada,
                                 out_channels=3 * bits_entrada,
                                 kernel_size=2,
                                 stride=1)
        self.cnn2a_2 = nn.Conv1d(in_channels=3 * bits_entrada,
                                 out_channels=2 * bits_entrada,
                                 kernel_size=4,
                                 stride=1)
        self.cnn2b_2 = nn.Conv1d(in_channels=3 * bits_entrada,
                                 out_channels=2 * bits_entrada,
                                 kernel_size=4,
                                 stride=1)
        self.cnn3a_2 = nn.Conv1d(in_channels=2 * bits_entrada,
                                 out_channels=bits_entrada,
                                 kernel_size=6,
                                 stride=1)
        self.cnn3b_2 = nn.Conv1d(in_channels=2 * bits_entrada,
                                 out_channels=bits_entrada,
                                 kernel_size=6,
                                 stride=1)

        self.lin3 = nn.Linear(4, 3)

    def forward(self, x):
        # Potser és aquest codi de repetir el que fa que no surti bé i que siguin tots molt semblants
        # x_cat = x
        # for k in range(self.repeticio):
        #     x_cat = torch.cat((x, x_cat), dim=2)

        x = self.lin1(x)
        x1 = self.cnn3a(self.cnn2a(self.cnn1a(x)))
        x2 = self.cnn3b(self.cnn2b(self.cnn1b(x)))
        x2 = torch.sigmoid(x2)
        x = torch.mul(x1, x2)

        x = self.lin2(x)
        x1_2 = self.cnn3a_2(self.cnn2a_2(self.cnn1a_2(x)))
        x2_2 = self.cnn3b_2(self.cnn2b_2(self.cnn1b_2(x)))
        x2_2 = torch.sigmoid(x2_2)
        x_2 = torch.mul(x1_2, x2_2)

        x = self.lin3(x_2)
        return (x)


class Decoder(nn.Module):
    def __init__(self):
        super(Decoder, self).__init__()

        self.lin1 = nn.Linear(3, 4)

        # Lout​=(Lin​−1)×stride−2×padding+dilation×(kernel_size−1)+output_padding+1 = (Lin-1)+(kernel_size-1)+1 = Lin +kernel_size -1
        self.cnn1a = nn.ConvTranspose1d(in_channels=bits_entrada,
                                        out_channels=2 * bits_entrada,
                                        kernel_size=2,
                                        stride=1)
        self.cnn1b = nn.ConvTranspose1d(in_channels=bits_entrada,
                                        out_channels=2 * bits_entrada,
                                        kernel_size=2,
                                        stride=1)
        self.cnn2a = nn.ConvTranspose1d(in_channels=2 * bits_entrada,
                                        out_channels=3 * bits_entrada,
                                        kernel_size=4,
                                        stride=1)
        self.cnn2b = nn.ConvTranspose1d(in_channels=2 * bits_entrada,
                                        out_channels=3 * bits_entrada,
                                        kernel_size=4,
                                        stride=1)
        self.cnn3a = nn.ConvTranspose1d(in_channels=3 * bits_entrada,
                                        out_channels=4 * bits_entrada,
                                        kernel_size=6,
                                        stride=1)
        self.cnn3b = nn.ConvTranspose1d(in_channels=3 * bits_entrada,
                                        out_channels=4 * bits_entrada,
                                        kernel_size=6,
                                        stride=1)

        self.lin2 = nn.Linear(13, 13)
        self.cnn1a_2 = nn.ConvTranspose1d(in_channels=4 * bits_entrada,
                                          out_channels=3 * bits_entrada,
                                          kernel_size=2,
                                          stride=1)
        self.cnn1b_2 = nn.ConvTranspose1d(in_channels=4 * bits_entrada,
                                          out_channels=3 * bits_entrada,
                                          kernel_size=2,
                                          stride=1)
        self.cnn2a_2 = nn.ConvTranspose1d(in_channels=3 * bits_entrada,
                                          out_channels=2 * bits_entrada,
                                          kernel_size=4,
                                          stride=1)
        self.cnn2b_2 = nn.ConvTranspose1d(in_channels=3 * bits_entrada,
                                          out_channels=2 * bits_entrada,
                                          kernel_size=4,
                                          stride=1)
        self.cnn3a_2 = nn.ConvTranspose1d(in_channels=2 * bits_entrada,
                                          out_channels=bits_entrada,
                                          kernel_size=6,
                                          stride=1)
        self.cnn3b_2 = nn.ConvTranspose1d(in_channels=2 * bits_entrada,
                                          out_channels=bits_entrada,
                                          kernel_size=6,
                                          stride=1)

        self.lin3 = nn.Linear(22, 1)

    def forward(self, x):
        x = self.lin1(x)
        x1 = self.cnn3a(self.cnn2a(self.cnn1a(x)))
        x2 = self.cnn3b(self.cnn2b(self.cnn1b(x)))
        x2 = torch.sigmoid(x2)
        x = torch.mul(x1, x2)

        x = self.lin2(x)
        x1_2 = self.cnn3a_2(self.cnn2a_2(self.cnn1a_2(x)))
        x2_2 = self.cnn3b_2(self.cnn2b_2(self.cnn1b_2(x)))
        x2_2 = torch.sigmoid(x2_2)
        x_2 = torch.mul(x1_2, x2_2)

        x = self.lin3(x_2)
        x = torch.sigmoid(10 * x)
        return x


class AutoEncoder(nn.Module):
    def __init__(self):
        super(AutoEncoder, self).__init__()

        self.enc = Encoder()
        self.dec = Decoder()

        bits_entrada = 9
        bits_sortida = 27
        Rm = math.log2(2)
        Rc = bits_entrada * 1.0 / bits_sortida
        EbN0 = 10**(2 / 10)
        self.noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

    def forward(self, x):
        mig = self.enc(x)
        mig = 2 * torch.sigmoid(100 * mig) - 1
        # Aquest factor de 100 l'hauria de fer més flexible, hauria de calcular el mínim valor de x i fer-ho adaptable, ja que si tot ja són propers a 1 no cal aquest factor.
        mig_noise = mig + self.noiseSigma * torch.randn(mig.shape)
        x = self.dec(mig_noise.float())
        return x, mig, mig_noise


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')

    bits_entrada = 3

    torch.manual_seed(0)
    # mod_enc = Encoder(bits_entrada, 6)
    # mod_enc.to(device)
    # mod_dec = Decoder(6, num_etiquetes)
    # mod_dec.to(device)
    autoenc = AutoEncoder()
    num_etiquetes = 2**bits_entrada
    #optim_enc = torch.optim.SGD(mod_enc.parameters(), lr=1e-3, weight_decay=1e-5)
    #optim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-5)
    optim_auto = torch.optim.SGD(autoenc.parameters(),
                                 lr=1e-3,
                                 weight_decay=1e-5)

    criterion = BCELoss()
    loss_detall = BCELoss(reduction='none')

    ordre = list(range(num_etiquetes))
    # Per cada etiqueta he de fer el seu vector d'entrada
    entrada = []
    for element in ordre:
        st = ''
        # He d'afegir tants 0 al davant com longitud
        for k in format(element, "b").zfill(bits_entrada):
            st = st + k + ','
        entrada.append(eval('[' + st + ']'))
    entrada_torch = torch.Tensor(entrada) * 2 - 1
    entrada_torch = entrada_torch.view([num_etiquetes, bits_entrada, 1])

    f = open("debugant.log", "w")
    for epoch in range(167500):
        autoenc.train()

        if (epoch % 50) == 0:
            f.write("\n**************\nEpoch: " + str(epoch) +
                    "\n**************\n")

        # optim_enc.zero_grad()
        # optim_dec.zero_grad()
        optim_auto.zero_grad()

        seq_true = (entrada_torch.view(num_etiquetes, bits_entrada) + 1) / 2
        seq_pred, mig, mig_noise = autoenc(entrada_torch)

        #print("seq_pred: " + str(seq_pred.shape))
        #print("seq_true: " + str(seq_true.shape))
        loss = criterion(seq_pred.view(num_etiquetes, bits_entrada), seq_true)
        # L'entrada al BCE és (N,*) on N és el tamany del batch

        loss.backward()
        # optim_enc.step()
        # optim_dec.step()
        optim_auto.step()

        if (epoch % 50) == 0:
            f.write("\n***\nSeq. entrada: " + str(seq_true.detach().numpy()))
            f.write("\nMig: " + str(mig.detach().numpy()))
            f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            f.write("\nSeq. predita: " + str(seq_pred.detach().numpy()))
            perdua = loss_detall(seq_pred.view(num_etiquetes, bits_entrada),
                                 seq_true)
            f.write("\nPèrdua: " + str(perdua))
            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:' +
                        str(round(torch.cuda.memory_allocated(0) /
                                  1024**3, 1)) + 'GB')
                f.write('\nCached:   ' +
                        str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                        'GB')

            print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
            torch.save(
                {
                    'epoch': epoch,
                    'model_state_dict': autoenc.state_dict(),
                    'optimizer_state_dict': optim_auto.state_dict(),
                    'loss': loss,
                    'bits_entrada': bits_entrada,
                }, "AE_GCNN_RepeatChannels.pth")

    f.close()

    torch.save(
        {
            'epoch': epoch,
            'model_state_dict': autoenc.state_dict(),
            'optimizer_state_dict': optim_auto.state_dict(),
            'loss': loss,
            'bits_entrada': bits_entrada,
        }, "AE_GCNN_RepeatChannels.pth")
