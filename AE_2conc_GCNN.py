# Vull utilitzar el 2conc que ja tinc i fer-lo congelat que em servirà com a etapa fixa per afegir després diferents capes de GCNN que millorin el codi. Ja que quan tenia aquestes capes soles no eren capaces de generar un codi amb distància entre els generats.

import torch
import torch.nn as nn
from torch.nn import BCELoss
from random import shuffle
import math
import Autoencoder_2conc as model

###########################
# Carreguem els components del 2conc
###########################

checkpoint = torch.load("AutoEnc_2conc_2EbN0_ep167500_9º27.pth")
bits_entrada_2conc = checkpoint['bits_entrada']
bits_mig_2conc = checkpoint['bits_mig']

ae_2conc = model.AutoEncoder()
ae_2conc.load_state_dict(checkpoint['model_state_dict'])

# congelem els paràmetres https://jimmy-shen.medium.com/pytorch-freeze-part-of-the-layers-4554105e03a6
for param in ae_2conc.parameters():
    param.requires_grad = False


###########################
# Noves capes
###########################
class Encoder(nn.Module):
    # He fet pel cas 9/27
    def __init__(self):
        super(Encoder, self).__init__()

        self.lin1_a = nn.Linear(27, 27)
        self.lin1_b = nn.Linear(27, 27)
        self.lin1_c = nn.Linear(27, 27)

        # La convolució ha de ser amb diferents canals i la longitud L (N, C_in, L_in) on N és el tamany de batch, C_in són els canals d'entrada i L_in la longitud. A la fórmula de la documentació no veig L, però sí C_in i C_out, L potser està en la convolució del pes per l'input.
        self.cnn1a = nn.Conv1d(in_channels=1,
                               out_channels=3,
                               kernel_size=4,
                               stride=1)
        self.cnn1b = nn.Conv1d(in_channels=1,
                               out_channels=3,
                               kernel_size=4,
                               stride=1)
        self.cnn2a = nn.Conv1d(in_channels=3,
                               out_channels=6,
                               kernel_size=7,
                               stride=1)
        self.cnn2b = nn.Conv1d(in_channels=3,
                               out_channels=6,
                               kernel_size=7,
                               stride=1)
        self.cnn3a = nn.Conv1d(in_channels=6,
                               out_channels=1,
                               kernel_size=4,
                               stride=1,
                               padding=6)
        self.cnn3b = nn.Conv1d(in_channels=6,
                               out_channels=1,
                               kernel_size=4,
                               stride=1,
                               padding=6)
        # L_out = \frac{L_in + 2*padding -dilation*(kernel_size-1)-1}{stride} + 1
        # L_out del primer és L_in-2+1-1+1=L_in-kernel+1

        self.lin2 = nn.Linear(27*3, 27*3) # Potser no hauria de fer comb.lineal, hauria de repetir els codis.
        self.cnn1a_2 = nn.Conv1d(in_channels=1,
                                 out_channels=3,
                                 kernel_size=4,
                                 stride=1)
        self.cnn1b_2 = nn.Conv1d(in_channels=1,
                                 out_channels=3,
                                 kernel_size=4,
                                 stride=1)
        self.cnn2a_2 = nn.Conv1d(in_channels=3,
                                 out_channels=6,
                                 kernel_size=7,
                                 stride=1)
        self.cnn2b_2 = nn.Conv1d(in_channels=3,
                                 out_channels=6,
                                 kernel_size=7,
                                 stride=1)
        self.cnn3a_2 = nn.Conv1d(in_channels=6,
                                 out_channels=1,
                                 kernel_size=4,
                                 stride=1,
                                 padding=6)
        self.cnn3b_2 = nn.Conv1d(in_channels=6,
                                 out_channels=1,
                                 kernel_size=4,
                                 stride=1,
                                 padding=6)

        self.lin3 = nn.Linear(27*3, 27)

    def forward(self, x):
        x_a = self.lin1_a(x)
        x_b = self.lin1_b(x)
        x_c = self.lin1_c(x)
        x = torch.cat((x_a, x_b, x_c), 2)  # Longitud 27

        x1 = self.cnn3a(self.cnn2a(self.cnn1a(x)))
        x2 = self.cnn3b(self.cnn2b(self.cnn1b(x)))
        x2 = torch.sigmoid(x2)
        x = torch.mul(x1, x2) + x

        x = self.lin2(x)
        x1_2 = self.cnn3a_2(self.cnn2a_2(self.cnn1a_2(x)))
        x2_2 = self.cnn3b_2(self.cnn2b_2(self.cnn1b_2(x)))
        x2_2 = torch.sigmoid(x2_2)
        x_2 = torch.mul(x1_2, x2_2) + x

        x = self.lin3(x)
        return (x)


class Decoder(nn.Module):
    def __init__(self):
        super(Decoder, self).__init__()

        self.lin1 = nn.Linear(27, 27*3)

        # Lout​=(Lin​−1)×stride−2×padding+dilation×(kernel_size−1)+output_padding+1 = (Lin-1)+(kernel_size-1)+1 = Lin +kernel_size -1
        self.cnn1a = nn.Conv1d(in_channels=1,
                                        out_channels=3,
                                        kernel_size=4,
                                        stride=1)
        self.cnn1b = nn.Conv1d(in_channels=1,
                                        out_channels=3,
                                        kernel_size=4,
                                        stride=1)
        self.cnn2a = nn.Conv1d(in_channels=3,
                                        out_channels=6,
                                        kernel_size=7,
                                        stride=1)
        self.cnn2b = nn.Conv1d(in_channels=3,
                                        out_channels=6,
                                        kernel_size=7,
                                        stride=1)
        self.cnn3a = nn.Conv1d(in_channels=6,
                                        out_channels=1,
                                        kernel_size=4,
                                        stride=1,
                               padding=6)
        self.cnn3b = nn.Conv1d(in_channels=6,
                                        out_channels=1,
                                        kernel_size=4,
                                        stride=1,
                               padding=6)

        self.lin2 = nn.Linear(27*3, 27*3)
        self.cnn1a_2 = nn.Conv1d(in_channels=1,
                                          out_channels=3,
                                          kernel_size=4,
                                          stride=1)
        self.cnn1b_2 = nn.Conv1d(in_channels=1,
                                          out_channels=3,
                                          kernel_size=4,
                                          stride=1)
        self.cnn2a_2 = nn.Conv1d(in_channels=3,
                                          out_channels=6,
                                          kernel_size=7,
                                          stride=1)
        self.cnn2b_2 = nn.Conv1d(in_channels=3,
                                          out_channels=6,
                                          kernel_size=7,
                                          stride=1)
        self.cnn3a_2 = nn.Conv1d(in_channels=6,
                                          out_channels=1,
                                          kernel_size=4,
                                          stride=1,
                                 padding=6)
        self.cnn3b_2 = nn.Conv1d(in_channels=6,
                                          out_channels=1,
                                          kernel_size=4,
                                          stride=1,
                                 padding=6)

        self.lin3_a = nn.Linear(27, 9)
        self.lin3_b = nn.Linear(27, 9)
        self.lin3_c = nn.Linear(27, 9)

    def forward(self, x):
        x = self.lin1(x)
        x1 = self.cnn3a(self.cnn2a(self.cnn1a(x)))
        x2 = self.cnn3b(self.cnn2b(self.cnn1b(x)))
        x2 = torch.sigmoid(x2)
        x = torch.mul(x1, x2) + x

        x = self.lin2(x)
        x1_2 = self.cnn3a_2(self.cnn2a_2(self.cnn1a_2(x)))
        x2_2 = self.cnn3b_2(self.cnn2b_2(self.cnn1b_2(x)))
        x2_2 = torch.sigmoid(x2_2)
        x_2 = torch.mul(x1_2, x2_2) + x

        x = torch.chunk(x_2,3,dim=2)
        #print ("x[0] shape: " + str(x[0].shape))
        x_a = self.lin3_a(x[0])
        x_b = self.lin3_b(x[1])
        x_c = self.lin3_c(x[2])

        # Els preparo per poder aplicar el BCELoss
        x = torch.cat((x_a, x_b, x_c), 2)  # 1x9
        x = torch.sigmoid(10 * x)
        return x


class AutoEncoder(nn.Module):
    def __init__(self):
        super(AutoEncoder, self).__init__()

        self.enc = Encoder()
        self.dec = Decoder()

        bits_entrada = 9
        bits_sortida = 27
        Rm = math.log2(2)
        Rc = bits_entrada * 1.0 / bits_sortida
        EbN0 = 10**(2 / 10)
        self.noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

    def forward(self, x):
        mig = self.enc(x)
        mig = 2 * torch.sigmoid(100 * mig) - 1
        # Aquest factor de 100 l'hauria de fer més flexible, hauria de calcular el mínim valor de x i fer-ho adaptable, ja que si tot ja són propers a 1 no cal aquest factor.
        #print("***** Mig: " + str(mig))
        mig_noise = mig + self.noiseSigma * torch.randn(mig.shape)
        x = self.dec(mig_noise.float())
        return x, mig, mig_noise


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')

    torch.manual_seed(0)
    # mod_enc = Encoder(bits_entrada, 6)
    # mod_enc.to(device)
    # mod_dec = Decoder(6, num_etiquetes)
    # mod_dec.to(device)
    autoenc = AutoEncoder()

    optim_auto = torch.optim.SGD(autoenc.parameters(),
                                 lr=1e-3,
                                 weight_decay=1e-5)

    criterion = BCELoss()
    loss_detall = BCELoss(reduction='none')

    # Generem l'entrada a partir dels 3 bits originals del 2conc
    num_etiquetes_2conc = 2**bits_entrada_2conc
    ordre = list(range(num_etiquetes_2conc))
    # Per cada etiqueta he de fer el seu vector d'entrada
    entrada = []
    for element in ordre:
        st = ''
        # He d'afegir tants 0 al davant com longitud
        for k in format(element, "b").zfill(bits_entrada_2conc):
            st = st + k + ','
        entrada.append(eval('[' + st + ']'))
    entrada_torch_2conc = torch.Tensor(entrada) * 2 - 1
    entrada_torch_2conc = entrada_torch_2conc.view(
        [num_etiquetes_2conc, 1, bits_entrada_2conc])

    sortida_enc_2conc = ae_2conc.enc(entrada_torch_2conc)
    sortida_enc_2conc = (sortida_enc_2conc > 0) * 2 - 1
    sortida_enc_2conc = sortida_enc_2conc.float()

    f = open("debugant.log", "w")
    for epoch in range(167500):
        autoenc.train()

        if (epoch % 50) == 0:
            f.write("\n**************\nEpoch: " + str(epoch) +
                    "\n**************\n")

        # optim_enc.zero_grad()
        # optim_dec.zero_grad()
        optim_auto.zero_grad()

        # yapf: disable
        seq_true = (entrada_torch_2conc.view(entrada_torch_2conc.shape[0],
                                           entrada_torch_2conc.shape[2]) + 1) / 2
        # yapf: enable

        seq_pred, mig, mig_noise = autoenc(sortida_enc_2conc)

        sortida_dec_2conc = ae_2conc.dec(2 * seq_pred - 1)

        #print("seq_pred: " + str(seq_pred.shape))
        #print("seq_true: " + str(seq_true.shape))
        loss = criterion(
            sortida_dec_2conc.view(seq_true.shape[0], seq_true.shape[1]),
            seq_true)
        # L'entrada al BCE és (N,*) on N és el tamany del batch

        loss.backward()
        # optim_enc.step()
        # optim_dec.step()
        optim_auto.step()

        if (epoch % 50) == 0:
            f.write("\n***\nSeq. entrada: " + str(seq_true.detach().numpy()))
            f.write("\nMig: " + str(mig.detach().numpy()))
            f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            f.write("\nSeq. predita: " + str(seq_pred.detach().numpy()))
            perdua = loss_detall(
                sortida_dec_2conc.view(seq_true.shape[0], seq_true.shape[1]),
                seq_true)
            f.write("\nPèrdua: " + str(perdua))
            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:' +
                        str(round(torch.cuda.memory_allocated(0) /
                                  1024**3, 1)) + 'GB')
                f.write('\nCached:   ' +
                        str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                        'GB')

            print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
            torch.save(
                {
                    'epoch': epoch,
                    'model_state_dict': autoenc.state_dict(),
                    'optimizer_state_dict': optim_auto.state_dict(),
                    'loss': loss,
                    'bits_entrada': 3,
                }, "AE_2conc_GCNN.pth")

    f.close()

    torch.save(
        {
            'epoch': epoch,
            'model_state_dict': autoenc.state_dict(),
            'optimizer_state_dict': optim_auto.state_dict(),
            'loss': loss,
            'bits_entrada': 3,
        }, "AE_2conc_GCNN.pth")
