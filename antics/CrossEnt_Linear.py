# Basat en el CrossEnt_RNN però ara amb xarxes convolucionals

# Third Party
import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss, MSELoss
from random import shuffle
import numpy as np

############
# COMPONENTS
############


class Encoder(nn.Module):
    def __init__(self, num_features, embedding_dim=64):
        super(Encoder, self).__init__()

        self.num_features = num_features
        self.embedding_dim = embedding_dim

        self.lin1 = nn.Linear(num_features, embedding_dim)

    def forward(self, x):
        x = self.lin1(x)
        return (x)


class Decoder(nn.Module):
    def __init__(self, input_dim=64, output_dim=1):
        super(Decoder, self).__init__()

        self.input_dim = input_dim
        self.output_dim = output_dim

        self.out = nn.Linear(self.input_dim, output_dim)

    def forward(self, x):
        x = self.out(
            x
        )  # Té dimensions (N,*,H_in). Diria que N són el nombre de samples, * qualsevol cosa i H_in el número de features de l'entrada
        return x


# PEl que mostren els logs sembla que només s'optimitzi el decoder. Hauré de fer la implementació dels dos mòduls per separat i fer l'optimització de cadascun. Tal i com sembla que facin a https://github.com/astorfi/sequence-to-sequence-from-scratch/blob/master/seq2seq.py
# Però quina és la funció de cost de l'encoder? No és la de l'etiqueta final, si no com se'l valora?
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

torch.manual_seed(0)
mod_enc = Encoder(3, 6)
mod_enc.to(device)
mod_dec = Decoder(6, 8)
mod_dec.to(device)

optim_enc = torch.optim.Adam(mod_enc.parameters(), lr=1e-3, weight_decay=1e-5)
optim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-5)
#criterion = CrossEntropyLoss(weight=torch.Tensor([1e5, 1, 1, 1, 1, 1, 1, 2]), reduction='sum') # Si no és sum no té en compte els pesos. El pes 0 indica que té loss=0 i per tant no hi deu haver millora en el gradient.
criterion = CrossEntropyLoss()

input = torch.tensor([[-1., -1., -1.], [-1., 1., -1.], [1., -1., -1.],
                      [1., 1., -1.], [-1., -1., 1.], [-1., 1., 1.],
                      [1., -1., 1.], [1., 1., 1.]]).to(device)
# He pensat que potser s'encallava en alguns valors perquè els epochs eren sempre iguals, ara variaré l'ordre
ordre = list(range(len(input)))
x = input.view(len(input), 1, 3)

losses, embeddings, aproximacions = [], [], []
tanh = nn.Tanh()
thresh = nn.Threshold(0,-1)
f = open("debugant.log", "w")
for epoch in range(1150):
    mod_enc.train() # https://stackoverflow.com/questions/51433378/what-does-model-train-do-in-pytorch
    mod_dec.train()

    shuffle(ordre)
    if (epoch%50)==0:
        f.write("\n**************\nEpoch: " + str(epoch) + "\n**************\n")
    for batch in ordre:
        seq_true = x[batch]
        seq_true = seq_true.view(
            1, 1, 3
        )  # Per al Conv1d: NNN is a batch size, CCC denotes a number of channels, LLL is a length of signal sequence
        # Per al linial  (N,*,H_in)
        optim_enc.zero_grad()
        optim_dec.zero_grad()
        mig = mod_enc(seq_true)
        #mig_noise = (mig > 0) * 2 - 1 # Quan he fet aquestes operacions ha deixat de tenir el grad_fn
        # mig_noise = mig_noise + torch.randn(mig.shape)
        # He de fer una combinació de funcions per binaritzar. També hi havia el pytorch-tools https://github.com/pabloppp/pytorch-tools
        mig_noise = torch.ceil(thresh(tanh(mig)))
        mig_noise = mig_noise + 0.5*torch.randn(mig.shape)
        seq_pred = mod_dec(mig_noise.float()) # Crec que el problema és el mig_noise que no és una variable, si no només un tensor i per tant no passa la funció de derivada cap enrere https://towardsdatascience.com/getting-started-with-pytorch-part-1-understanding-how-automatic-differentiation-works-5008282073ec

        # Passem les etiquetes esperades, les de seq_true
        seq_true_bits = (seq_true + 1) / 2
        etiquetes_true = torch.LongTensor([0])
        for k in range(max(seq_true_bits.shape)):
            etiquetes_true = etiquetes_true + ((2**k) * seq_true_bits[0][0][k])
        loss = criterion(
            seq_pred.view(1, 8), etiquetes_true.long()
        )  # L'entrada al crossEntropy és (N,C) on N és el tamany del batch (1 en el nostre cas) i C el resultat per cada classe

        loss.backward()
        optim_enc.step()
        optim_dec.step()

        losses.append(loss.item())
        embeddings.append(mig)
        aproximacions.append(seq_pred)
        if (epoch%50)==0:
            f.write("\n***\nSeq. entrada: " + str(seq_true.detach().numpy()))
            f.write("\nEtiqueta correcta: " + str(etiquetes_true))
            f.write("\nMig: " + str(mig.detach().numpy()))
            f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            f.write("\nSeq. predita: " + str(seq_pred.detach().numpy()))
            f.write("\nPèrdua: " + str(loss.item()))
            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:', round(torch.cuda.memory_allocated(0)/1024**3,1), 'GB')
                f.write('\nCached:   ', round(torch.cuda.memory_cached(0)/1024**3,1), 'GB')
    print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
f.close()

# Gravem model per si volem continuar millorant https://pytorch.org/tutorials/beginner/saving_loading_models.html
torch.save({
    'epoch': epoch,
    'model_state_dict_enc': mod_enc.state_dict(),
    'model_state_dict_dec': mod_dec.state_dict(),
    'optimizer_state_dict_enc': optim_enc.state_dict(),
    'optimizer_state_dict_dec': optim_dec.state_dict(),
    'loss': loss,
}, "CrossEnt_Linear.pth")
# El mig surt tant petit que poc es pot millorar. I si li afegim soroll? El soroll no afecta gaire, fa que també es torni petit els pesos del codificador. L'haurem de fer més complicat. I com hem d'afegir el soroll?

# # Model generat
# loss_mostra = []
# for k in range(len(input)):
#     entrada = x[k]
#     etiqueta_ent = 0
#     entrada_bit = (x[k] + 1) / 2
#     for l in range(len(entrada[0])):
#         etiqueta_ent = etiqueta_ent + ((2**l) * entrada_bit[0][l])
#     seq_pred, mig, mig_noise = model(entrada.view(1, 1, 3))
#     loss = criterion(seq_pred.view(1, 8), etiqueta_ent.view(1).long())
#     loss_mostra.append(loss.item())
#     # print("*** {};{} - {} \n- {} - {}".format(str(entrada), str(etiqueta_ent),
#     #                                          str(mig), str(seq_pred), str(loss)))
#     print("*** {};{} \n {} \n - {} - {}".format(str(entrada),
#                                                 str(etiqueta_ent),
#                                                 str(mig_noise), str(seq_pred),
#                                                 str(loss)))

# Busquem quin té l'error més petit i si està molt separat dels altres, si és així el traiem a l'hora d'actualitzar-se
# mitja = np.mean(loss_mostra)
# desv = np.std(loss_mostra)
# dist = 1
# pesos = []
# for ind in range(len(input)):
#     if (mitja - loss_mostra[ind]) > 0 \
#        and (mitja - loss_mostra[ind]) > (dist * desv):
#         pesos.append(0)
#     else:
#         pesos.append(1)
# criterion = CrossEntropyLoss(weight=torch.Tensor(pesos), reduction='sum')
# # En comptes de fer els pesos traurem l'element
# # També puc provar d'entrenar només dos, i després anar incrementant a veure com es comporta

# Fer un log per veure bé els detalls i estudiar si fa els passos bé

# En aquesta primera versió, augmenta els errors dels que no estan. Potser perquè l'estructura és massa senzilla i es necessitaria fer-la més complexa.
# He augmentat la complexitat però també dóna pes a dues classes. Potser hem de balancejar els pesos per donar altres valors a altres casos.
# Barrejaré el MSSE amb el crossEntropy. A veure si millora el resultat obtingut
