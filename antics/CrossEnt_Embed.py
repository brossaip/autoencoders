# Basat en el CrossEnt_Conv però ara amb xarxes lineals abans i després de la convolució
# He de treballar amb les etiquetes i no amb vectors.

# Third Party
import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss, MSELoss
from random import shuffle
import math

############
# COMPONENTS
############


def plot_grad_flow(named_parameters):
    ave_grads = []
    layers = []
    for n, p in named_parameters:
        if (p.requires_grad) and ("bias" not in n):
            layers.append(n)
            ave_grads.append(p.grad.abs().mean())
    plt.plot(ave_grads, alpha=0.3, color="b")
    plt.hlines(0, 0, len(ave_grads) + 1, linewidth=1, color="k")
    plt.xticks(range(0, len(ave_grads), 1), layers, rotation="vertical")
    plt.xlim(xmin=0, xmax=len(ave_grads))
    plt.xlabel("Layers")
    plt.ylabel("average gradient")
    plt.title("Gradient flow")
    plt.grid(True)


def plot_grad_flow_v2(named_parameters):
    '''Plots the gradients flowing through different layers in the net during training.
    Can be used for checking for possible gradient vanishing / exploding problems.
    
    Usage: Plug this function in Trainer class after loss.backwards() as 
    "plot_grad_flow(self.model.named_parameters())" to visualize the gradient flow'''
    ave_grads = []
    max_grads = []
    layers = []
    for n, p in named_parameters:
        if (p.requires_grad) and ("bias" not in n):
            layers.append(n)
            ave_grads.append(p.grad.abs().mean())
            max_grads.append(p.grad.abs().max())
    plt.bar(np.arange(len(max_grads)), max_grads, alpha=0.1, lw=1, color="c")
    plt.bar(np.arange(len(max_grads)), ave_grads, alpha=0.1, lw=1, color="b")
    plt.hlines(0, 0, len(ave_grads) + 1, lw=2, color="k")
    plt.xticks(range(0, len(ave_grads), 1), layers, rotation="vertical")
    plt.xlim(left=0, right=len(ave_grads))
    plt.ylim(bottom=-0.001, top=0.02)  # zoom in on the lower gradient regions
    plt.xlabel("Layers")
    plt.ylabel("average gradient")
    plt.title("Gradient flow")
    plt.grid(True)
    plt.legend([
        Line2D([0], [0], color="c", lw=4),
        Line2D([0], [0], color="b", lw=4),
        Line2D([0], [0], color="k", lw=4)
    ], ['max-gradient', 'mean-gradient', 'zero-gradient'])


class Encoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida):
        super(Encoder, self).__init__()

        self.bits_entrada = bits_entrada
        self.bits_sortida = bits_sortida

        self.embd = nn.Embedding(2**bits_entrada, bits_sortida)
        self.lin1 = nn.Linear(bits_sortida, bits_sortida)
        # La convolució ha de ser amb diferents canals i la longitud L (N, C_in, L_in) on N és el tamany de batch, C_in són els canals d'entrada i L_in la longitud. A la fórmula de la documentació no veig L, però sí C_in i C_out, L potser està en la convolució del pes per l'input.
        kernel_size = 2
        self.cnn1 = nn.Conv1d(
            in_channels=1,
            out_channels=1,
            kernel_size=
            kernel_size,  # Kernel_size can't be greater than input size
            stride=1)
        # Després del lineal, tinc una seqüència amb més bits, però segueix sent unidimensional, d'un sol canal, per això el Conv1d té in_channels=1
        self.lin2 = nn.Linear(bits_sortida - kernel_size + 1, bits_sortida)

    def forward(self, x):
        x = self.embd(x)
        x = self.lin1(x)
        x = self.cnn1(x)
        x = self.lin2(x)

        return (x)


class Decoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida):
        super(Decoder, self).__init__()

        self.lin1 = nn.Linear(bits_entrada, bits_entrada)
        kernel_size = 2
        self.cnt1 = nn.ConvTranspose1d(in_channels=1,
                                       out_channels=1,
                                       kernel_size=2,
                                       stride=1)
        self.out = nn.Linear(bits_entrada + kernel_size - 1, 2**bits_sortida)

    def forward(self, x):
        x = self.lin1(x)
        x = self.cnt1(x)
        x = self.out(
            x
        )  # Té dimensions (N,*,H_in). Diria que N són el nombre de samples, * qualsevol cosa i H_in el número de features de l'entrada
        return x


class AutoEncoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida, bits_mig):
        super(AutoEncoder, self).__init__()

        self.enc = Encoder(bits_entrada, bits_mig)
        self.dec = Decoder(bits_mig, bits_sortida)

        self.tanh = nn.Tanh()
        self.thresh = nn.Threshold(0, -1)

        Rm = math.log2(2)
        Rc = bits_entrada * 1.0 / bits_sortida
        EbN0 = 10**(6 / 10)
        self.noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

    def forward(self, x):
        mig = self.enc(x)
        # mig_noise_tanh = self.tanh(mig)
        # mig_noise_thres = self.thresh(mig_noise_tanh)
        # mig_noise_neg = -mig_noise_thres
        # mig_noise_neg_thres = self.thresh(mig_noise_neg)
        mig_noise = 2 * torch.sigmoid(10 * mig) - 1
        mig_noise = mig_noise + self.noiseSigma * torch.randn(mig_noise.shape)
        x = self.dec(mig_noise.float())
        return x, mig, mig_noise


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

bits_entrada = 3
bits_mig = 6

torch.manual_seed(0)
# mod_enc = Encoder(bits_entrada, 6)
# mod_enc.to(device)
# mod_dec = Decoder(6, num_etiquetes)
# mod_dec.to(device)
autoenc = AutoEncoder(bits_entrada, bits_entrada, bits_mig)
num_etiquetes = 2**bits_entrada
#optim_enc = torch.optim.SGD(mod_enc.parameters(), lr=1e-3, weight_decay=1e-5)
#optim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-5)
optim_auto = torch.optim.SGD(autoenc.parameters(), lr=1e-3, weight_decay=1e-5)

criterion = CrossEntropyLoss()
loss_detall = CrossEntropyLoss(reduction='none')

ordre = list(range(num_etiquetes))

# Per si volem recarregar el model
# checkpoint = torch.load("CrossEnt_embed.pth")
# autoenc.load_state_dict(checkpoint['model_state_dict'])
# optim_auto.load_state_dict(checkpoint['optimizer_state_dict'])
# loss = checkpoint['loss']
# epoch_carregada = checkpoint['epoch']

losses, embeddings, aproximacions = [], [], []
tanh = nn.Tanh()
thresh = nn.Threshold(0, -1)
f = open("debugant.log", "w")
for epoch in range(65000):
    # mod_enc.train()
    # mod_dec.train()
    autoenc.train()

    if (epoch % 50) == 0:
        f.write("\n**************\nEpoch: " + str(epoch) +
                "\n**************\n")

    # optim_enc.zero_grad()
    # optim_dec.zero_grad()
    optim_auto.zero_grad()

    seq_true = torch.Tensor(ordre).view(num_etiquetes, 1).long()
    seq_true.to(device)

    # mig = mod_enc(seq_true)
    # mig_noise = torch.ceil(
    #     thresh(tanh(mig))
    # )  # Aquest mig noise fa que el backpropagation no arribi. Provaré que el vector de sortida també generi una etiqueta com el decoder
    # #mig_noise = mig
    # seq_pred = mod_dec(mig_noise.float())

    seq_pred, mig, mig_noise = autoenc(seq_true)

    loss = criterion(
        seq_pred.view(num_etiquetes, 8), seq_true.view(8)
    )  # L'entrada al crossEntropy és (N,C) on N és el tamany del batch (1 en el nostre cas) i C el resultat per cada classe

    loss.backward()
    # optim_enc.step()
    # optim_dec.step()
    optim_auto.step()

    losses.append(loss.item())
    embeddings.append(mig)
    aproximacions.append(seq_pred)

    if (epoch % 50) == 0:
        f.write("\n***\nSeq. entrada: " + str(seq_true.detach().numpy()))
        f.write("\nMig: " + str(mig.detach().numpy()))
        f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
        f.write("\nSeq. predita: " + str(seq_pred.detach().numpy()))
        perdua = loss_detall(seq_pred.view(num_etiquetes, 8), seq_true.view(8))
        f.write("\nPèrdua: " + str(perdua))
        if device.type == 'cuda':
            f.write(torch.cuda.get_device_name(0))
            f.write('\nMemory Usage:')
            f.write('\nAllocated:' +
                    str(round(torch.cuda.memory_allocated(0) / 1024**3, 1)) +
                    'GB')
            f.write('\nCached:   ' +
                    str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                    'GB')

        print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
f.close()

torch.save(
    {
        'epoch': epoch,
        'model_state_dict': autoenc.state_dict(),
        'optimizer_state_dict': optim_auto.state_dict(),
        'loss': loss,
        'bits_entrada': bits_entrada,
        'bits_mig': bits_mig,
    }, "CrossEnt_embed.pth")

# Mirem el BER, l'anterior era l'error d'entrenament per trobar un codi.
# Fins i tot podria entrenar el decoder perquè milloris amb les diferents entrades
# autoenc = AutoEncoder(3, 3, 6)
# optim_auto = torch.optim.SGD(autoenc.parameters(), lr=1e-3, weight_decay=1e-5)

# checkpoint = torch.load("CrossEnt_embed.pth")
# autoenc.load_state_dict(checkpoint['model_state_dict'])
# optim_auto.load_state_dict(checkpoint['optimizer_state_dict'])

# Generem l'útlima sortida
ordre = list(range(num_etiquetes))
seq_true = torch.Tensor(ordre).view(num_etiquetes, 1).long()
seq_pred, mig, mig_noise = autoenc(seq_true)
dades = (mig > 0.5) * 2 - 1

errors = 0
for k in range(1000):
    dades_noise = dades + autoenc.noiseSigma * torch.randn(dades.shape)

    seq_pred = autoenc.dec(dades_noise)
    val, ind = torch.max(seq_pred, dim=2)
    for eti in range(num_etiquetes):
        if ind[eti] != eti:
            errors = errors + 1
print("Errors: " + str(errors) + " de 1000 words")

# Provant graf per debugar
#from gv import digraph
#import torch
#from torch.autograd import Variable


def make_dot(var, params):
    """ Produces Graphviz representation of PyTorch autograd graph
    
    Blue nodes are the Variables that require grad, orange are Tensors
    saved for backward in torch.autograd.Function
    
    Args:
        var: output Variable
        params: dict of (name, Variable) to add names to node that
            require grad (TODO: make optional)
    """
    param_map = {id(v): k for k, v in params.items()}
    print(param_map)

    node_attr = dict(style='filled',
                     shape='box',
                     align='left',
                     fontsize='12',
                     ranksep='0.1',
                     height='0.2')
    # dot = digraph(node_attr=node_attr, graph_attr=dict(size="12,12"))
    dot = digraph()
    seen = set()

    def size_to_str(size):
        return '(' + (', ').join(['%d' % v for v in size]) + ')'

    def add_nodes(var):
        if var not in seen:
            if torch.is_tensor(var):
                dot.node(str(id(var)),
                         size_to_str(var.size()),
                         fillcolor='orange')
            elif hasattr(var, 'variable'):
                u = var.variable
                node_name = '%s\n %s' % (param_map.get(
                    id(u)), size_to_str(u.size()))
                dot.node(str(id(var)), node_name, fillcolor='lightblue')
            else:
                dot.node(str(id(var)), str(type(var).__name__))
            seen.add(var)
            if hasattr(var, 'next_functions'):
                for u in var.next_functions:
                    if u[0] is not None:
                        dot.edge(str(id(u[0])), str(id(var)))
                        add_nodes(u[0])
            if hasattr(var, 'saved_tensors'):
                for t in var.saved_tensors:
                    dot.edge(str(id(t)), str(id(var)))
                    add_nodes(t)

    add_nodes(var.grad_fn)
    return dot
