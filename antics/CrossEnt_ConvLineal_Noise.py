# Basat en el CrossEnt_Conv però ara amb xarxes lineals abans i després de la convolució

# Third Party
import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss, MSELoss
from random import shuffle

############
# COMPONENTS
############


class Encoder(nn.Module):
    def __init__(self, num_features, embedding_dim=64):
        super(Encoder, self).__init__()

        self.num_features = num_features
        self.embedding_dim = embedding_dim

        self.lin1 = nn.Linear(num_features, embedding_dim)
        # La convolució ha de ser amb diferents canals i la longitud L (N, C_in, L_in) on N és el tamany de batch, C_in són els canals d'entrada i L_in la longitud. A la fórmula de la documentació no veig L, però sí C_in i C_out, L potser està en la convolució del pes per l'input.
        kernel_size = 2
        self.cnn1 = nn.Conv1d(
            in_channels=1,
            out_channels=1,
            kernel_size=
            kernel_size,  # Kernel_size can't be greater than input size
            stride=1)
        # Després del lineal, tinc una seqüència amb més bits, però segueix sent unidimensional, d'un sol canal, per això el Conv1d té in_channels=1
        self.lin2 = nn.Linear(embedding_dim - kernel_size + 1, embedding_dim)

    def forward(self, x):
        x = self.lin1(x)
        x = self.cnn1(x)
        x = self.lin2(x)
        return (x)


class Decoder(nn.Module):
    def __init__(self, input_dim=64, output_dim=1):
        super(Decoder, self).__init__()

        self.input_dim = input_dim
        self.output_dim = output_dim
        kernel_size = 2
        self.lin1 = nn.Linear(input_dim, input_dim - kernel_size + 1)
        self.cnt1 = nn.ConvTranspose1d(in_channels=1,
                                       out_channels=1,
                                       kernel_size=2,
                                       stride=1)
        self.out = nn.Linear(self.input_dim, output_dim)

    def forward(self, x):
        x = self.lin1(x)
        x = self.cnt1(x)
        x = self.out(
            x
        )  # Té dimensions (N,*,H_in). Diria que N són el nombre de samples, * qualsevol cosa i H_in el número de features de l'entrada
        return x


class Coder_Loss(nn.Module):
    def __init__(self):
        super(Coder_Loss, self).__init__()

    def forward(self, sortida_enc, etiquetes):
        # El que he de fer és que les etiquetes esiguin relacionades amb el valor de la sortida del codificador. No pas posar-les aquí. Potser hauré de fer tot el batch que siguin totes a la vegada. Així no aniré creant un vector amb els items que pot ser que sigui el que faci que no es relacioni

        # He posat sortida_enc perquè ho relacioni en el backward
        hist = torch.histc(etiquetes,
                           bins=len(etiquetes),
                           min=0,
                           max=len(etiquetes))
        torch.max()


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

torch.manual_seed(0)
mod_enc = Encoder(3, 6)
mod_enc.to(device)
mod_dec = Decoder(6, 8)
mod_dec.to(device)

optim_enc = torch.optim.Adam(mod_enc.parameters(), lr=1e-3, weight_decay=1e-5)
optim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-5)
criterion = CrossEntropyLoss()
crit_enc = Coder_Loss()

input = torch.tensor([[1., 1., -1.], [-1., -1., 1.], [-1., 1., 1.],
                      [-1., -1., -1.], [-1., 1., -1.], [1., -1., -1.],
                      [1., -1., 1.], [1., 1., 1.]]).to(device)
input = 0.5 * input
# He pensat que potser s'encallava en alguns valors perquè els epochs eren sempre iguals, ara variaré l'ordre
ordre = list(range(len(input)))
x = input.view(len(input), 1, 3)

losses, embeddings, aproximacions = [], [], []
tanh = nn.Tanh()
thresh = nn.Threshold(0, -1)
f = open("debugant.log", "w")
for epoch in range(350):
    mod_enc.train()
    mod_dec.train()

    shuffle(ordre)
    if (epoch % 50) == 0:
        f.write("\n**************\nEpoch: " + str(epoch) +
                "\n**************\n")

    # Puc passar tot l'x com a batch i funciona correctament.
    # Després passo el mig a -1 i 1
    # Comparo amb bucle per anar calculant la distància. Potser hauré de repetir el resultat en un vector de 8¿?
    for batch in ordre:
        seq_true = x[batch]
        seq_true = seq_true.view(
            1, 1, 3
        )  # NNN is a batch size, CCC denotes a number of channels, LLL is a length of signal sequence
        optim_enc.zero_grad()
        optim_dec.zero_grad()

        mig = mod_enc(seq_true)
        mig_noise = 0.5 * torch.ceil(thresh(tanh(mig)))
        #mig_noise = mig
        #mig_noise = mig_noise + 0.5*torch.randn(mig.shape)
        seq_pred = mod_dec(mig_noise.float())

        # Passem les etiquetes esperades, les de seq_true
        seq_true_bits = (seq_true + 1) / 2
        etiquetes_true = torch.LongTensor([0])
        for k in range(max(seq_true_bits.shape)):
            etiquetes_true = etiquetes_true + ((2**k) * seq_true_bits[0][0][k])
        loss = criterion(
            seq_pred.view(1, 8), etiquetes_true.long()
        )  # L'entrada al crossEntropy és (N,C) on N és el tamany del batch (1 en el nostre cas) i C el resultat per cada classe
        loss.backward()
        if epoch < 10:
            optim_enc.step()
            optim_dec.step()
        elif epoch < 20:
            optim_dec.step()
        elif epoch < 30:
            optim_enc.step()
        elif epoch < 40:
            optim_dec.step()
        elif epoch < 50:
            optim_enc.step()
        elif epoch < 60:
            optim_dec.step()
        elif epoch < 70:
            optim_enc.step()
        elif epoch < 80:
            optim_dec.step()
        elif epoch < 90:
            optim_enc.step()

        losses.append(loss.item())
        embeddings.append(mig)
        aproximacions.append(seq_pred)

        if (epoch % 50) == 0:
            f.write("\n***\nSeq. entrada: " + str(seq_true.detach().numpy()))
            f.write("\nEtiqueta correcta: " + str(etiquetes_true))
            f.write("\nMig: " + str(mig.detach().numpy()))
            f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            f.write("\nSeq. predita: " + str(seq_pred.detach().numpy()))
            f.write("\nPèrdua: " + str(loss.item()))
            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:',
                        round(torch.cuda.memory_allocated(0) / 1024**3, 1),
                        'GB')
                f.write('\nCached:   ',
                        round(torch.cuda.memory_cached(0) / 1024**3, 1), 'GB')
    print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
f.close()

torch.save(
    {
        'epoch': epoch,
        'model_state_dict_enc': mod_enc.state_dict(),
        'model_state_dict_dec': mod_dec.state_dict(),
        'optimizer_state_dict_enc': optim_enc.state_dict(),
        'optimizer_state_dict_dec': optim_dec.state_dict(),
        'loss': loss,
    }, "CrossEnt_ConvLinear.pth")
# Sembla que arribi un moment que els pesos es fan tan petits que el codificador ja no fa res. Per totes les seqüències d'entrada, dóna sempre la mateixa sortida

# He de provar de fer-ho per etapes. Primer millorem codificador, congelem i després millorem descodificador
