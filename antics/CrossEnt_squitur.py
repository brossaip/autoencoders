# Basat en el rae de sequitur https://github.com/shobrook/sequitur/blob/master/sequitur/autoencoders/rae.py
# Pel tema del crossEntropy he seguit el video https://www.youtube.com/watch?v=7q7E91pHoW4
# Com que ha de ser multiclasse en posarem 3 bits.
# Quan valorem el crossEntropy, necessitem que la sortida indiqui el número de classes, una sortida lineal, tal i com indica la pàg 73 del Pytorch Recipes
# Per tant les etiquetes seran un vector de 8 posicions amb 1 indicant el valor

# Third Party
import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss, MSELoss

############
# COMPONENTS
############


class Encoder(nn.Module):
    def __init__(self, seq_len, num_features, embedding_dim=64):
        super(Encoder, self).__init__()

        self.seq_len, self.num_features = seq_len, num_features
        self.embedding_dim, self.hidden_dim = embedding_dim, 2 * embedding_dim

        self.rnn1 = nn.LSTM(input_size=num_features,
                            hidden_size=self.hidden_dim,
                            num_layers=1,
                            batch_first=True)
        self.rnn2 = nn.LSTM(input_size=self.hidden_dim,
                            hidden_size=embedding_dim,
                            num_layers=1,
                            batch_first=True)

    def forward(self, x):
        x = x.reshape((1, self.seq_len, self.num_features))

        x, (hidden_n, cell_n) = self.rnn1(x)
        x, (hidden_n, cell_n) = self.rnn2(x)
        # return hidden_n.reshape((1, self.embedding_dim))
        return(x)


class Decoder(nn.Module):
    def __init__(self, seq_len, input_dim=64, output_dim=1):
        super(Decoder, self).__init__()

        self.seq_len, self.input_dim = seq_len, input_dim
        self.hidden_dim, self.output_dim = 2 * input_dim, output_dim

        self.rnn1 = nn.LSTM(input_size=input_dim,
                            hidden_size=input_dim,
                            num_layers=1,
                            batch_first=True)
        self.rnn2 = nn.LSTM(input_size=input_dim,
                            hidden_size=self.hidden_dim,
                            num_layers=1,
                            batch_first=True)
        self.out = nn.Linear(self.input_dim, 8)


    def forward(self, x):
        x = x.repeat(self.seq_len, 1)
        x = x.reshape((1, self.seq_len, self.input_dim))

        x, (hidden_n, cell_n) = self.rnn1(x)
        x, (hidden_n, cell_n) = self.rnn2(x)
        x = x.reshape((self.seq_len, self.hidden_dim))


        return x


#########
# EXPORTS
#########


class RAE(nn.Module):
    def __init__(self, seq_len, num_features, embedding_dim=64):
        # seq_len seria el número d'elements de mostra
        # num_features són les característiques, les dimensions d'entrada.
        super(RAE, self).__init__()

        self.seq_len, self.num_features = seq_len, num_features
        self.embedding_dim = embedding_dim

        self.encoder = Encoder(seq_len, num_features, embedding_dim)
        self.decoder = Decoder(seq_len, embedding_dim, num_features)

    def forward(self, x):
        mig = self.encoder(x)
        x = self.decoder(mig)

        return x, mig


torch.manual_seed(0)
model = RAE(1, 3, 8)

optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)
criterion = CrossEntropyLoss()

input = torch.tensor([[-1., -1., -1.], [-1., 1., -1.], [1., -1., -1.],
                      [1., 1., -1.], [-1., -1., 1.], [-1., 1., 1.],
                      [1., -1., 1.], [1., 1., 1.]])
x = input.view(8, 1, 3)

losses, embeddings, aproximacions = [], [], []
for epoch in range(150):
    model.train()
    for batch in range(len(input)):
        seq_true = x[batch]
        optimizer.zero_grad()
        seq_pred, mig = model(seq_true)

        # Passem les etiquetes esperades, les de seq_true
        seq_true_bits = (seq_true + 1) / 2
        etiquetes_true = torch.LongTensor([0])
        for k in range(len(seq_true_bits)):
            etiquetes_true = etiquetes_true + ((2**k) * seq_true_bits[0][k])
        loss = criterion(seq_pred, etiquetes_true.long())

        loss.backward()
        optimizer.step()

        losses.append(loss.item())
        embeddings.append(mig)
        aproximacions.append(seq_pred)

    print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))

# Model generat
for k in range(len(input)):
    entrada = x[k]
    seq_pred, mig = model(entrada)
    print("*** {} - {} - {}".format(str(entrada),str(mig),str(seq_pred)))
