# Carego Autoencoder_conc i carrego les etapes que vull aprofitar
# Primer provaré fent més complexe el descodificador i si no millora gaire, afegiré també una etapa del codificador
# També puc correlar la informació dels 3 bits abans de concatenar-los

import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss, BCELoss
from random import shuffle
import math
import numpy as np
import Autoencoder_conc as original

# He de carregar primer el model amb els valors estàtics i després accedir a les parts
checkpoint = torch.load("AutoEnc_conc_2EbN0_ep17500.pth")

autoenc = original.AutoEncoder(3,3,9)
autoenc.load_state_dict(checkpoint['model_state_dict'])

encoder = autoenc.enc
orig_decoder = autoenc.dec


def bits_orig_decoder(x):
    x = orig_decoder.lin1(x)
    x = orig_decoder.cnt1(x)
    x = orig_decoder.out(x)
    xc = torch.chunk(x, 3, dim=2)

    # Vull tenir la informació final però també actuar en els bits anteriors, per això concateno els bits repetint-los 2 vegades, ja que havia aconseguit bons resultats
    x_a = orig_decoder.lin1_a(xc[0])
    x_b = orig_decoder.lin1_b(xc[1])
    x_c = orig_decoder.lin1_c(xc[2])
    x = torch.cat((x_a, x_b, x_c,x,x_a,x_b,x_c ), 2)
    x = torch.sigmoid(x)

class Nova_etapa_dec(nn.Module):
    def __init__(self):
        super(Nova_etapa_dec, self).__init__()

        self.lin1_2st = nn.Linear(15,15)
        self.cnn1_2st = nn.Conv1d(
            in_channels=1,
            out_channels=1,
            kernel_size=3,
            stride=1)
        # L_out = \frac{L_in + 2*padding -dilation*(kernel_size-1)-1}{stride} + 1
        self.lin2_2st = nn.Linear(int((15+2*0-1*(3-1))/1+1),3)

    def forwad(self,x):
        x = self.lin1_2st(x)
        x = self.cnn1_2st(X)
        x = self.lin2_2st(x)
        x = torch.sigmoid(10*x)

if __name__ == '__main__':
    device = torch.device('cpu')

    torch.manual_seed(0)
    nova_et_dec = Nova_etapa_dec()

    optim_ned = torch.optim.SGD(nova_et_dec.parameters(), lr=1e-3, weight_decay=1e-5)

    criterion = BCELoss()
    loss_detall = BCELoss(reduction='none')

    ordre = list(range(8))
    # Per cada etiqueta he de fer el seu vector d'entrada
    entrada = []
    for element in ordre:
        st = ''
        # He d'afegir tants 0 al davant com longitud
        for k in format(element, "b").zfill(3):
            st = st + k + ','
        entrada.append(eval('[' + st + ']'))
    entrada_torch = torch.Tensor(entrada) * 2 - 1
    entrada_torch = entrada_torch.view([8, 1, 3])

    losses, embeddings, aproximacions = [], [], []

    # Potència soroll
    Rm = math.log2(2)
    Rc = 3 * 1.0 / 9
    EbN0 = 10**(2 / 10)
    noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

    f = open("debugant.log", "w")
    for epoch in range(17500):
        nova_et_dec.train()

        if (epoch % 50) == 0:
            f.write("\n**************\nEpoch: " + str(epoch) +
                    "\n**************\n")

        optim_ned.zero_grad()

        seq_true = (entrada_torch.view(8, 3) + 1) / 2

        encode = encoder(entrada_torch)
        codi = 2*torch.sigmoid(100*encode)-1

        # Falta afegir el soroll !!!!!!!!!!!!
        senyal = codi + noiseSigma * torch.randn(codi.shape)
        decode = bits_orig_decoder(encode)

        decode_2stg = optim_ned(decode)

        loss = criterion(decode_2stg.view(8, 3), seq_true)
        # L'entrada al BCE és (N,*) on N és el tamany del batch

        loss.backward()
        optim_ned.step()

        losses.append(loss.item())
        aproximacions.append(seq_pred)

        if (epoch % 50) == 0:
            f.write("\n***\nSeq. entrada: " + str(seq_true.detach().numpy()))
            f.write("\nMig: " + str(mig.detach().numpy()))
            f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            f.write("\nSeq. predita: " + str(seq_pred.detach().numpy()))
            perdua = loss_detall(seq_pred.view(8, 3), seq_true)
            f.write("\nPèrdua: " + str(perdua))

            print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
    f.close()

    torch.save(
        {
            'epoch': epoch,
            'model_state_dict': optim_ned.state_dict(),
            'optimizer_state_dict': optim_ned.state_dict(),
            'loss': loss,
        }, "AutoEnc_conc_2stg.pth")
