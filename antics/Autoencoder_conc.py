# Vull provar si concatenant diferents combinacions lineals, en lloc d'una que generi 9 bits funciona més bé. Semblant als LDPC que fan diferents c.lineals repetides. Continuaré provant pel cas 3/9

# Third Party
import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss, MSELoss, BCELoss
from random import shuffle
import math

############
# COMPONENTS
############


def plot_grad_flow(named_parameters):
    ave_grads = []
    layers = []
    for n, p in named_parameters:
        if (p.requires_grad) and ("bias" not in n):
            layers.append(n)
            ave_grads.append(p.grad.abs().mean())
    plt.plot(ave_grads, alpha=0.3, color="b")
    plt.hlines(0, 0, len(ave_grads) + 1, linewidth=1, color="k")
    plt.xticks(range(0, len(ave_grads), 1), layers, rotation="vertical")
    plt.xlim(xmin=0, xmax=len(ave_grads))
    plt.xlabel("Layers")
    plt.ylabel("average gradient")
    plt.title("Gradient flow")
    plt.grid(True)


def plot_grad_flow_v2(named_parameters):
    '''Plots the gradients flowing through different layers in the net during training.
    Can be used for checking for possible gradient vanishing / exploding problems.
    
    Usage: Plug this function in Trainer class after loss.backwards() as 
    "plot_grad_flow(self.model.named_parameters())" to visualize the gradient flow'''
    ave_grads = []
    max_grads = []
    layers = []
    for n, p in named_parameters:
        if (p.requires_grad) and ("bias" not in n):
            layers.append(n)
            ave_grads.append(p.grad.abs().mean())
            max_grads.append(p.grad.abs().max())
    plt.bar(np.arange(len(max_grads)), max_grads, alpha=0.1, lw=1, color="c")
    plt.bar(np.arange(len(max_grads)), ave_grads, alpha=0.1, lw=1, color="b")
    plt.hlines(0, 0, len(ave_grads) + 1, lw=2, color="k")
    plt.xticks(range(0, len(ave_grads), 1), layers, rotation="vertical")
    plt.xlim(left=0, right=len(ave_grads))
    plt.ylim(bottom=-0.001, top=0.02)  # zoom in on the lower gradient regions
    plt.xlabel("Layers")
    plt.ylabel("average gradient")
    plt.title("Gradient flow")
    plt.grid(True)
    plt.legend([
        Line2D([0], [0], color="c", lw=4),
        Line2D([0], [0], color="b", lw=4),
        Line2D([0], [0], color="k", lw=4)
    ], ['max-gradient', 'mean-gradient', 'zero-gradient'])


class Encoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida):
        super(Encoder, self).__init__()

        self.lin1_a = nn.Linear(bits_entrada, bits_entrada)
        self.lin1_b = nn.Linear(bits_entrada, bits_entrada)
        self.lin1_c = nn.Linear(bits_entrada, bits_entrada)
        # La convolució ha de ser amb diferents canals i la longitud L (N, C_in, L_in) on N és el tamany de batch, C_in són els canals d'entrada i L_in la longitud. A la fórmula de la documentació no veig L, però sí C_in i C_out, L potser està en la convolució del pes per l'input.
        kernel_size = 1
        self.cnn1 = nn.Conv1d(
            in_channels=1,
            out_channels=1,
            kernel_size=
            kernel_size,  # Kernel_size can't be greater than input size
            stride=1)
        # L_out = \frac{L_in + 2*padding -dilation*(kernel_size-1)-1}{stride} + 1
        self.lin2 = nn.Linear(bits_sortida - kernel_size + 1 - 1 + 1,
                              bits_sortida)

    def forward(self, x):
        x_a = self.lin1_a(x)
        x_b = self.lin1_b(x)
        x_c = self.lin1_c(x)
        x = torch.cat((x_a, x_b, x_c), 2)
        x = self.cnn1(x)
        x = self.lin2(x)

        return (x)


class Decoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida):
        # Hauré de tornar a separar els bits en els seus repetits i després fer un binari classification
        super(Decoder, self).__init__()

        self.lin1 = nn.Linear(bits_entrada, bits_entrada)
        kernel_size = 1
        self.cnt1 = nn.ConvTranspose1d(in_channels=1,
                                       out_channels=1,
                                       kernel_size=kernel_size,
                                       stride=1)
        self.out = nn.Linear(bits_entrada - 1 + kernel_size - 1 + 1,
                             bits_entrada)
        # Lout​=(Lin​−1)×stride−2×padding+dilation×(kernel_size−1)+output_padding+1
        self.lin1_a = nn.Linear(bits_sortida, 1)
        self.lin1_b = nn.Linear(bits_sortida, 1)
        self.lin1_c = nn.Linear(bits_sortida, 1)

    def forward(self, x):
        x = self.lin1(x)
        x = self.cnt1(x)
        # He de separar la convolució resultant en els 3 inicials i després fer una c.l. per poder aplicar Binary CrossEntropyLoss
        x = self.out(
            x
        )  # Té dimensions (N,*,H_in). Diria que N són el nombre de samples, * qualsevol cosa i H_in el número de features de l'entrada
        x = torch.chunk(x, 3, dim=2)
        # Ha retornat una tupla amb els 3 vectors
        # Ara hauria de combinar cada 1 d'aquests vectors per obtenir un bit que sigui l'original.
        # No podré fer el BCELoss. He de tornar a ajuntar els bits per fer les etiquetes de tots els possibles valors de l'entrada
        # No cal, entenc per la docu (https://pytorch.org/docs/stable/generated/torch.nn.BCELoss.html#torch.nn.BCELoss) que si aplico el BCE em farà el binari loss per cadascú. He creat un vector de prova i he vist que el BCELoss et fa el binari entropy loss bit a bit. Si fas el BCEWithLogitsLoss no et cal fer un sigmoid abans.
        x_a = self.lin1_a(x[0])
        x_b = self.lin1_b(x[1])
        x_c = self.lin1_c(x[2])
        x = torch.cat((x_a, x_b, x_c), 2)
        x = torch.sigmoid(
            10 * x)  # Quan he afegit aquest sigmoid l'error ha disminuït molt.
        return x


class AutoEncoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida, bits_mig):
        super(AutoEncoder, self).__init__()

        self.enc = Encoder(bits_entrada, bits_mig)
        self.dec = Decoder(bits_mig, bits_sortida)

        Rm = math.log2(2)
        Rc = 3 * 1.0 / 9
        EbN0 = 10**(5 / 10)
        self.noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

    def forward(self, x):
        mig = self.enc(x)
        mig = 2 * torch.sigmoid(100 * mig) - 1
        mig_noise = mig + self.noiseSigma * torch.randn(mig.shape)
        x = self.dec(mig_noise.float())
        return x, mig, mig_noise


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')

    bits_entrada = 3
    bits_mig = 9

    torch.manual_seed(0)
    # mod_enc = Encoder(bits_entrada, 6)
    # mod_enc.to(device)
    # mod_dec = Decoder(6, num_etiquetes)
    # mod_dec.to(device)
    autoenc = AutoEncoder(bits_entrada, bits_entrada, bits_mig)
    num_etiquetes = 2**bits_entrada
    #optim_enc = torch.optim.SGD(mod_enc.parameters(), lr=1e-3, weight_decay=1e-5)
    #optim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-5)
    optim_auto = torch.optim.SGD(autoenc.parameters(),
                                 lr=1e-3,
                                 weight_decay=1e-5)

    criterion = BCELoss()
    loss_detall = BCELoss(reduction='none')

    ordre = list(range(num_etiquetes))
    # Per cada etiqueta he de fer el seu vector d'entrada
    entrada = []
    for element in ordre:
        st = ''
        # He d'afegir tants 0 al davant com longitud
        for k in format(element, "b").zfill(bits_entrada):
            st = st + k + ','
        entrada.append(eval('[' + st + ']'))
    entrada_torch = torch.Tensor(entrada) * 2 - 1
    entrada_torch = entrada_torch.view([num_etiquetes, 1, bits_entrada])

    losses, embeddings, aproximacions = [], [], []
    tanh = nn.Tanh()
    thresh = nn.Threshold(0, -1)
    f = open("debugant.log", "w")
    for epoch in range(37500):
        autoenc.train()

        if (epoch % 50) == 0:
            f.write("\n**************\nEpoch: " + str(epoch) +
                    "\n**************\n")

        # optim_enc.zero_grad()
        # optim_dec.zero_grad()
        optim_auto.zero_grad()

        seq_true = (entrada_torch.view(8, 3) + 1) / 2
        seq_pred, mig, mig_noise = autoenc(entrada_torch)

        loss = criterion(seq_pred.view(8, 3), seq_true)
        # L'entrada al BCE és (N,*) on N és el tamany del batch

        loss.backward()
        # optim_enc.step()
        # optim_dec.step()
        optim_auto.step()

        losses.append(loss.item())
        embeddings.append(mig)
        aproximacions.append(seq_pred)

        if (epoch % 50) == 0:
            f.write("\n***\nSeq. entrada: " + str(seq_true.detach().numpy()))
            f.write("\nMig: " + str(mig.detach().numpy()))
            f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            f.write("\nSeq. predita: " + str(seq_pred.detach().numpy()))
            perdua = loss_detall(seq_pred.view(8, 3), seq_true)
            f.write("\nPèrdua: " + str(perdua))
            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:' +
                        str(round(torch.cuda.memory_allocated(0) /
                                  1024**3, 1)) + 'GB')
                f.write('\nCached:   ' +
                        str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                        'GB')

            print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
    f.close()

    torch.save(
        {
            'epoch': epoch,
            'model_state_dict': autoenc.state_dict(),
            'optimizer_state_dict': optim_auto.state_dict(),
            'loss': loss,
            'bits_entrada': bits_entrada,
            'bits_mig': bits_mig,
        }, "AutoEnc_conc.pth")

    # Mirem el BER, l'anterior era l'error d'entrenament per trobar un codi.
    # Fins i tot podria entrenar el decoder perquè milloris amb les diferents entrades
    # autoenc = AutoEncoder(3, 3, 6)
    # optim_auto = torch.optim.SGD(autoenc.parameters(), lr=1e-3, weight_decay=1e-5)

    # checkpoint = torch.load("CrossEnt_embed.pth")
    # autoenc.load_state_dict(checkpoint['model_state_dict'])
    # optim_auto.load_state_dict(checkpoint['optimizer_state_dict'])

    # # Generem l'útlima sortida
    # ordre = list(range(num_etiquetes))
    # seq_true = torch.Tensor(ordre).view(num_etiquetes, 1).long()
    # seq_pred, mig, mig_noise = autoenc(seq_true)
    # dades = (mig > 0.5) * 2 - 1

    # errors = 0
    # for k in range(1000):
    #     dades_noise = dades + autoenc.noiseSigma * torch.randn(dades.shape)

    #     seq_pred = autoenc.dec(dades_noise)
    #     val, ind = torch.max(seq_pred, dim=2)
    #     for eti in range(num_etiquetes):
    #         if ind[eti] != eti:
    #             errors = errors + 1
    # print("Errors: " + str(errors) + " de 1000 words")

    # Provant graf per debugar
    #from gv import digraph
    #import torch
    #from torch.autograd import Variable


    def make_dot(var, params):
        """ Produces Graphviz representation of PyTorch autograd graph

        Blue nodes are the Variables that require grad, orange are Tensors
        saved for backward in torch.autograd.Function

        Args:
            var: output Variable
            params: dict of (name, Variable) to add names to node that
                require grad (TODO: make optional)
        """
        param_map = {id(v): k for k, v in params.items()}
        print(param_map)

        node_attr = dict(style='filled',
                         shape='box',
                         align='left',
                         fontsize='12',
                         ranksep='0.1',
                         height='0.2')
        # dot = digraph(node_attr=node_attr, graph_attr=dict(size="12,12"))
        dot = digraph()
        seen = set()

        def size_to_str(size):
            return '(' + (', ').join(['%d' % v for v in size]) + ')'

        def add_nodes(var):
            if var not in seen:
                if torch.is_tensor(var):
                    dot.node(str(id(var)),
                             size_to_str(var.size()),
                             fillcolor='orange')
                elif hasattr(var, 'variable'):
                    u = var.variable
                    node_name = '%s\n %s' % (param_map.get(
                        id(u)), size_to_str(u.size()))
                    dot.node(str(id(var)), node_name, fillcolor='lightblue')
                else:
                    dot.node(str(id(var)), str(type(var).__name__))
                seen.add(var)
                if hasattr(var, 'next_functions'):
                    for u in var.next_functions:
                        if u[0] is not None:
                            dot.edge(str(id(u[0])), str(id(var)))
                            add_nodes(u[0])
                if hasattr(var, 'saved_tensors'):
                    for t in var.saved_tensors:
                        dot.edge(str(id(t)), str(id(var)))
                        add_nodes(t)

        add_nodes(var.grad_fn)
        return dot
