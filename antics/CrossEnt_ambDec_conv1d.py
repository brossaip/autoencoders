# Vull provar que potser amb una convolució en lloc de una convtransp també dóna bons resultats

# Third Party
import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss, MSELoss
from random import shuffle
import math
import numpy as np

############
# COMPONENTS
############

class Encoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida):
        super(Encoder, self).__init__()

        self.bits_entrada = bits_entrada
        self.bits_sortida = bits_sortida

        self.embd = nn.Embedding(2**bits_entrada, bits_sortida)
        self.lin1 = nn.Linear(bits_sortida, bits_sortida)
        # La convolució ha de ser amb diferents canals i la longitud L (N, C_in, L_in) on N és el tamany de batch, C_in són els canals d'entrada i L_in la longitud. A la fórmula de la documentació no veig L, però sí C_in i C_out, L potser està en la convolució del pes per l'input.
        kernel_size = 2
        self.cnn1 = nn.Conv1d(
            in_channels=1,
            out_channels=1,
            kernel_size=
            kernel_size,  # Kernel_size can't be greater than input size
            stride=1)
        # Després del lineal, tinc una seqüència amb més bits, però segueix sent unidimensional, d'un sol canal, per això el Conv1d té in_channels=1
        # L_out = \frac{L_in + 2*padding -dilation*(kernel_size-1)-1}{stride} + 1
        self.lin2 = nn.Linear(bits_sortida - kernel_size + 1, bits_sortida)

    def forward(self, x):
        x = self.embd(x)
        x = self.lin1(x)
        x = self.cnn1(x)
        x = self.lin2(x)

        return (x)


class Decoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida):
        super(Decoder, self).__init__()

        self.lin1 = nn.Linear(bits_entrada, bits_entrada)
        kernel_size = 2
        self.cnn1 = nn.Conv1d(in_channels=1,
                              out_channels=1,
                              kernel_size=kernel_size,
                              stride=1)
        # L_out_transp = (L_in-1)*stride - 2*padding + dilation*(kernel_size-1) + output_padding+1
        self.out = nn.Linear(bits_entrada -kernel_size + 1, 2**bits_sortida)

    def forward(self, x):
        x = self.lin1(x)
        x = self.cnn1(x)
        x = self.out(x)
        # Té dimensions (N,*,H_in). Diria que N són el nombre de samples, * qualsevol cosa i H_in el número de features de l'entrada
        return x


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')
    bits_entrada = 3
    num_etiquetes = 2**bits_entrada
    bits_sortida = bits_entrada*3

    torch.manual_seed(0)
    mod_enc = Encoder(bits_entrada, bits_sortida)
    mod_enc.to(device)
    mod_dec = Decoder(bits_sortida, bits_entrada)
    mod_dec.to(device)
    optim_enc = torch.optim.SGD(mod_enc.parameters(), lr=1e-3, weight_decay=1e-5)
    optim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-5)

    criterion = CrossEntropyLoss()
    loss_detall = CrossEntropyLoss(reduction='none')

    ordre = list(range(num_etiquetes))
    Rm = math.log2(2)
    Rc = bits_entrada*1.0/bits_sortida
    EbN0 = 10**(12/10)
    noiseSigma = math.sqrt(1/(2*Rm*Rc*EbN0))


    losses, embeddings, aproximacions = [], [], []
    canvi = 0
    f = open("debugant.log", "w")
    for epoch in range(32000):
        mod_enc.train()
        mod_dec.train()
        # autoenc.train()

        if (epoch % 50) == 0:
            f.write("\n**************\nEpoch: " + str(epoch) +
                    "\n**************\n")

        optim_enc.zero_grad()
        optim_dec.zero_grad()
        # optim_auto.zero_grad()

        seq_true = torch.Tensor(ordre).view(num_etiquetes, 1).long().to(device)

        mig = mod_enc(seq_true)
        mig_noise = 2*torch.sigmoid(10*mig)-1
        mig_noise = mig_noise + noiseSigma*torch.randn(mig_noise.shape).to(device)
        mig_noise = mig_noise.float()
        seq_pred = mod_dec(mig_noise)
        # seq_pred, mig, mig_noise = autoenc(seq_true)

        loss = criterion(
            seq_pred.view(num_etiquetes, num_etiquetes), seq_true.view(num_etiquetes)
        )  # L'entrada al crossEntropy és (N,C) on N és el tamany del batch (1 en el nostre cas) i C el resultat per cada classe

        loss.backward()
        # optim_auto.step()
        optim_enc.step()
        optim_dec.step()

        losses.append(loss.item())
        embeddings.append(mig)
        aproximacions.append(seq_pred)
        mean3 = np.mean(losses[len(losses)-10:])
        if mean3 <= 0.0001 and canvi ==0:
            EbN0 = 10**(10/10)
            noiseSigma = math.sqrt(1/(2*Rm*Rc*EbN0))
            canvi = 1
            epoch_canvi = epoch
            f.write("\n****** Canvi1: " + str(noiseSigma) + "; epoch: " + str(epoch_canvi) )
            print("\n****** Canvi1: " + str(noiseSigma) + "; Loss: " + str(loss))
        if mean3 <= 0.0001 and canvi ==1 and epoch>epoch_canvi+100:
            EbN0 = 10**(8/10)
            noiseSigma = math.sqrt(1/(2*Rm*Rc*EbN0))
            canvi = 2
            epoch_canvi = epoch
            f.write("\n****** Canvi3: " + str(noiseSigma) + "; epoch: " + str(epoch_canvi) )
            print("\n****** Canvi2: " + str(noiseSigma) + "; epoch: " + str(epoch_canvi) )
        if mean3 <= 0.0001 and canvi ==2 and epoch>epoch_canvi+100:
            EbN0 = 10**(6/10)
            noiseSigma = math.sqrt(1/(2*Rm*Rc*EbN0))
            canvi = 3
            epoch_canvi = epoch
            f.write("\n****** Canvi3: " + str(noiseSigma) + "; epoch: " + str(epoch_canvi) )
            print("\n****** Canvi3: " + str(noiseSigma) + "; epoch: " + str(epoch_canvi) )

        if (epoch % 50) == 0:
            # f.write("\n***\nSeq. entrada: " + str(seq_true.detach().numpy()))
            f.write("\nMig: " + str(mig.detach().cpu().numpy()))
            f.write("\nMig Noise: " + str(mig_noise.detach().cpu().numpy()))
            f.write("\nSeq. predita: " + str(seq_pred.detach().cpu().numpy()))
            perdua = loss_detall(seq_pred.view(num_etiquetes, num_etiquetes), seq_true.view(num_etiquetes))
            f.write("\nPèrdua: " + str(perdua))
            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:' + str(round(torch.cuda.memory_allocated(0) / 1024**3, 1)) + 'GB')
                f.write('\nCached:   ' + str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) + 'GB')

            print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
    f.close()

    np.savetxt("losses.csv",losses, delimiter=",")
    torch.save(
        {
            'epoch': epoch,
            'encoder_state_dict': mod_enc.state_dict(),
            'decoder_state_dict': mod_dec.state_dict(),
            'optimizer_encoder_state_dict': optim_enc.state_dict(),
            'optimizer_decoder_state_dict': optim_dec.state_dict(),
            'loss': loss,
            'bits_entrada': bits_entrada,
            'bits_sortida': bits_sortida,
        }, "CrossEnt_Embed_2conv1d.pth")

    # Comprovar si el de només 1 convolucional, amb aquests EbN0 donava els mateixos errors o no
