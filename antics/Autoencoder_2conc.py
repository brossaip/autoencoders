# Basta en Autoencoder_conc però tornant a separar i augmentar per després tornar a reduir

# Third Party
import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss, MSELoss, BCELoss
from random import shuffle
import math

############
# COMPONENTS
############


class Encoder(nn.Module):
    # He fet pel cas 9/27
    def __init__(self):
        super(Encoder, self).__init__()

        self.lin1_a = nn.Linear(9, 9)
        self.lin1_b = nn.Linear(9, 9)
        self.lin1_c = nn.Linear(9, 9)
        # La convolució ha de ser amb diferents canals i la longitud L (N, C_in, L_in) on N és el tamany de batch, C_in són els canals d'entrada i L_in la longitud. A la fórmula de la documentació no veig L, però sí C_in i C_out, L potser està en la convolució del pes per l'input.
        kernel_size = 1
        self.cnn1 = nn.Conv1d(
            in_channels=1,
            out_channels=1,
            kernel_size=
            kernel_size,  # Kernel_size can't be greater than input size
            stride=1)
        # L_out = \frac{L_in + 2*padding -dilation*(kernel_size-1)-1}{stride} + 1
        self.lin2_a = nn.Linear(27 - kernel_size + 1 - 1 + 1, 27)
        self.lin2_b = nn.Linear(27 - kernel_size + 1 - 1 + 1, 27)
        self.lin2_c = nn.Linear(27 - kernel_size + 1 - 1 + 1, 27)
        self.cnn2 = nn.Conv1d(in_channels=1,
                              out_channels=1,
                              kernel_size=4,
                              stride=1)
        self.lin4 = nn.Linear(81 - 1 * (4 - 1) - 1 + 1, 27)

    def forward(self, x):
        x_cat = torch.cat((x, x, x), dim=2)
        x_a = self.lin1_a(x)
        x_b = self.lin1_b(x)
        x_c = self.lin1_c(x)
        x = torch.cat((x_a, x_b, x_c), 2)  # Longitud 27
        x = self.cnn1(x)
        x_a = self.lin2_a(x)
        x_b = self.lin2_b(x)
        x_c = self.lin2_c(x)
        x = torch.cat((x_a, x_b, x_c), 2)  # Longitud 81
        x = self.cnn2(x)
        x = self.lin4(x)  # La torno a fer de 27
        return (x)


class Decoder(nn.Module):
    def __init__(self):
        super(Decoder, self).__init__()

        self.lin1 = nn.Linear(27, 81)
        self.cnt1 = nn.ConvTranspose1d(in_channels=1,
                                       out_channels=1,
                                       kernel_size=4,
                                       stride=1)
        self.lincnt = nn.Linear((81 - 1) * 1 - 2 * 0 + 1 * (4 - 1) + 0 + 1, 81)
        # Lout​=(Lin​−1)×stride−2×padding+dilation×(kernel_size−1)+output_padding+1

        self.lin1_a = nn.Linear(27, 9)
        self.lin1_b = nn.Linear(27, 9)
        self.lin1_c = nn.Linear(27, 9)

        self.cnt2 = nn.ConvTranspose1d(in_channels=1,
                                       out_channels=1,
                                       kernel_size=1,
                                       stride=1)
        self.out = nn.Linear((27 - 1) * 1 - 2 * 0 + 1 * (1 - 1) + 0 + 1, 27)

        self.lin2_a = nn.Linear(9, 3)
        self.lin2_b = nn.Linear(9, 3)
        self.lin2_c = nn.Linear(9, 3)

        self

    def forward(self, x):
        x = self.lin1(x)  # 1x81
        x = self.cnt1(x)
        x = self.lincnt(x)  # 1x81

        x = torch.chunk(x, 3, dim=2)  # 3x27
        x_a = self.lin1_a(x[0])
        x_b = self.lin1_b(x[1])
        x_c = self.lin1_c(x[2])

        x = torch.cat((x_a, x_b, x_c), 2)  # 1x27
        x = self.cnt2(x)
        x = self.out(x)

        x = torch.chunk(x, 3, dim=2)  # 3x9
        x_a = self.lin2_a(x[0])
        x_b = self.lin2_b(x[1])
        x_c = self.lin2_c(x[2])

        x = torch.cat((x_a, x_b, x_c), 2)  # 1x9
        x = torch.sigmoid(10 * x)
        return x


class AutoEncoder(nn.Module):
    def __init__(self):
        super(AutoEncoder, self).__init__()

        self.enc = Encoder()
        self.dec = Decoder()

        bits_entrada = 9
        bits_sortida = 27
        Rm = math.log2(2)
        Rc = bits_entrada * 1.0 / bits_sortida
        EbN0 = 10**(2 / 10)
        self.noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

    def forward(self, x):
        mig = self.enc(x)
        mig = 2 * torch.sigmoid(10 * mig) - 1
        mig_noise = mig + self.noiseSigma * torch.randn(mig.shape)
        x = self.dec(mig_noise.float())
        return x, mig, mig_noise


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')

    bits_entrada = 9
    bits_mig = 27

    torch.manual_seed(0)
    # mod_enc = Encoder(bits_entrada, 6)
    # mod_enc.to(device)
    # mod_dec = Decoder(6, num_etiquetes)
    # mod_dec.to(device)
    autoenc = AutoEncoder()
    num_etiquetes = 2**bits_entrada
    #optim_enc = torch.optim.SGD(mod_enc.parameters(), lr=1e-3, weight_decay=1e-5)
    #optim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-5)
    optim_auto = torch.optim.SGD(autoenc.parameters(),
                                 lr=1e-3,
                                 weight_decay=1e-5)

    criterion = BCELoss()
    loss_detall = BCELoss(reduction='none')

    ordre = list(range(num_etiquetes))
    # Per cada etiqueta he de fer el seu vector d'entrada
    entrada = []
    for element in ordre:
        st = ''
        # He d'afegir tants 0 al davant com longitud
        for k in format(element, "b").zfill(bits_entrada):
            st = st + k + ','
        entrada.append(eval('[' + st + ']'))
    entrada_torch = torch.Tensor(entrada) * 2 - 1
    entrada_torch = entrada_torch.view([num_etiquetes, bits_entrada, 1])

    f = open("debugant.log", "w")
    for epoch in range(167500):
        autoenc.train()

        if (epoch % 50) == 0:
            f.write("\n**************\nEpoch: " + str(epoch) +
                    "\n**************\n")

        # optim_enc.zero_grad()
        # optim_dec.zero_grad()
        optim_auto.zero_grad()

        seq_true = (entrada_torch.view(num_etiquetes, 9) + 1) / 2
        seq_pred, mig, mig_noise = autoenc(entrada_torch)

        loss = criterion(seq_pred.view(num_etiquetes, 9), seq_true)
        # L'entrada al BCE és (N,*) on N és el tamany del batch

        loss.backward()
        # optim_enc.step()
        # optim_dec.step()
        optim_auto.step()

        if (epoch % 50) == 0:
            f.write("\n***\nSeq. entrada: " + str(seq_true.detach().numpy()))
            f.write("\nMig: " + str(mig.detach().numpy()))
            f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            f.write("\nSeq. predita: " + str(seq_pred.detach().numpy()))
            perdua = loss_detall(seq_pred.view(num_etiquetes, 9), seq_true)
            f.write("\nPèrdua: " + str(perdua))
            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:' +
                        str(round(torch.cuda.memory_allocated(0) /
                                  1024**3, 1)) + 'GB')
                f.write('\nCached:   ' +
                        str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                        'GB')

            print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
    f.close()

    torch.save(
        {
            'epoch': epoch,
            'model_state_dict': autoenc.state_dict(),
            'optimizer_state_dict': optim_auto.state_dict(),
            'loss': loss,
            'bits_entrada': bits_entrada,
            'bits_mig': bits_mig,
        }, "AutoEnc_conc.pth")

    # Mirem el BER, l'anterior era l'error d'entrenament per trobar un codi.
    # Fins i tot podria entrenar el decoder perquè milloris amb les diferents entrades
    # autoenc = AutoEncoder(3, 3, 6)
    # optim_auto = torch.optim.SGD(autoenc.parameters(), lr=1e-3, weight_decay=1e-5)

    # checkpoint = torch.load("CrossEnt_embed.pth")
    # autoenc.load_state_dict(checkpoint['model_state_dict'])
    # optim_auto.load_state_dict(checkpoint['optimizer_state_dict'])

    # # Generem l'útlima sortida
    # ordre = list(range(num_etiquetes))
    # seq_true = torch.Tensor(ordre).view(num_etiquetes, 1).long()
    # seq_pred, mig, mig_noise = autoenc(seq_true)
    # dades = (mig > 0.5) * 2 - 1

    # errors = 0
    # for k in range(1000):
    #     dades_noise = dades + autoenc.noiseSigma * torch.randn(dades.shape)

    #     seq_pred = autoenc.dec(dades_noise)
    #     val, ind = torch.max(seq_pred, dim=2)
    #     for eti in range(num_etiquetes):
    #         if ind[eti] != eti:
    #             errors = errors + 1
    # print("Errors: " + str(errors) + " de 1000 words")
