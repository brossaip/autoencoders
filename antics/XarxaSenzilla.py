# Començaré amb una xarxa senzilla com l'exemple https://www.bloggerdrive.com/pytorch-tutorial-understanding-and-implimanting-autoencoders/
# Però utilitzaré un RNN
# Primer ho faré amb un optimitzador de MSE i després miraré amb un softmax per classificar les categories.
# Tinc complicat enviar el hidden. Buscada informació per internet he trobat:
# Té tot d'exemples fet en pytorch https://www.curiousily.com/posts/time-series-anomaly-detection-using-lstm-autoencoder-with-pytorch-in-python/ i el seu exemple està extret de la implementació de https://github.com/shobrook/sequitur/tree/master/sequitur/autoencoders

import numpy as np
import matplotlib.pyplot as plt
from pandas import read_csv
import pandas as pd
import math
import torch.nn as nn
import torch
import torch.optim as optim


class Sequence(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers, output_size):
        # El meu input_size és 1 que són les carcaterístiques d'entrada. Si faig tots els chips alhora serien 7
        super().__init__()
        self.hidden_size = hidden_size
        self.input_size = input_size
        self.num_layers = num_layers
        self.rnn = nn.RNN(input_size,
                          hidden_size,
                          num_layers,
                          batch_first=True)
        self.output = nn.Linear(hidden_size, output_size)

    def forward(self, input, hidden):
        out, hidden = self.rnn(input.float(), hidden)
        out = self.output(out)
        return out, hidden


# El detector és igual que el Generador però amb els tamanys invertits


class AutoEncoder(nn.Module):
    def __init__(self):
        super().__init__()
        self.encoder = Sequence(2, 2, 2, 4)
        self.decoder = Sequence(4, 2, 2, 2)

    def forward(self, x, hidden_enc, hidden_dec):
        out_enc, hidden_enc = self.encoder(x, hidden_enc)
        print("**** Input: " + str(x))
        print("**** Out encoder: " + str(out_enc))
        print("**** Hidden encoder: " + str(hidden_enc))
        # Aquí li hauria d'afegir el soroll
        # I abans normalitzar els bits per -1 i 1
        out_dec, hidden_dec = self.decoder(out_enc, hidden_dec)
        print("**** Out decoder: " + str(out_dec))
        print("**** Hidden decoder: " + str(hidden_dec))


input = torch.tensor([[-1., -1.], [-1., 1.], [1., -1.], [1., 1.]])
model = AutoEncoder()

np.random.seed(0)
torch.manual_seed(0)

loss_function = nn.MSELoss()
optimizer = optim.Adam(model.parameters(), weight_decay=1e-5)
