# Basat en el CrossEnt_RNN però ara amb xarxes convolucionals

# Third Party
import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss, MSELoss
from random import shuffle

############
# COMPONENTS
############


class Encoder(nn.Module):
    def __init__(self, num_features, embedding_dim=64):
        super(Encoder, self).__init__()

        self.num_features = num_features
        self.embedding_dim = embedding_dim

        self.cnn1 = nn.Conv1d(
            in_channels=num_features,
            out_channels=embedding_dim,
            kernel_size=1,  # Kernel_size can't be greater than input size
            stride=1)

    def forward(self, x):
        x = self.cnn1(x)
        return (x)


class Decoder(nn.Module):
    def __init__(self, input_dim=64, output_dim=1):
        super(Decoder, self).__init__()

        self.input_dim = input_dim
        self.output_dim = output_dim

        self.cnn1 = nn.Conv1d(in_channels=input_dim,
                              out_channels=input_dim,
                              kernel_size=1,
                              stride=1)
        self.out = nn.Linear(self.input_dim, output_dim)

    def forward(self, x):
        x = self.cnn1(x)
        x = self.out(
            x.view(1, 1, self.input_dim)
        )  # Té dimensions (N,*,H_in). Diria que N són el nombre de samples, * qualsevol cosa i H_in el número de features de l'entrada
        return x


#########
# EXPORTS
#########


class ConvAE(nn.Module):
    def __init__(self, num_features, embedding_dim, num_etiquetes):
        # seq_len seria el número d'elements de mostra
        # num_features són les característiques, les dimensions d'entrada.
        super(ConvAE, self).__init__()

        self.num_features = num_features
        self.embedding_dim, self.num_etiquetes = embedding_dim, num_etiquetes

        self.encoder = Encoder(num_features, embedding_dim)
        self.decoder = Decoder(embedding_dim, num_etiquetes)

    def forward(self, x):
        mig = self.encoder(x)
        mig_noise = (mig > 0) * 2 - 1
        # mig_noise = mig_noise + torch.randn(mig.shape)
        x = self.decoder(mig_noise)

        return x, mig


torch.manual_seed(0)
model = ConvAE(3, 30, 8)

optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)
criterion = CrossEntropyLoss(weight=torch.Tensor([1e5, 1, 1, 1, 1, 1, 1, 2]))

input = torch.tensor([[1., 1., -1.], [-1., -1., 1.], [-1., 1., 1.],
                      [-1., -1., -1.], [-1., 1., -1.], [1., -1., -1.],
                      [1., -1., 1.], [1., 1., 1.]])
# He pensat que potser s'encallava en alguns valors perquè els epochs eren sempre iguals, ara variaré l'ordre
ordre = list(range(8))
x = input.view(8, 1, 3)

losses, embeddings, aproximacions = [], [], []

for epoch in range(10):
    model.train()
    shuffle(ordre)
    for batch in ordre:
        seq_true = x[batch]
        seq_true = seq_true.view(
            1, 3, 1
        )  # NNN is a batch size, CCC denotes a number of channels, LLL is a length of signal sequence
        optimizer.zero_grad()
        seq_pred, mig = model(seq_true)

        # Passem les etiquetes esperades, les de seq_true
        seq_true_bits = (seq_true + 1) / 2
        etiquetes_true = torch.LongTensor([0])
        for k in range(len(seq_true_bits)):
            etiquetes_true = etiquetes_true + ((2**k) * seq_true_bits[0][0][k])
        loss = criterion(
            seq_pred.view(1, 8), etiquetes_true.long()
        )  # L'entrada al crossEntropy és (N,C) on N és el tamany del batch (1 en el nostre cas) i C el resultat per cada classe

        loss.backward()
        optimizer.step()

        losses.append(loss.item())
        embeddings.append(mig)
        aproximacions.append(seq_pred)

    print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))

# Model generat
for k in range(len(input)):
    entrada = x[k]
    etiqueta_ent = 0
    entrada_bit = (x[k] + 1) / 2
    for l in range(len(entrada[0])):
        etiqueta_ent = etiqueta_ent + ((2**l) * entrada_bit[0][l])
    seq_pred, mig = model(entrada.view(1, 3, 1))
    loss = criterion(seq_pred.view(1, 8), etiqueta_ent.view(1).long())
    # print("*** {};{} - {} \n- {} - {}".format(str(entrada), str(etiqueta_ent),
    #                                          str(mig), str(seq_pred), str(loss)))
    print("*** {};{} \n- {} - {}".format(str(entrada), str(etiqueta_ent),
                                         str(seq_pred), str(loss)))

# En aquesta primera versió, augmenta els errors dels que no estan. Potser perquè l'estructura és massa senzilla i es necessitaria fer-la més complexa.
# He augmentat la complexitat però també dóna pes a dues classes. Potser hem de balancejar els pesos per donar altres valors a altres casos.
