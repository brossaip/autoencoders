# Carrego el model entrenat i entreno només la part de l'encoder

import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss, MSELoss
from random import shuffle
import math

############
# COMPONENTS
############

class Encoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida):
        super(Encoder, self).__init__()

        self.bits_entrada = bits_entrada
        self.bits_sortida = bits_sortida

        self.embd = nn.Embedding(2**bits_entrada, bits_sortida)
        self.lin1 = nn.Linear(bits_sortida, bits_sortida)
        # La convolució ha de ser amb diferents canals i la longitud L (N, C_in, L_in) on N és el tamany de batch, C_in són els canals d'entrada i L_in la longitud. A la fórmula de la documentació no veig L, però sí C_in i C_out, L potser està en la convolució del pes per l'input.
        kernel_size = 2
        self.cnn1 = nn.Conv1d(
            in_channels=1,
            out_channels=1,
            kernel_size=
            kernel_size,  # Kernel_size can't be greater than input size
            stride=1)
        # Després del lineal, tinc una seqüència amb més bits, però segueix sent unidimensional, d'un sol canal, per això el Conv1d té in_channels=1
        self.lin2 = nn.Linear(bits_sortida - kernel_size + 1, bits_sortida*2)
        self.cnn2 = nn.Conv1d(
            in_channels=1,
            out_channels=1,
            kernel_size=kernel_size + 1,
            stride=2)
        self.lin3 = nn.Linear(bits_sortida - kernel_size + 1, bits_sortida)

    def forward(self, x):
        x = self.embd(x)
        x = self.lin1(x)
        x = self.cnn1(x)
        x = self.lin2(x)
        x = self.cnn2(x)
        x = self.lin3(x)

        return (x)


class Decoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida):
        super(Decoder, self).__init__()

        self.lin1 = nn.Linear(bits_entrada, bits_entrada)
        kernel_size = 2
        self.cnt1 = nn.ConvTranspose1d(in_channels=1,
                                       out_channels=1,
                                       kernel_size=kernel_size+1,
                                       stride=2)
        # L_out = (L_in-1)*stride - 2*padding + dilation*(kernel_size-1) + output_padding+1
        self.lin2 = nn.Linear( (bits_entrada-1)*2 + kernel_size+1, bits_sortida*2)
        self.cnt2 = nn.ConvTranspose1d(in_channels=1,
                                       out_channels=1,
                                       kernel_size=kernel_size,
                                       stride=1)
        self.out = nn.Linear(bits_entrada + kernel_size - 1, 2**bits_sortida)

    def forward(self, x):
        x = self.lin1(x)
        x = self.cnt1(x)
        x = self.lin2(x)
        x = self.cnt2(x)
        x = self.out(
            x
        )  # Té dimensions (N,*,H_in). Diria que N són el nombre de samples, * qualsevol cosa i H_in el número de features de l'entrada
        return x


class AutoEncoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida, bits_mig):
        super(AutoEncoder, self).__init__()

        self.enc = Encoder(bits_entrada, bits_mig)
        self.dec = Decoder(bits_mig, bits_sortida)
        self.tanh = nn.Tanh()
        self.thresh = nn.Threshold(0, -1)

        Rm = math.log2(2)
        Rc = bits_entrada*1.0/bits_sortida
        EbN0 = 10**(2/10)
        self.noiseSigma = math.sqrt(1/(2*Rm*Rc*EbN0))

    def forward(self, x):
        mig = self.enc(x)
        mig_noise = 2*torch.sigmoid(10*mig)-1
        #mig_noise = mig_noise + self.noiseSigma*torch.randn(mig_noise.shape)
        x = self.dec(mig_noise.float())
        return x, mig, mig_noise



device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
device = torch.device('cpu')
autoenc = AutoEncoder(3, 3, 6)
optim_auto = torch.optim.SGD(autoenc.parameters(), lr=1e-3, weight_decay=1e-5)

checkpoint = torch.load("CrossEnt_Embed_2conv1d.pth")
autoenc.load_state_dict(checkpoint['model_state_dict'])
optim_auto.load_state_dict(checkpoint['optimizer_state_dict'])
#loss_autoencoder = torch.load(checkpoint['loss'])

# Generem l'útlima sortida
num_etiquetes = 2**3
ordre = list(range(num_etiquetes))
seq_true = torch.Tensor(ordre).view(num_etiquetes, 1).long()
seq_pred, mig, mig_noise = autoenc(seq_true)
dades = (mig > 0.5) * 2 - 1

# Preparem per entrenar el decoder
mod_dec = autoenc.dec
optim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-5, weight_decay=1e-5)
criterion = CrossEntropyLoss()
loss_detall = CrossEntropyLoss(reduction='none')

bits_entrada = 3
bits_sortida = 6
Rm = math.log2(2)
Rc = bits_entrada * 1.0 / bits_sortida
EbN0 = 10**(8 / 10)
noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

f=open("debugant_dec_2cnv1d.log","w")
errors = 0
for epoch in range(10500):
    mod_dec.train()
    if (epoch % 50) == 0:
        f.write("\n**************\nEpoch: " + str(epoch) +
                "\n**************\n")

    optim_dec.zero_grad()

    seq_true = torch.Tensor(ordre).view(num_etiquetes, 1).long()
    entrada = dades + noiseSigma* torch.randn(dades.shape)
    entrada.to(device)

    seq_pred = mod_dec(entrada)

    loss = criterion(
        seq_pred.view(num_etiquetes, 8), seq_true.view(8)
    )  # L'entrada al crossEntropy és (N,C) on N és el tamany del batch (1 en el nostre cas) i C el resultat per cada classe

    loss.backward()
    optim_dec.step()

    val, ind = torch.max(seq_pred, dim=2)
    for eti in range(num_etiquetes):
        if ind[eti] != eti:
            errors = errors + 1

    if (epoch % 50) == 0:
        f.write("\n***\nSeq. entrada: " + str(seq_true.detach().numpy()))
        f.write("\nMig: " + str(dades.detach().numpy()))
        f.write("\nMig Noise: " + str(entrada.detach().numpy()))
        f.write("\nSeq. predita: " + str(seq_pred.detach().numpy()))
        perdua = loss_detall(seq_pred.view(num_etiquetes, 8), seq_true.view(8))
        f.write("\nPèrdua: " + str(perdua))
        f.write("\nWord errors: " + str(errors) + " de " + str((epoch+1)*8) + " ; Percentatge: " + str(errors/(epoch+1)/8*100))
        if device.type == 'cuda':
            f.write(torch.cuda.get_device_name(0))
            f.write('\nMemory Usage:')
            f.write('\nAllocated:' +
                    str(round(torch.cuda.memory_allocated(0) / 1024**3, 1)) +
                    'GB')
            f.write('\nCached:   ' +
                    str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                    'GB')

        print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
f.close()

print("Word errors: " + str(errors) + " de " + str((epoch+1)*8) + " ; Percentatge: " + str(errors/(epoch+1)/8*100))
