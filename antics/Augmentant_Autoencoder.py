# Carrego el model entrenat i li afegeixo una nova capa i augmento el soroll de 8dB

import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss, MSELoss
from random import shuffle
import math
import numpy as np
import CrossEnt_Embed_cnv1d as model

##########
# Nova etapa de la xarxa neuronal
##########
class Enc_Etapa(nn.Module):
    def __init__(self, bits_entrada, bits_sortida):
        super(Enc_Etapa, self).__init__()

        self.bits_entrada = bits_entrada
        self.bits_sortida = bits_sortida

        self.lin1 = nn.Linear(bits_entrada, 2*bits_sortida)
        # La convolució ha de ser amb diferents canals i la longitud L (N, C_in, L_in) on N és el tamany de batch, C_in són els canals d'entrada i L_in la longitud. A la fórmula de la documentació no veig L, però sí C_in i C_out, L potser està en la convolució del pes per l'input.
        kernel_size = 3
        self.cnn1 = nn.Conv1d(
            in_channels=1,
            out_channels=1,
            kernel_size=
            kernel_size,  # Kernel_size can't be greater than input size
            stride=1)
        # Després del lineal, tinc una seqüència amb més bits, però segueix sent unidimensional, d'un sol canal, per això el Conv1d té in_channels=1
        # L_out = \frac{L_in + 2*padding -dilation*(kernel_size-1)-1}{stride} + 1
        self.lin2 = nn.Linear(2*bits_sortida - kernel_size + 1, bits_sortida)

    def forward(self, x):
        x = self.lin1(x)
        x = self.cnn1(x)
        x = self.lin2(x)

        return (x)


class Dec_Etapa(nn.Module):
    def __init__(self, num_etiquetes, bits_mig):
        super(Dec_Etapa, self).__init__()

        self.lin1 = nn.Linear(num_etiquetes, bits_mig)
        kernel_size = 3
        self.cnt1 = nn.ConvTranspose1d(in_channels=1,
                                       out_channels=1,
                                       kernel_size=kernel_size,
                                       stride=1)
        # L_out = (L_in-1)*stride - 2*padding + dilation*(kernel_size-1) + output_padding+1
        self.out = nn.Linear(bits_mig-1 +kernel_size -1 + 1, num_etiquetes)

    def forward(self, x):
        x = self.lin1(x)
        x = self.cnt1(x)
        x = self.out(x)

        return x

if __name__ == '__main__':
    ############
    # Antic mòdul
    ############
    bits_entrada = 4
    bits_sortida = 12
    num_etiquetes = 2**bits_entrada

    checkpoint = torch.load("CrossEnt_Embed_Basic.pth")
    bits_entrada = checkpoint['bits_entrada']
    bits_sortida = checkpoint['bits_sortida']

    mod_enc = model.Encoder(bits_entrada, bits_sortida)
    mod_dec = model.Decoder(bits_sortida, bits_entrada)

    mod_enc.load_state_dict(checkpoint['encoder_state_dict'])
    mod_dec.load_state_dict(checkpoint['decoder_state_dict'])
    # Els traiem de l'optimització perquè no variïn
    # mod_enc.embd.weight.requires_grad=False
    # mod_enc.lin1.weight.requires_grad=False
    # mod_enc.cnn1.weight.requires_grad=False
    # mod_enc.lin2.weight.requires_grad=False
    # mod_dec.lin1.weight.requires_grad=False
    # mod_dec.cnt1.weight.requires_grad=False
    # mod_dec.out.weight.requires_grad=False
    # No fa falta treure'ls perquè a l'optimització diem només els pesos que han de canviar


    # Nova etapa
    enc_etapa = Enc_Etapa(bits_sortida, bits_sortida)
    dec_etapa = Dec_Etapa(num_etiquetes, 2*bits_sortida)

    # Generem l'última sortida
    ordre = list(range(num_etiquetes))
    seq_true = torch.Tensor(ordre).view(num_etiquetes, 1).long()
    mig = mod_enc(seq_true)
    dades = (mig > 0.5) * 2 - 1

    # Preparem per entrenar el decoder
    criterion = CrossEntropyLoss()
    loss_detall = CrossEntropyLoss(reduction='none')

    Rm = math.log2(2)
    Rc = bits_entrada * 1.0 / bits_sortida
    EbN0 = 10**(10 / 10)
    noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

    device = torch.device('cpu')
    enc_etapa.to(device)
    dec_etapa.to(device)

    optim_enc = torch.optim.SGD(enc_etapa.parameters(), lr=1e-3, weight_decay=1e-5)
    optim_dec = torch.optim.Adam(dec_etapa.parameters(), lr=1e-3, weight_decay=1e-5)

    criterion = CrossEntropyLoss()
    loss_detall = CrossEntropyLoss(reduction='none')

    losses = []
    f=open("debugant_etapa.log","w")
    for epoch in range(10500):
        if (epoch % 50) == 0:
            f.write("\n**************\nEpoch: " + str(epoch) +
                    "\n**************\n")

        enc_etapa.train()
        dec_etapa.train()

        optim_enc.zero_grad()
        optim_dec.zero_grad()

        entrada = dades.float()
        entrada.to(device)

        # Dades nova etapa encoder i nova decoder
        mig = enc_etapa(entrada)
        mig_noise = 2*torch.sigmoid(10*mig)-1
        mig_noise = mig_noise + noiseSigma*torch.randn(mig_noise.shape).to(device)
        mig_noise = mig_noise.float()
        seq_pred_abansEtapa = mod_dec(mig_noise)
        seq_pred = dec_etapa(seq_pred_abansEtapa)

        loss = criterion(
            seq_pred.view(num_etiquetes, num_etiquetes), seq_true.view(num_etiquetes)
        )  # L'entrada al crossEntropy és (N,C) on N és el tamany del batch (1 en el nostre cas) i C el resultat per cada classe

        loss.backward()
        losses.append(loss.item())

        optim_dec.step()
        optim_enc.step()
        
        if (epoch % 50) == 0:
            f.write("\n***\nSeq. entrada: " + str(entrada.detach().numpy()))
            f.write("\nMig: " + str(mig.detach().numpy()))
            f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            f.write("\nSeq. predita: " + str(seq_pred.detach().numpy()))
            perdua = loss_detall(seq_pred.view(num_etiquetes, num_etiquetes), seq_true.view(num_etiquetes))
            f.write("\nPèrdua: " + str(perdua))
            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:' +
                        str(round(torch.cuda.memory_allocated(0) / 1024**3, 1)) +
                        'GB')
                f.write('\nCached:   ' +
                        str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                        'GB')

            print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
    f.close()

    # He de gravar el model
    np.savetxt("losses.csv",losses, delimiter=",")
    torch.save(
        {
            'encoder_state_dict': enc_etapa.state_dict(),
            'decoder_state_dict': dec_etapa.state_dict(),
            'optimizer_encoder_state_dict': optim_enc.state_dict(),
            'optimizer_decoder_state_dict': optim_dec.state_dict(),
            'loss': loss,
            'bits_entrada': bits_entrada,
            'bits_sortida': bits_sortida,
        }, "Nova_etapa_autoencoder.pth")

    # Comprovar si el de només 1 convolucional, amb aquests EbN0 donava els mateixos errors o no
