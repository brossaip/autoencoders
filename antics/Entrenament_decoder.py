# Carrego el model entrenat i entreno només la part de l'encoder

import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss, MSELoss
from random import shuffle
import math
import CrossEnt_Embed_2cnv1d as model

############
# COMPONENTS
############
bits_entrada = 3
bits_sortida = 9

checkpoint = torch.load("CrossEnt_Embed_2conv1d.pth")
bits_entrada = checkpoint['bits_entrada']
bits_sortida = checkpoint['bits_sortida']
tam_lvl1 = checkpoint['tam_lvl1']
tam_lvl2 = checkpoint['tam_lvl2']
tam_lvl3 = checkpoint['tam_lvl3']

mod_enc = model.Encoder(bits_entrada, bits_sortida, bits_sortida, bits_sortida, bits_sortida)
mod_dec = model.Decoder(bits_sortida, bits_entrada, bits_sortida, bits_sortida, bits_sortida)
optim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-4)
optim_enc = torch.optim.SGD(mod_enc.parameters(), lr=1e-3, weight_decay=1e-4)


mod_enc.load_state_dict(checkpoint['encoder_state_dict'])
mod_dec.load_state_dict(checkpoint['decoder_state_dict'])
optim_enc.load_state_dict(checkpoint['optimizer_encoder_state_dict'])
optim_dec.load_state_dict(checkpoint['optimizer_decoder_state_dict'])
#loss_autoencoder = torch.load(checkpoint['loss'])

# Generem l'útlima sortida
num_etiquetes = 2**3
ordre = list(range(num_etiquetes))
seq_true = torch.Tensor(ordre).view(num_etiquetes, 1).long()
mig = mod_enc(seq_true)
dades = (mig > 0.5) * 2 - 1

# Preparem per entrenar el decoder
criterion = CrossEntropyLoss()
loss_detall = CrossEntropyLoss(reduction='none')

Rm = math.log2(2)
Rc = bits_entrada * 1.0 / bits_sortida
EbN0 = 10**(8 / 10)
noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

device = torch.device('cpu')

f=open("debugant_dec.log","w")
errors = 0
for epoch in range(10500):
    mod_dec.train()
    if (epoch % 50) == 0:
        f.write("\n**************\nEpoch: " + str(epoch) +
                "\n**************\n")

    optim_dec.zero_grad()

    seq_true = torch.Tensor(ordre).view(num_etiquetes, 1).long()
    entrada = dades + noiseSigma* torch.randn(dades.shape)
    entrada.to(device)

    seq_pred = mod_dec(entrada)

    loss = criterion(
        seq_pred.view(num_etiquetes, 8), seq_true.view(8)
    )  # L'entrada al crossEntropy és (N,C) on N és el tamany del batch (1 en el nostre cas) i C el resultat per cada classe

    loss.backward()
    optim_dec.step()

    val, ind = torch.max(seq_pred, dim=2)
    for eti in range(num_etiquetes):
        if ind[eti] != eti:
            errors = errors + 1

    if (epoch % 50) == 0:
        f.write("\n***\nSeq. entrada: " + str(seq_true.detach().numpy()))
        f.write("\nMig: " + str(dades.detach().numpy()))
        f.write("\nMig Noise: " + str(entrada.detach().numpy()))
        f.write("\nSeq. predita: " + str(seq_pred.detach().numpy()))
        perdua = loss_detall(seq_pred.view(num_etiquetes, 8), seq_true.view(8))
        f.write("\nPèrdua: " + str(perdua))
        f.write("Word errors: " + str(errors) + " de " + str((epoch+1)*8) + " ; Percentatge: " + str(errors/(epoch+1)/8*100))
        if device.type == 'cuda':
            f.write(torch.cuda.get_device_name(0))
            f.write('\nMemory Usage:')
            f.write('\nAllocated:' +
                    str(round(torch.cuda.memory_allocated(0) / 1024**3, 1)) +
                    'GB')
            f.write('\nCached:   ' +
                    str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                    'GB')

        print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
f.close()

print("Word errors: " + str(errors) + " de " + str((epoch+1)*8) + " ; Percentatge: " + str(errors/(epoch+1)/8*100))
