# Basat en el rae de sequitur https://github.com/shobrook/sequitur/blob/master/sequitur/autoencoders/rae.py
# Pel tema del crossEntropy he seguit el video https://www.youtube.com/watch?v=7q7E91pHoW4
# Com que ha de ser multiclasse en posarem 3 bits.
# Quan valorem el crossEntropy, necessitem que la sortida indiqui el número de classes, una sortida lineal, tal i com indica la pàg 73 del Pytorch Recipes
# Per tant les etiquetes seran un vector de 8 posicions amb 1 indicant el valor

# En anar treballant amb sequitur, he vist que feia coses que jo no entenia. Intentaré fer la versió amb RNN que ja els he treballat i el que jo entenc dels autoencoders

# Third Party
import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss, MSELoss

############
# COMPONENTS
############


class Encoder(nn.Module):
    def __init__(self, seq_len, num_features, embedding_dim=64):
        super(Encoder, self).__init__()

        self.seq_len, self.num_features = seq_len, num_features
        self.embedding_dim = embedding_dim

        self.rnn1 = nn.RNN(input_size=num_features,
                           hidden_size=self.embedding_dim,
                           num_layers=2,
                           batch_first=True)

    def forward(self, x, hidden):
        x, hidden = self.rnn1(x, hidden)
        hidden.detach_()
        hidden = hidden.detach()
        return (x, hidden)


class Decoder(nn.Module):
    def __init__(self, seq_len, input_dim=64, output_dim=1):
        super(Decoder, self).__init__()

        self.seq_len, self.input_dim = seq_len, input_dim
        self.hidden_dim, self.output_dim = 2 * input_dim, output_dim

        self.rnn1 = nn.RNN(input_size=input_dim,
                           hidden_size=input_dim,
                           num_layers=1,
                           batch_first=True)
        self.out = nn.Linear(self.input_dim, output_dim)

    def forward(self, x, hidden):
        x, hidden = self.rnn1(x, hidden)
        hidden.detach_()
        hidden = hidden.detach()
        x = self.out(x)
        return x, hidden


#########
# EXPORTS
#########


class RAE(nn.Module):
    def __init__(self, seq_len, num_features, embedding_dim, num_etiquetes):
        # seq_len seria el número d'elements de mostra
        # num_features són les característiques, les dimensions d'entrada.
        super(RAE, self).__init__()

        self.seq_len, self.num_features = seq_len, num_features
        self.embedding_dim, self.num_etiquetes = embedding_dim, num_etiquetes

        self.encoder = Encoder(seq_len, num_features, embedding_dim)
        self.decoder = Decoder(seq_len, embedding_dim, num_etiquetes)

    def forward(self, x, hidden_enc, hidden_dec):
        mig, hidden_enc = self.encoder(x, hidden_enc)
        x, hidden_dec = self.decoder(mig, hidden_dec)

        return x, mig, hidden_enc, hidden_dec


torch.manual_seed(0)
model = RAE(1, 3, 6, 8)

optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)
criterion = CrossEntropyLoss()

input = torch.tensor([[-1., -1., -1.], [-1., 1., -1.], [1., -1., -1.],
                      [1., 1., -1.], [-1., -1., 1.], [-1., 1., 1.],
                      [1., -1., 1.], [1., 1., 1.]])
x = input.view(8, 1, 3)

losses, embeddings, aproximacions = [], [], []
for epoch in range(350):
    model.train()
    hidden_enc = None
    hidden_dec = None
    for batch in range(len(input)):
        seq_true = x[batch]
        seq_true = seq_true.view(1, 1, 3)
        optimizer.zero_grad()
        seq_pred, mig, hidden_enc, hidden_dec = model(seq_true, hidden_enc,
                                                      hidden_dec)

        # Passem les etiquetes esperades, les de seq_true
        seq_true_bits = (seq_true + 1) / 2
        etiquetes_true = torch.LongTensor([0])
        for k in range(len(seq_true_bits)):
            etiquetes_true = etiquetes_true + ((2**k) * seq_true_bits[0][0][k])
        loss = criterion(
            seq_pred.view(1, 8), etiquetes_true.long()
        )  # L'entrada al crossEntropy és (N,C) on N és el tamany del batch (1 en el nostre cas) i C el resultat per cada classe

        loss.backward(retain_graph=True)
        optimizer.step()

        losses.append(loss.item())
        embeddings.append(mig)
        aproximacions.append(seq_pred)

    print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))

# Model generat
for k in range(len(input)):
    entrada = x[k]
    etiqueta_ent = 0
    entrada_bit = (x[k] + 1) / 2
    for k in range(len(entrada[0])):
        etiqueta_ent = etiqueta_ent + ((2**k) * entrada_bit[0][k])
    seq_pred, mig, _, _ = model(entrada.view(1, 1, 3), hidden_enc, hidden_dec)
    loss = criterion(seq_pred.view(1, 8), etiqueta_ent.view(1).long())
    print("*** {};{} - {} - {}".format(str(entrada), str(etiqueta_ent),
                                       str(mig), str(seq_pred)))

# Tal i com l'he generat, aquest model té problemes amb el hidden. Miraré de crear un convolutional net que a la llarga pot fer com un RNN crec haver llegit
# https://stackoverflow.com/questions/48274929/pytorch-runtimeerror-trying-to-backward-through-the-graph-a-second-time-but

# No té sentit RNN amb tots els vectors d'entrada, ja que no sé mai quin estat estarà, hauria de resetejar a cada entrada de 3 bits. Hauré de mirar el convolucional
