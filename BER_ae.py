# Carrego el model entrenat del concatenat i calculo el BER

import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss, MSELoss
from random import shuffle
import math
import AE_conv_Lin as model

############
# COMPONENTS
############

checkpoint = torch.load("AE_conv_Lin_ep7500_01_01.pth")
bits_entrada = checkpoint['bits_entrada']
bits_mig = checkpoint['bits_mig']

autoenc = model.AutoEncoder(bits_entrada, bits_mig)
autoenc.load_state_dict(checkpoint['model_state_dict'])

num_etiquetes = 2**bits_entrada
ordre = list(range(num_etiquetes))
# Per cada etiqueta he de fer el seu vector d'entrada
entrada = []
for element in ordre:
    st = ''
    # He d'afegir tants 0 al davant com longitud
    for k in format(element, "b").zfill(bits_entrada):
        st = st + k + ','
    entrada.append(eval('[' + st + ']'))
entrada_torch = torch.Tensor(entrada) * 2 - 1
entrada_torch = entrada_torch.view([num_etiquetes, 1, bits_entrada])

errors = 0
iteracions = int(1e4)
for k in range(iteracions):
    seq_pred, mig, mig_noise = autoenc(entrada_torch)
    res = (seq_pred > 0.5) * 2 - 1
    errors = errors + torch.sum(entrada_torch != res)
    if k % 1000 == 0:
        print(str(errors) + " : " + str(k))
print("Error total " + str(errors.detach().numpy() /
                           (num_etiquetes * bits_entrada * iteracions)))
