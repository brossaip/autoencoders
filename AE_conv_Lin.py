# Vull fer un cas senzill concatenant convolucions amb kernel de tamany gradual i lineals que ofereixen l'augment perdut per la convolució amb no linealitat activa per fer que tinguin el mateix tamany i es puguin crear la dimensionalitat extra.
# També podria crear aquesta dimensionalitat amb el convTranspose1d

# Third Party
import torch
import torch.nn as nn
from torch.nn import BCELoss
import math

############
# COMPONENTS
############


class Encoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida):
        super(Encoder, self).__init__()

        self.bits_entrada = bits_entrada
        self.bits_sortida = bits_sortida
        self.array_cnv1d = nn.ModuleList()
        self.array_lin = nn.ModuleList()
        for k in range(self.bits_entrada - 1):
            cnv_temp = nn.Conv1d(in_channels=1,
                                 out_channels=1,
                                 kernel_size=k + 1,
                                 bias=False)
            cnv_temp.weight.data = torch.ones(k + 1).view(1, 1, k + 1)
            self.array_cnv1d.append(cnv_temp)
            lin_temp = nn.Linear(self.bits_entrada - k,
                                 self.bits_sortida,
                                 bias=False)
            # Iniciaitzem els pesos de l'array_lin. Els pesos tenen la forma inversa de Linear
            inici = []
            for j in range(int(self.bits_sortida / (self.bits_entrada - k))):
                inici.append(torch.eye(self.bits_entrada - k))
            lin_temp.weight.data = torch.cat(inici, 0)
            lin_temp.weight.data = lin_temp.weight.data + 0.1 * torch.rand(
                lin_temp.weight.data.shape)

            self.array_lin.append(lin_temp)

        # L_out = \frac{L_in + 2*padding -dilation*(kernel_size-1)-1}{stride} + 1
        # L_out del primer és 9-2+1-1+1=8, segon és 8-3+1=6 i el tercer és 5+4padding.
        self.lin_fin = nn.Linear((self.bits_entrada - 1) * bits_sortida - 1,
                                 bits_sortida,
                                 bias=False)
        self.lin_fin.weight.data = torch.eye(
            bits_sortida, (bits_entrada - 1) * bits_sortida - 1)
        self.lin_fin.weight.data = self.lin_fin.weight.data + 0.1 * torch.rand(
            self.lin_fin.weight.data.shape)

    def forward(self, x):
        x_array = []
        for k in range(self.bits_entrada - 1):
            x_array.append(self.array_lin[k](self.array_cnv1d[k](x)))

        # Ara les ha d'agrupar com canals i passar de tots els diferents canals a 1 de sol
        x = torch.cat(x_array, 2)
        # print("**** concat shape: " + str(x.shape))
        x = self.lin_fin(x)
        return (x)


class Decoder(nn.Module):
    def __init__(self, bits_entrada, bits_sortida):
        super(Decoder, self).__init__()

        # Els bits entrada i sortida són de més gran a més petit, al revés que l'encoder
        self.bits_entrada = bits_entrada
        self.bits_sortida = bits_sortida

        # Perquè els arrays apareguin he de crear un moduleList
        self.array_lin = nn.ModuleList()
        self.array_cnv1d = nn.ModuleList()
        for k in range(self.bits_entrada - 1):
            cnv = nn.Conv1d(in_channels=1, out_channels=1, kernel_size=k + 1)
            lin = nn.Linear(self.bits_entrada - k, self.bits_sortida)
            self.array_lin.append(lin)
            self.array_cnv1d.append(cnv)
        # L_out = \frac{L_in + 2*padding -dilation*(kernel_size-1)-1}{stride} + 1
        # L_out del primer és 9-2+1-1+1=8, segon és 8-3+1=6 i el tercer és 5+4padding.

        self.cnv_fin = nn.Conv1d(self.bits_entrada - 1, 1, 1)
        self.lin_fin = nn.Linear((self.bits_entrada - 1) * self.bits_sortida,
                                 self.bits_sortida)
        self.relu = nn.ReLU()

    def forward(self, x):
        x_array = []
        for k in range(self.bits_entrada - 1):
            # print("*** iter: " + str(k))
            # print("*** cnv1d: " + str(self.array_cnv1d[k]))
            # print("*** lin: " + str(self.array_lin[k]))
            # print("*** x: " + str(x.shape))
            x_cnv = self.array_cnv1d[k](x)
            # print("*** x: " + str(x.shape))
            x_lin = self.array_lin[k](x_cnv)
            # print("*** x: " + str(x.shape))
            x_array.append(x_lin)

        # Ara les ha d'agrupar com canals i passar de tots els diferents canals a 1 de sol
        x = torch.cat(x_array, 2)
        #print("**** concat shape: " + str(x.shape))
        x = self.lin_fin(x)
        x = torch.sigmoid(10 * x)
        return (x)


class AutoEncoder(nn.Module):
    def __init__(self, bits_entrada, bits_mig):
        super(AutoEncoder, self).__init__()

        self.enc = Encoder(bits_entrada, bits_mig)
        self.dec = Decoder(bits_mig, bits_entrada)

        #bits_entrada = 3
        #bits_sortida = 9
        Rm = math.log2(2)
        Rc = bits_entrada * 1.0 / bits_mig
        EbN0 = 10**(2 / 10)
        self.noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

    def forward(self, x):
        mig = self.enc(x)
        mig = 2 * torch.sigmoid(100 * mig) - 1
        # Aquest factor de 100 l'hauria de fer més flexible, hauria de calcular el mínim valor de x i fer-ho adaptable, ja que si tot ja són propers a 1 no cal aquest factor.
        mig_noise = mig + self.noiseSigma * torch.randn(mig.shape)
        x = self.dec(mig_noise.float())
        return x, mig, mig_noise


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')

    bits_entrada = 3
    bits_mig = 9

    torch.manual_seed(0)
    # mod_enc = Encoder(bits_entrada, 6)
    # mod_enc.to(device)
    # mod_dec = Decoder(6, num_etiquetes)
    # mod_dec.to(device)
    autoenc = AutoEncoder(bits_entrada, bits_mig)
    num_etiquetes = 2**bits_entrada
    #optim_enc = torch.optim.SGD(mod_enc.parameters(), lr=1e-3, weight_decay=1e-5)
    #optim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-5)
    optim_auto = torch.optim.SGD(autoenc.parameters(),
                                 lr=1e-3,
                                 weight_decay=1e-5)

    criterion = BCELoss()
    loss_detall = BCELoss(reduction='none')

    ordre = list(range(num_etiquetes))
    # Per cada etiqueta he de fer el seu vector d'entrada
    entrada = []
    for element in ordre:
        st = ''
        # He d'afegir tants 0 al davant com longitud
        for k in format(element, "b").zfill(bits_entrada):
            st = st + k + ','
        entrada.append(eval('[' + st + ']'))
    entrada_torch = torch.Tensor(entrada) * 2 - 1
    entrada_torch = entrada_torch.view([num_etiquetes, 1, bits_entrada])

    f = open("debugant.log", "w")
    for epoch in range(17500):
        autoenc.train()

        if (epoch % 50) == 0:
            f.write("\n**************\nEpoch: " + str(epoch) +
                    "\n**************\n")

        # optim_enc.zero_grad()
        # optim_dec.zero_grad()
        optim_auto.zero_grad()

        seq_true = (entrada_torch.view(entrada_torch.shape[0],
                                       entrada_torch.shape[2]) + 1) / 2
        seq_pred, mig, mig_noise = autoenc(entrada_torch)

        loss = criterion(seq_pred.view(seq_true.shape[0], seq_true.shape[1]),
                         seq_true)
        # L'entrada al BCE és (N,*) on N és el tamany del batch

        loss.backward()
        # optim_enc.step()
        # optim_dec.step()
        optim_auto.step()

        if (epoch % 50) == 0:
            f.write("\n***\nSeq. entrada: " + str(seq_true.detach().numpy()))
            f.write("\nMig: " + str(mig.detach().numpy()))
            # f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            f.write("\nSeq. predita: " + str(seq_pred.detach().numpy()))
            perdua = loss_detall(
                seq_pred.view(seq_true.shape[0], seq_true.shape[1]), seq_true)
            f.write("\nPèrdua: " + str(perdua))
            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:' +
                        str(round(torch.cuda.memory_allocated(0) /
                                  1024**3, 1)) + 'GB')
                f.write('\nCached:   ' +
                        str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                        'GB')

            print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
    f.close()

    torch.save(
        {
            'epoch': epoch,
            'model_state_dict': autoenc.state_dict(),
            'optimizer_state_dict': optim_auto.state_dict(),
            'loss': loss,
            'bits_entrada': bits_entrada,
            'bits_mig': bits_mig,
        }, "AE_conv_Lin.pth")

    # Calculem l'error
    errors = 0
    iteracions = int(1e4)
    for k in range(iteracions):
        seq_pred, mig, mig_noise = autoenc(entrada_torch)
        res = (seq_pred > 0.5) * 2 - 1
        errors = errors + torch.sum(entrada_torch != res)
        if k % 1000 == 0:
            print(str(errors) + " : " + str(k))
    print("Error total " + str(errors.detach().numpy() /
                               (num_etiquetes * bits_entrada * iteracions)))
