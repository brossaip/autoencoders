# Vull provar que si repeteixo molt els codis inicials, després puc basar-me en una reducció de dimensionalitat com fan els autoencoders per reconstruir el gran repeat code i poder corregir els errors
# El VAE no ha donat els resultats que esparava. Tornaré al model senzill per poder fer la prova

import torch
import torch.nn as nn
from torch.nn import BCELoss, BCEWithLogitsLoss, MSELoss
import math
from random import shuffle
import AE_supercode_125500ep_10dB__11 as model
import pdb

############
# COMPONENTS
############
def distancia(codi):
    maxi = 0
    pos_max = []
    mini = 1e9
    pos_min = []
    for k in range(len(codi)):
        for j in range(len(codi) - k - 1):
            dist = torch.sum(torch.abs(codi[k] - codi[j + k + 1]))
            dist = dist.detach().numpy()
            if dist > maxi:
                maxi = dist
                pos_max = [k, j + k + 1]
            if dist < mini:
                mini = dist
                pos_min = [k, j + k + 1]

    return ([maxi, pos_max, mini, pos_min])

# Primer carregaré l'antic i després faré el nou a partir de l'antic (exm. AE_2conc_GCNN)
checkpoint = torch.load("AE_supercode_125500ep_10dB__11.pth")
ae_1stage = model.AutoEncoder()
ae_1stage.load_state_dict(checkpoint['model_state_dict'])

# congelem els paràmetres https://jimmy-shen.medium.com/pytorch-freeze-part-of-the-layers-4554105e03a6
for param in ae_1stage.parameters():
    param.requires_grad = False

class Encoder_2stage(nn.Module):
    def __init__(self):
        super(Encoder_2stage, self).__init__()
        self.cnn1a = nn.Conv1d(
            in_channels=1,
            out_channels=10,
            kernel_size=8, 
            stride=1,
            dilation=1)
        self.cnn1b = nn.Conv1d(in_channels=1,
                               out_channels=10,
                               kernel_size=8,
                               stride=1,
                               dilation=1)
        self.cnn2a = nn.Conv1d(in_channels=10,
                               out_channels=20,
                               kernel_size=12,
                               stride=1,
                               dilation=1)
        self.cnn2b = nn.Conv1d(in_channels=10,
                               out_channels=20,
                               kernel_size=12,
                               stride=1,
                               dilation=1)
        self.cnn3a = nn.Conv1d(in_channels=20,
                               out_channels=1,
                               kernel_size=6,
                               stride=1,
                               padding=1,
                               dilation=1)
        self.cnn3b = nn.Conv1d(in_channels=20,
                               out_channels=1,
                               kernel_size=6,
                               stride=1,
                               padding=1,
                               dilation=1)
        self.linear = nn.Linear(9,30)
        self.relu = nn.ReLU()
    def forward(self, x):
        x_lin = self.linear(x)
        x_lin = self.relu(x_lin)
        x1 = self.cnn3a(self.cnn2a(self.cnn1a(x_lin)))
        x2 = self.cnn3b(self.cnn2b(self.cnn1b(x_lin)))  # Longitud és 9
        x2 = torch.sigmoid(x2)
        # print("**** Tamany x1: " + str(x1.shape) + " - " + str(x2.shape) + " - " + str(x.shape))
        x = torch.mul(x1, x2) + x
        x = torch.tanh(x)
        return (x)

class Decoder_2stage(nn.Module):
    def __init__(self):
        super(Decoder_2stage, self).__init__()
        self.cnn1a = nn.ConvTranspose1d(
            in_channels=1,
            out_channels=10,
            kernel_size=8, 
            stride=1,
            dilation=1)
        self.cnn1b = nn.ConvTranspose1d(in_channels=1,
                               out_channels=10,
                               kernel_size=8,
                               stride=1,
                               dilation=1)
        self.cnn2a = nn.ConvTranspose1d(in_channels=10,
                               out_channels=20,
                               kernel_size=12,
                               stride=1,
                               dilation=1)
        self.cnn2b = nn.ConvTranspose1d(in_channels=10,
                               out_channels=20,
                               kernel_size=12,
                               stride=1,
                               dilation=1)
        self.cnn3a = nn.ConvTranspose1d(in_channels=20,
                               out_channels=1,
                               kernel_size=6,
                               stride=1,
                               padding=1,
                               dilation=1)
        self.cnn3b = nn.ConvTranspose1d(in_channels=20,
                               out_channels=1,
                               kernel_size=6,
                               stride=1,
                               padding=1,
                               dilation=1)
        self.linear = nn.Linear(30,9)
        self.relu = nn.ReLU()
    def forward(self, x):
        xa = torch.cat([x]*math.ceil(30/9),dim=2)
        xa = torch.split(xa,30,dim=2)
        xa = xa[0]
        x1 = self.cnn3a(self.cnn2a(self.cnn1a(x)))
        x2 = self.cnn3b(self.cnn2b(self.cnn1b(x)))  # Longitud és 9, però la sortida és 30
        x2 = torch.sigmoid(x2)
        # print("**** Tamany x1: " + str(x1.shape) + " - " + str(x2.shape))
        x = torch.mul(x1, x2) + xa
        x = self.linear(x)
        x = self.relu(x)
        x = torch.tanh(x)
        return (x)
class AutoEncoder_2stage(nn.Module):
    def __init__(self):
        super(AutoEncoder_2stage, self).__init__()

        self.enc2stg = Encoder_2stage()
        self.dec2stg = Decoder_2stage()

        bits_entrada = 3
        bits_sortida = 9
        Rm = math.log2(2)
        Rc = bits_entrada * 1.0 / bits_sortida
        EbN0 = 10**(10 / 10)
        self.noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

    def forward(self, x):
        mig_1st = ae_1stage.enc(x)  # Està entre -1|1
        mig_2st = self.enc2stg(mig_1st)
        # print("**** tamany mig: " + str(mig.shape))
        mig_noise = mig_2st + self.noiseSigma * torch.randn(mig_2st.shape).to(device)

        x2stg = self.dec2stg(mig_noise.float())
        x = ae_1stage.dec(x2stg)
        return x, mig_1st, mig_noise, mig_2st, x2stg


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')

    bits_entrada = 3
    bits_mig = 9

    torch.manual_seed(0)
    autoenc = AutoEncoder_2stage()

    num_etiquetes = 2**bits_entrada
    #optim_enc = torch.optim.SGD(mod_enc.parameters(), lr=1e-3, weight_decay=1e-5)
    #o1ptim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-5)
    optim_auto = torch.optim.SGD(autoenc.parameters(),
                                 lr=1e-3,
                                 weight_decay=1e-5)

    # criterion = BCEWithLogitsLoss() # Si jo no faig el sigmoid i deixo que el faci el pytorch no es queda clavat a 0.5 els que retornaven 0. Ara bé, no entenc com estan entre 0 i 1 l'error reportat segueix sent 0.3. Encara que siguin iguals els vectors sempre dóna un error.
    # loss_detall = BCELoss(reduction='none')
    criterion = BCEWithLogitsLoss()


    ordre = list(range(num_etiquetes))

    f = open("debugant.log", "w")
    for epoch in range(1255000):
        autoenc.train()

        # Per cada etiqueta he de fer el seu vector d'entrada
        shuffle(ordre)
        entrada = []
        for element in ordre:
            st = ''
            # He d'afegir tants 0 al davant com longitud
            for k in format(element, "b").zfill(bits_entrada):
                st = st + k + ','
            entrada.append(eval('[' + st + ']'))
        seq_true = torch.Tensor(entrada).to(device)  # 0|1
        entrada_torch = seq_true  # 0|1
        entrada_torch = 2*entrada_torch.view(
           [num_etiquetes, 1, bits_entrada])-1
        # Creem la matriu del supervector
        entrada_torch_SV = torch.cat([entrada_torch] * 100,
                                     dim=2).to(device)  # -1|1

        if (epoch % 500) == 0:
            f.write("\n**************\nEpoch: " + str(epoch) +
                    "\n**************\n")

        # optim_enc.zero_grad()
        # optim_dec.zero_grad()
        optim_auto.zero_grad()

        seq_pred, mig_1st, mig_noise, mig, x2stg = autoenc(entrada_torch_SV)

        # Ara he de passar de la seq_pred que conté un supervector a la descodificació d'un repeat code de només 3 bits
        # yapf: disable
        # print("*** " + str(seq_true.shape) + " : " + str(seq_pred.shape))
        # seq_pred_decode = torch.zeros_like(seq_true).view(seq_true.shape[0],1,seq_true.shape[1]).to(device)
        # # seq_pred = seq_pred*2-1 Ja estarà entre -1 i 1
        # for k in range(seq_pred.shape[0]):
        #     for l in range(seq_pred.shape[1]):
        #         for m in range(int(seq_pred.shape[2] / seq_true.shape[1])):
        #             seq_pred_decode[k][l][0] = seq_pred_decode[k][l][0] + seq_pred[k][l][3 * m]
        #             seq_pred_decode[k][l][1] = seq_pred_decode[k][l][1] + seq_pred[k][l][3 * m + 1]
        #             seq_pred_decode[k][l][2] = seq_pred_decode[k][l][2] + seq_pred[k][l][3 * m + 2]
        # seq_pred_decode_sigmoid = (torch.tanh(seq_pred_decode) + 1) / 2  # El poso també entre 0 i 1
        # yapf: enable
        # loss = criterion(
        #     seq_pred_decode_sigmoid.view(seq_true.shape[0], seq_true.shape[1]).to(device),
        #     seq_true)
        # L'entrada al BCELoss és (N,*) on N és el tamany del batch

        # pdb.set_trace()
        loss = criterion(torch.sigmoid(
            seq_pred.view(entrada_torch_SV.shape[0],
                          entrada_torch_SV.shape[2]).to(device)),
            torch.sigmoid(entrada_torch_SV.view(entrada_torch_SV.shape[0],
                                       entrada_torch_SV.shape[2])))

        loss.backward()
        # optim_enc.step()
        # optim_dec.step()
        optim_auto.step()

        if (epoch % 500) == 0:
            f.write("\nMig: " + str(mig.cpu().detach().numpy()))
            f.write("\n***\nSeq. entrada: " +
                    str(seq_true.cpu().detach().numpy()))
            # f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            dist = distancia(mig.cpu())
            f.write("\nDist codi: " + str(dist))
            f.write("\nSeq. predita: " + str(seq_pred.cpu().detach().numpy()))
            #f.write("\nSeq. predita decode: " + str(seq_pred_decode_sigmoid.cpu().detach().numpy()))
            # perdua = loss_detall(
            #     seq_pred_decode_sigmoid.view(seq_true.shape[0], seq_true.shape[1]), seq_true)
            # f.write("\nPèrdua: " + str(perdua))
            f.write("\nPèrdua: " + str(loss))
            f.write("\nPesos enc: " + str(autoenc.enc2stg.cnn1a.weight))
            f.write("\nPesos cnn1b: " + str(autoenc.enc2stg.cnn1b.weight))
            f.write("\nPesos dec: " + str(autoenc.dec2stg.cnn1a.weight))
            f.write("\nPesos dec cnn1b: " + str(autoenc.dec2stg.cnn1b.weight))
            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:' +
                        str(round(torch.cuda.memory_allocated(0) /
                                  1024**3, 1)) + 'GB')
                f.write('\nCached:   ' +
                        str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                        'GB')

            print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
        if (epoch % 5000) == 0:
            torch.save(
                {
                    'epoch': epoch,
                    'model_state_dict': autoenc.state_dict(),
                    'optimizer_state_dict': optim_auto.state_dict(),
                    'loss': loss,
                    'bits_entrada': bits_entrada,
                    'bits_mig': bits_mig,
                }, "AE_supercode.pth")

    f.close()

    torch.save(
        {
            'epoch': epoch,
            'model_state_dict': autoenc.state_dict(),
            'optimizer_state_dict': optim_auto.state_dict(),
            'loss': loss,
            'bits_entrada': bits_entrada,
            'bits_mig': bits_mig,
        }, "AE_supercode.pth")

    # # Calculem l'error
    # errors = 0
    # iteracions = int(1e4)
    # for k in range(iteracions):
    #     seq_pred, mig, mig_noise = autoenc(entrada_torch)
    #     res = (seq_pred > 0.5) * 2 - 1
    #     errors = errors + torch.sum(entrada_torch != res)
    #     if k % 1000 == 0:
    #         print(str(errors) + " : " + str(k))
    # print("Error total " + str(errors.cpu().detach().numpy() /
    #                            (num_etiquetes * bits_entrada * iteracions)))
