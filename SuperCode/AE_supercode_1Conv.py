# Vull provar que si repeteixo molt els codis inicials, després puc basar-me en una reducció de dimensionalitat com fan els autoencoders per reconstruir el gran repeat code i poder corregir els errors
# El VAE no ha donat els resultats que esparava. Tornaré al model senzill per poder fer la prova
# Vull provar de no usar la pseudo RNN i provar amb convolucionals com AlexNet per al supercodi. Aquí hi ha una implementació https://github.com/tyommik/AlexNet/blob/master/alexnet.py
# Provaré amb 1 convolucional i el transpose perquè hauria de ser suficient per regenerar el codi

import torch
import torch.nn as nn
from torch.nn import BCELoss, BCEWithLogitsLoss, MSELoss
import math
from random import shuffle
import pdb
import numpy as np

############
# COMPONENTS
############
def distancia(codi):
    maxi = 0
    pos_max = []
    mini = 1e9
    pos_min = []
    for k in range(len(codi)):
        for j in range(len(codi) - k - 1):
            dist = torch.sum(torch.abs(codi[k] - codi[j + k + 1]))
            dist = dist.detach().numpy()
            if dist > maxi:
                maxi = dist
                pos_max = [k, j + k + 1]
            if dist < mini:
                mini = dist
                pos_min = [k, j + k + 1]

    return ([maxi, pos_max, mini, pos_min])

def calculem_error(entrada, seq_pred):
    # En el cas de supervector de 18, entrada són els 3 bits del 3|9, i seq_pred ha de trobar els 3 bits i és la seqüència de 18 bits
    [_,_,nombreBits] = entrada.shape
    [dim0,dim1,dim2] = seq_pred.shape
    posb0 = torch.from_numpy(np.arange(0,dim2,nombreBits))
    posb1 = torch.from_numpy(np.arange(1,dim2,nombreBits))
    posb2 = torch.from_numpy(np.arange(2,dim2,nombreBits))
    b0 = torch.sum(torch.index_select(seq_pred,2,posb0),dim=2)
    b1 = torch.sum(torch.index_select(seq_pred,2,posb1),dim=2)
    b2 = torch.sum(torch.index_select(seq_pred,2,posb2),dim=2)
    b0_est = ((b0>0)*2-1).squeeze()
    b1_est = ((b1>0)*2-1).squeeze()
    b2_est = ((b2>0)*2-1).squeeze()
    b0_ent = torch.index_select(entrada,2,torch.tensor([0])).squeeze()
    b1_ent = torch.index_select(entrada,2,torch.tensor([1])).squeeze()
    b2_ent = torch.index_select(entrada,2,torch.tensor([2])).squeeze()
    error = sum(b0_ent != b0_est)
    error = error + sum(b1_ent != b1_est)
    error = error + sum(b2_ent != b2_est)
    return((error,dim0*nombreBits))

class Encoder(nn.Module):
    def __init__(self):
        super(Encoder, self).__init__()
        self.cnn1a = nn.Conv1d(in_channels=1,
                               out_channels=1,
                               kernel_size=11,
                               stride=2,
                               dilation=1,
                               padding=3)

    def forward(self, x):
        x1 = self.cnn1a(x)
        x = torch.tanh(x1)
        return (x)


class Decoder(nn.Module):
    def __init__(self):
        super(Decoder, self).__init__()

        # Els bits entrada i sortida són de més gran a més petit, al revés que l'encoder
        self.cnn1a = nn.ConvTranspose1d(
            in_channels=1,
            out_channels=1,
            kernel_size=11,  # Kernel_size can't be greater than input size
            stride=2,
            dilation=1,
            output_padding=0,
            padding=3)

    def forward(self, x):
        x1 = self.cnn1a(x)
        x = torch.tanh(x1)
        return (x)


class AutoEncoder(nn.Module):
    def __init__(self):
        super(AutoEncoder, self).__init__()

        self.enc = Encoder()
        self.dec = Decoder()
        self.db = 8

        bits_entrada = 3
        bits_sortida = 9
        Rm = math.log2(2)
        Rc = bits_entrada * 1.0 / bits_sortida
        EbN0 = 10**(self.db / 10)
        self.noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

    def forward(self, x):

        mig = self.enc(x)  # Està entre -1|1
        # print("**** tamany mig: " + str(mig.shape))
        mig_noise = mig + self.noiseSigma * torch.randn(mig.shape).to(device)

        x = self.dec(mig_noise.float())
        return x, mig, mig_noise


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')

    bits_entrada = 3
    bits_mig = 9

    torch.manual_seed(0)
    autoenc = AutoEncoder().to(device)

    # Recuperem l'últim epoch
    # checkpoint = torch.load("AE_supercode75000_1conv_10db.pth")
    # autoenc.load_state_dict(checkpoint['model_state_dict'])


    num_etiquetes = 2**bits_entrada
    #optim_enc = torch.optim.SGD(mod_enc.parameters(), lr=1e-3, weight_decay=1e-5)
    #o1ptim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-5)
    optim_auto = torch.optim.SGD(autoenc.parameters(),
                                 lr=1e-3,
                                 weight_decay=1e-5)

    # criterion = BCEWithLogitsLoss() # Si jo no faig el sigmoid i deixo que el faci el pytorch no es queda clavat a 0.5 els que retornaven 0. Ara bé, no entenc com estan entre 0 i 1 l'error reportat segueix sent 0.3. Encara que siguin iguals els vectors sempre dóna un error.
    # loss_detall = BCELoss(reduction='none')
    criterion = BCEWithLogitsLoss()
    #criterion = MSELoss(reduction='sum')

    ordre = list(range(num_etiquetes))

    f = open("debugant.log", "w")
    for epoch in range(50500):
        autoenc.train()

        # Per cada etiqueta he de fer el seu vector d'entrada
        shuffle(ordre)
        entrada = []
        for element in ordre:
            st = ''
            # He d'afegir tants 0 al davant com longitud
            for k in format(element, "b").zfill(bits_entrada):
                st = st + k + ','
            entrada.append(eval('[' + st + ']'))
        seq_true = torch.Tensor(entrada).to(device)  # 0|1
        entrada_torch = seq_true  # 0|1
        entrada_torch = 2 * entrada_torch.view(
            [num_etiquetes, 1, bits_entrada]) - 1

        # Creem la matriu del supervector
        entrada_torch_SV = torch.cat([entrada_torch] * 7,
                                     dim=2).to(device)  # -1|1

        if epoch == 35000:
            print("Canviem el criteri d'error")
            criterion = MSELoss()
        if (epoch % 500) == 0:
            f.write("\n**************\nEpoch: " + str(epoch) +
                    "\n**************\n")

        # optim_enc.zero_grad()
        # optim_dec.zero_grad()
        optim_auto.zero_grad()

        seq_pred, mig, mig_noise = autoenc(entrada_torch_SV)

        # pdb.set_trace()
        loss = criterion(
            seq_pred.view(entrada_torch_SV.shape[0],
                          entrada_torch_SV.shape[2]).to(device),
            entrada_torch_SV.view(entrada_torch_SV.shape[0],
                                  entrada_torch_SV.shape[2]))

        loss.backward()
        # optim_enc.step()
        # optim_dec.step()
        optim_auto.step()

        if (epoch % 500) == 0:
            f.write("\nMig: " + str(mig.cpu().detach().numpy()))
            f.write("\n***\nSeq. entrada: " +
                    str(seq_true.cpu().detach().numpy()))
            # f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            dist = distancia(mig.cpu())
            f.write("\nDist codi: " + str(dist))
            f.write("\nSeq. predita: " + str(seq_pred.cpu().detach().numpy()))
            f.write("\nSeq. predita [0][30:50]: " +
                    str(seq_pred[0][30:50].cpu().detach().numpy()))

            #f.write("\nSeq. predita decode: " + str(seq_pred_decode_sigmoid.cpu().detach().numpy()))
            # perdua = loss_detall(
            #     seq_pred_decode_sigmoid.view(seq_true.shape[0], seq_true.shape[1]), seq_true)
            # f.write("\nPèrdua: " + str(perdua))
            f.write("\nPèrdua: " + str(loss))
            f.write("\nPesos enc: " + str(autoenc.enc.cnn1a.weight))
            f.write("\nPesos dec: " + str(autoenc.dec.cnn1a.weight))

            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:' +
                        str(round(torch.cuda.memory_allocated(0) /
                                  1024**3, 1)) + 'GB')
                f.write('\nCached:   ' +
                        str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                        'GB')

            print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
        if (epoch % 25000) == 0:
            torch.save(
                {
                    'epoch': epoch,
                    'model_state_dict': autoenc.state_dict(),
                    'optimizer_state_dict': optim_auto.state_dict(),
                    'loss': loss,
                    'bits_entrada': bits_entrada,
                    'bits_mig': bits_mig,
                }, "AE_supercode" + str(epoch) + ".pth")

    f.close()

    torch.save(
        {
            'epoch': epoch,
            'model_state_dict': autoenc.state_dict(),
            'optimizer_state_dict': optim_auto.state_dict(),
            'loss': loss,
            'bits_entrada': bits_entrada,
            'bits_mig': bits_mig,
        }, "AE_supercode_1cnv.pth")

    # Calculem l'error
    errors = 0
    numbits = 0
    iteracions = int(1e4)
    for j in range(iteracions):
        shuffle(ordre)
        entrada = []
        for element in ordre:
            st = ''
            # He d'afegir tants 0 al davant com longitud
            for k in format(element, "b").zfill(bits_entrada):
                st = st + k + ','
            entrada.append(eval('[' + st + ']'))
        seq_true = torch.Tensor(entrada).to(device)  # 0|1
        entrada_torch = seq_true  # 0|1
        entrada_torch = 2 * entrada_torch.view(
            [num_etiquetes, 1, bits_entrada]) - 1
        # Creem la matriu del supervector
        entrada_torch_SV = torch.cat([entrada_torch] * 6,
                                     dim=2).to(device)  # -1|1

        seq_pred, mig, mig_noise = autoenc(entrada_torch_SV)
        (errors_calc, numbits_calc) = calculem_error(entrada_torch, seq_pred)
        errors = errors + errors_calc
        numbits = numbits + numbits_calc
        if (j%500)==0:
            print("Errors iter: " + str(j) + " ; " + str(errors) + " ; " + str(numbits))
    print("*** Errors iter: " + str(j) + " ; " + str(errors) + " ; " + str(numbits))
