# Vull provar que si repeteixo molt els codis inicials, després puc basar-me en una reducció de dimensionalitat com fan els autoencoders per reconstruir el gran repeat code i poder corregir els errors
# El VAE no ha donat els resultats que esparava. Tornaré al model senzill per poder fer la prova
# Vull provar de no usar la pseudo RNN i provar amb convolucionals com AlexNet per al supercodi. Aquí hi ha una implementació https://github.com/tyommik/AlexNet/blob/master/alexnet.py
# Provaré amb 1 convolucional i el transpose perquè hauria de ser suficient per regenerar el codi
# Amb un sol convolucional no dóna gaire bons resultats. Però què passa si la segona etapa és una combinació lineal de dos autoencoders amb diferents pesos?
# Què és millor posar el lineal del descodificador just després del canal o després dels convTranspose1D? O posar-ne 2?

import torch
import torch.nn as nn
from torch.nn import BCELoss, BCEWithLogitsLoss, MSELoss
import math
from random import shuffle
import AE_SC_1cnv_15l as model15
import AE_SC_1cnv_18l as model18
import pdb
import numpy as np


def calculem_error(entrada, seq_pred):
    # En el cas de supervector de 18, entrada són els 3 bits del 3|9, i seq_pred ha de trobar els 3 bits i és la seqüència de 18 bits
    [_, _, nombreBits] = entrada.shape
    [dim0, dim1, dim2] = seq_pred.shape
    posb0 = torch.from_numpy(np.arange(0, dim2, nombreBits))
    posb1 = torch.from_numpy(np.arange(1, dim2, nombreBits))
    posb2 = torch.from_numpy(np.arange(2, dim2, nombreBits))
    b0 = torch.sum(torch.index_select(seq_pred, 2, posb0), dim=2)
    b1 = torch.sum(torch.index_select(seq_pred, 2, posb1), dim=2)
    b2 = torch.sum(torch.index_select(seq_pred, 2, posb2), dim=2)
    b0_est = ((b0 > 0) * 2 - 1).squeeze()
    b1_est = ((b1 > 0) * 2 - 1).squeeze()
    b2_est = ((b2 > 0) * 2 - 1).squeeze()
    b0_ent = torch.index_select(entrada, 2, torch.tensor([0])).squeeze()
    b1_ent = torch.index_select(entrada, 2, torch.tensor([1])).squeeze()
    b2_ent = torch.index_select(entrada, 2, torch.tensor([2])).squeeze()
    error = sum(b0_ent != b0_est)
    error = error + sum(b1_ent != b1_est)
    error = error + sum(b2_ent != b2_est)
    if error > 0:
        print("**** Hem detectat " + str(error) + " errors")
        print(str(entrada))
        print(str(seq_pred))
    return ((error, dim0 * nombreBits))


############
# Noves capes
############
class Encoder_2st(nn.Module):
    def __init__(self):
        super(Encoder_2st, self).__init__()
        self.lin = nn.Linear(18, 9)

    def forward(self, x):
        x1 = self.lin(x)
        x = torch.tanh(x1)
        return (x)


class Decoder_2st(nn.Module):
    def __init__(self):
        super(Decoder_2st, self).__init__()

        # Els bits entrada i sortida són de més gran a més petit, al revés que l'encoder
        self.lin = nn.Linear(33,
                             18)  # 18 d'un decoder i 15 altre decoder fan 33

    def forward(self, x):
        x1 = self.lin(x)
        x = torch.tanh(x1)
        return (x)


class AutoEncoder(nn.Module):
    def __init__(self):
        super(AutoEncoder, self).__init__()

        self.enc_2st = Encoder_2st()
        self.dec_2st = Decoder_2st()

        bits_entrada = 3
        bits_sortida = 9
        Rm = math.log2(2)
        Rc = bits_entrada * 1.0 / bits_sortida
        EbN0 = 10**(8 / 10)
        self.noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

        ###########################
        # Carreguem els components del 1Conv
        ###########################

        checkpoint = torch.load("AE_SC_1cnv_15l.pth")
        self.ae_15 = model15.AutoEncoder()
        self.ae_15.load_state_dict(checkpoint['model_state_dict'])

        checkpoint = torch.load("AE_SC_1cnv_18l.pth")
        self.ae_18 = model18.AutoEncoder()
        self.ae_18.load_state_dict(checkpoint['model_state_dict'])

        # congelem els paràmetres https://jimmy-shen.medium.com/pytorch-freeze-part-of-the-layers-4554105e03a6
        for param in self.ae_15.parameters():
            param.requires_grad = False
        for param in self.ae_18.parameters():
            param.requires_grad = False

    def forward(self, x):
        #print("**** Tamany entrada: " + str(x.shape))
        # Genero SC de 18 i extrec un subconjunt de 15 per al següent bloc
        st1_a = self.ae_18.enc(x)
        st1_b = self.ae_15.enc(torch.narrow(x, 2, 0, 15))
        # Ara concatenem la sortida per tornar a codificar.
        st1 = torch.cat((st1_a, st1_b), 2)
        #print("**** Tamany st1: " + str(st1.shape))
        mig = self.enc_2st(st1)  # Està entre -1|1
        #print("**** tamany mig: " + str(mig.shape))
        mig_noise = mig + self.noiseSigma * torch.randn(mig.shape).to(device)

        dec_a = self.ae_15.dec(mig_noise.float())
        dec_b = self.ae_18.dec(mig_noise.float())
        st1 = torch.cat((dec_a, dec_b), 2)
        #print("*** Tamany st2: " + str(x.shape))
        st2 = self.dec_2st(st1)
        # print("*** Tamany sortida: " + str(x.shape))
        return st2, mig, mig_noise


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')

    bits_entrada = 3
    bits_mig = 9

    torch.manual_seed(0)
    autoenc = AutoEncoder().to(device)
    # Carreguem històric
    checkpoint = torch.load("AE_supercode_1cnv_2st.pth")
    autoenc.load_state_dict(checkpoint['model_state_dict'])

    num_etiquetes = 2**bits_entrada
    #optim_enc = torch.optim.SGD(mod_enc.parameters(), lr=1e-3, weight_decay=1e-5)
    #o1ptim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-5)
    optim_auto = torch.optim.SGD(autoenc.parameters(),
                                 lr=1e-3,
                                 weight_decay=1e-5)

    criterion = BCEWithLogitsLoss(
        reduction='sum'
    )  # Si jo no faig el sigmoid i deixo que el faci el pytorch no es queda clavat a 0.5 els que retornaven 0. Ara bé, no entenc com estan entre 0 i 1 l'error reportat segueix sent 0.3. Encara que siguin iguals els vectors sempre dóna un error.
    # loss_detall = BCELoss(reduction='none')
    # criterion = MSELoss()

    ordre = list(range(num_etiquetes))

    f = open("debugant.log", "w")
    for epoch in range(255000):
        autoenc.train()

        # Per cada etiqueta he de fer el seu vector d'entrada
        shuffle(ordre)
        entrada = []
        for element in ordre:
            st = ''
            # He d'afegir tants 0 al davant com longitud
            for k in format(element, "b").zfill(bits_entrada):
                st = st + k + ','
            entrada.append(eval('[' + st + ']'))
        seq_true = torch.Tensor(entrada).to(device)  # 0|1
        entrada_torch = seq_true  # 0|1
        entrada_torch = 2 * entrada_torch.view(
            [num_etiquetes, 1, bits_entrada]) - 1

        # Creem la matriu del supervector
        entrada_torch_SV = torch.cat([entrada_torch] * 6,
                                     dim=2).to(device)  # -1|1

        if (epoch % 500) == 0:
            f.write("\n**************\nEpoch: " + str(epoch) +
                    "\n**************\n")

        if epoch == 80000:
            criterion = MSELoss(reduction='sum')
        # optim_enc.zero_grad()
        # optim_dec.zero_grad()
        optim_auto.zero_grad()

        seq_pred, mig, mig_noise = autoenc(entrada_torch_SV)
        # print("*** Tamany sortida: " + str(seq_pred.shape))
        # Ara he de passar de la seq_pred que conté un supervector a la descodificació d'un repeat code de només 3 bits
        # yapf: disable
        # print("*** " + str(seq_true.shape) + " : " + str(seq_pred.shape))
        # seq_pred_decode = torch.zeros_like(seq_true).view(seq_true.shape[0],1,seq_true.shape[1]).to(device)
        # # seq_pred = seq_pred*2-1 Ja estarà entre -1 i 1
        # for k in range(seq_pred.shape[0]):
        #     for l in range(seq_pred.shape[1]):
        #         for m in range(int(seq_pred.shape[2] / seq_true.shape[1])):
        #             seq_pred_decode[k][l][0] = seq_pred_decode[k][l][0] + seq_pred[k][l][3 * m]
        #             seq_pred_decode[k][l][1] = seq_pred_decode[k][l][1] + seq_pred[k][l][3 * m + 1]
        #             seq_pred_decode[k][l][2] = seq_pred_decode[k][l][2] + seq_pred[k][l][3 * m + 2]
        # seq_pred_decode_sigmoid = (torch.tanh(seq_pred_decode) + 1) / 2  # El poso també entre 0 i 1
        # yapf: enable
        # loss = criterion(
        #     seq_pred_decode_sigmoid.view(seq_true.shape[0], seq_true.shape[1]).to(device),
        #     seq_true)
        # L'entrada al BCELoss és (N,*) on N és el tamany del batch

        # pdb.set_trace()
        loss = criterion(
            seq_pred.view(entrada_torch_SV.shape[0],
                          entrada_torch_SV.shape[2]).to(device),
            entrada_torch_SV.view(entrada_torch_SV.shape[0],
                                  entrada_torch_SV.shape[2]))

        loss.backward()
        # optim_enc.step()
        # optim_dec.step()
        optim_auto.step()

        if (epoch % 500) == 0:
            f.write("\nMig: " + str(mig.cpu().detach().numpy()))
            f.write("\n***\nSeq. entrada: " +
                    str(seq_true.cpu().detach().numpy()))
            # f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            f.write("\nSeq. predita: " + str(seq_pred.cpu().detach().numpy()))
            f.write("\nSeq. predita [0][30:50]: " +
                    str(seq_pred[0][30:50].cpu().detach().numpy()))

            #f.write("\nSeq. predita decode: " + str(seq_pred_decode_sigmoid.cpu().detach().numpy()))
            # perdua = loss_detall(
            #     seq_pred_decode_sigmoid.view(seq_true.shape[0], seq_true.shape[1]), seq_true)
            # f.write("\nPèrdua: " + str(perdua))
            f.write("\nPèrdua: " + str(loss))
            f.write("\nPesos enc: " + str(autoenc.enc_2st.lin.weight))
            f.write("\nPesos dec: " + str(autoenc.dec_2st.lin.weight))

            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:' +
                        str(round(torch.cuda.memory_allocated(0) /
                                  1024**3, 1)) + 'GB')
                f.write('\nCached:   ' +
                        str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                        'GB')

            print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
        if (epoch % 5000) == 0:
            torch.save(
                {
                    'epoch': epoch,
                    'model_state_dict': autoenc.state_dict(),
                    'optimizer_state_dict': optim_auto.state_dict(),
                    'loss': loss,
                    'bits_entrada': bits_entrada,
                    'bits_mig': bits_mig,
                }, "AE_supercode" + str(epoch) + ".pth")

    f.close()

    torch.save(
        {
            'epoch': epoch,
            'model_state_dict': autoenc.state_dict(),
            'optimizer_state_dict': optim_auto.state_dict(),
            'loss': loss,
            'bits_entrada': bits_entrada,
            'bits_mig': bits_mig,
        }, "AE_supercode_1cnv_2st.pth")

    # Calculem l'error
    errors = 0
    numbits = 0
    iteracions = int(1e4)
    for j in range(iteracions):
        shuffle(ordre)
        entrada = []
        for element in ordre:
            st = ''
            # He d'afegir tants 0 al davant com longitud
            for k in format(element, "b").zfill(bits_entrada):
                st = st + k + ','
            entrada.append(eval('[' + st + ']'))
        seq_true = torch.Tensor(entrada).to(device)  # 0|1
        entrada_torch = seq_true  # 0|1
        entrada_torch = 2 * entrada_torch.view(
            [num_etiquetes, 1, bits_entrada]) - 1
        # Creem la matriu del supervector
        entrada_torch_SV = torch.cat([entrada_torch] * 6,
                                     dim=2).to(device)  # -1|1

        # Hem de posar a on està l'error per debugar a veure si computa bé
        seq_pred, mig, mig_noise = autoenc(entrada_torch_SV)
        (errors_calc, numbits_calc) = calculem_error(entrada_torch, seq_pred)
        errors = errors + errors_calc
        numbits = numbits + numbits_calc
        if (j % 500) == 0:
            print("Errors iter: " + str(j) + " ; " + str(errors) + " ; " +
                  str(numbits))
    print("*** Errors iter: " + str(j) + " ; " + str(errors) + " ; " +
          str(numbits))
