# Començaré per un cas que funciona d'imatges, un autoencoder senzill, com el del tutorial https://github.com/justincharney/Autoencoder_And_Denoiser_Pytorch/blob/main/Autoencoder.ipynb
# i després continuaré fent proves per decréixer el tamany fins a imatges de 3x3 ó 9x9 partint de les mateixes 28x28 que és el tamany del MNIST
# Ara passa a 7x7 que ja és bon rati. He de generar les imatges binàries a veure què tal funciona repetint els codis

import torch as torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torchvision import datasets
import torchvision.transforms as transforms
from PIL import Image
from torch.utils.data import Dataset
from glob import glob

class Patrons3Dataset(Dataset):
    def __init__(self,root_dir):
        self.files = glob(root_dir)
    def __len__(self):
        return len(self.files)
    def __getitem__(self, idx):
        # print("*** Index: " + str(idx))
        # print("*** files: " + str(self.files))
        img = np.asarray(Image.open(self.files[idx]).convert('L'))/255
        img_tensor = torch.Tensor(img)
        img_tensor = img_tensor.view([1,img_tensor.shape[0],img_tensor.shape[1]])
        punt3b = self.files[idx].find('3b_')
        puntpng = self.files[idx].find('.png')
        label = self.files[idx][punt3b+3:puntpng]
        return(img_tensor,label)

class AutoEncoder(nn.Module):
  def __init__(self):
    super(AutoEncoder,self).__init__()
    #Encoder
    self.conv1 = nn.Conv2d(1,16, kernel_size=3, padding=1)
    self.conv2 = nn.Conv2d(16,4, kernel_size=3,padding=1)
    self.conv3 = nn.Conv2d(4,4, kernel_size=3,padding=1)
    self.pool = nn.MaxPool2d(2,2)

    #Decoder
    self.t_conv0 = nn.ConvTranspose2d(4,4,kernel_size=2,stride=2)
    self.t_conv1 = nn.ConvTranspose2d(4,16,kernel_size=2,stride=2)
    self.t_conv2 = nn.ConvTranspose2d(16,1,kernel_size=2,stride=2)

    # Noise
    bits_entrada = 3
    bits_sortida = 9
    Rm = np.log2(2)
    Rc = bits_entrada * 1.0 / bits_sortida
    EbN0 = 10**(2 / 10)
    self.noiseSigma = np.sqrt(1 / (2 * Rm * Rc * EbN0))

  def forward(self, x):
    # print(" *** tamany inicial: " + str(x.shape))
    x = F.relu(self.conv1(x))
    x = self.pool(x)
    x = F.relu(self.conv2(x))
    x = self.pool(x)
    x = F.relu(self.conv3(x))
    x = self.pool(x)
    mig = 2*torch.sigmoid(x)-1
    # mig = 2*(x)-1
    x = mig + self.noiseSigma * torch.randn(x.shape)
    x = (x+1)/2
    # print(" *** tamany mig: " + str(x.shape))
    x = F.relu(self.t_conv0(x))
    x = F.relu(self.t_conv1(x))
    x = torch.sigmoid(self.t_conv2(x))
    return x,mig


torch.manual_seed(0)
model_2 = AutoEncoder()


#defining some parameters
num_epochs = 500
batch_size = 8

criterion2 = nn.MSELoss()
optimizer2 = torch.optim.Adam(model_2.parameters(), lr = 0.001)

# getting and preparing data

transform = transforms.ToTensor()

train_data = Patrons3Dataset('/home/jep/autoencoders/Patrons_encoding_3/imat*')

num_workers = 0
train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, num_workers=num_workers)

print(" **** Pesos: " + str(model_2.t_conv2.weight))
print(" **** Pesos2: " + str(model_2.conv1.weight))
for epoch in range(num_epochs):
    train_loss = 0
    for data in train_loader:
        images, _ = data

        optimizer2.zero_grad()
        outputs, mig = model_2(images)
        loss2 = criterion2(outputs, images)
        loss2.backward()
        optimizer2.step()
        train_loss += loss2.item()*images.size(0)
        if (epoch % 100) == 0:
            # print("*** mig: " + str(mig.shape))
            a=3
    print("Epoch {} \t Training Loss {:.6f}".format (epoch, train_loss))
print("*** Mig: " + str(mig))
print(" **** Pesos: " + str(model_2.t_conv2.weight))
print(" **** Pesos2: " + str(model_2.conv1.weight))
