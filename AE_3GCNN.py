# Vull provar el Gate Convolutional Neural network en aquesta codificació a veure què tal funciona. L'original https://github.com/DavidWBressler/GCNN/blob/master/GCNN.ipynb utilitza conv2d i diferents canals. Jo provaré amb conv1d i encara no implementaré res de canals.
# He partit del Autoencoder_2conc.py
# A partir d'avui 29/10/20 l'he usat per poder debugar les GCNN per poder donar codis que no estiguin repetits. Per debugar usaré cas 3º9
# Vaig a provar què passa quan en concateno dues GCNN

# Third Party
import torch
import torch.nn as nn
from torch.nn import BCELoss
import math

############
# COMPONENTS
############


class Encoder(nn.Module):
    # He fet pel cas 9/27
    def __init__(self):
        super(Encoder, self).__init__()

        self.lin1_a = nn.Linear(3, 3)
        self.lin1_b = nn.Linear(3, 3)
        self.lin1_c = nn.Linear(3, 3)

        self.lin2 = nn.Linear(9,9)
        self.cnn1a = nn.Conv1d(
            in_channels=1,
            out_channels=3,
            kernel_size=2,  # Kernel_size can't be greater than input size
            stride=1)
        self.cnn1b = nn.Conv1d(in_channels=1,
                               out_channels=3,
                               kernel_size=2,
                               stride=1)
        self.cnn2a = nn.Conv1d(in_channels=3,
                               out_channels=3,
                               kernel_size=3,
                               stride=1)
        self.cnn2b = nn.Conv1d(in_channels=3,
                               out_channels=3,
                               kernel_size=3,
                               stride=1)
        self.cnn3a = nn.Conv1d(in_channels=3,
                               out_channels=1,
                               kernel_size=2,
                               stride=1,
                               padding=2)
        self.cnn3b = nn.Conv1d(in_channels=3,
                               out_channels=1,
                               kernel_size=2,
                               stride=1,
                               padding=2)
        # L_out = \frac{L_in + 2*padding -dilation*(kernel_size-1)-1}{stride} + 1
        # L_out del primer és 9-2+1-1+1=8, segon és 8-3+1=6 i el tercer és 5+4padding.
        # Com que la meva xarxa no és deep, no afegeixo el residual que diuen
        self.lin2_2 = nn.Linear(9, 9)
        self.cnn1a_2 = nn.Conv1d(
            in_channels=1,
            out_channels=3,
            kernel_size=2,  # Kernel_size can't be greater than input size
            stride=1)
        self.cnn1b_2 = nn.Conv1d(in_channels=1,
                               out_channels=3,
                               kernel_size=2,
                               stride=1)
        self.cnn2a_2 = nn.Conv1d(in_channels=3,
                               out_channels=3,
                               kernel_size=3,
                               stride=1)
        self.cnn2b_2 = nn.Conv1d(in_channels=3,
                               out_channels=3,
                               kernel_size=3,
                               stride=1)
        self.cnn3a_2 = nn.Conv1d(in_channels=3,
                               out_channels=1,
                               kernel_size=2,
                               stride=1,
                               padding=2)
        self.cnn3b_2 = nn.Conv1d(in_channels=3,
                               out_channels=1,
                               kernel_size=2,
                               stride=1,
                               padding=2)

        self.lin2_3 = nn.Linear(9, 9)
        self.cnn1a_3 = nn.Conv1d(
            in_channels=1,
            out_channels=3,
            kernel_size=2,  # Kernel_size can't be greater than input size
            stride=1)
        self.cnn1b_3 = nn.Conv1d(in_channels=1,
                               out_channels=3,
                               kernel_size=2,
                               stride=1)
        self.cnn2a_3 = nn.Conv1d(in_channels=3,
                               out_channels=3,
                               kernel_size=3,
                               stride=1)
        self.cnn2b_3 = nn.Conv1d(in_channels=3,
                               out_channels=3,
                               kernel_size=3,
                               stride=1)
        self.cnn3a_3 = nn.Conv1d(in_channels=3,
                               out_channels=1,
                               kernel_size=2,
                               stride=1,
                               padding=2)
        self.cnn3b_3 = nn.Conv1d(in_channels=3,
                               out_channels=1,
                               kernel_size=2,
                               stride=1,
                               padding=2)

    def forward(self, x):
        x_a = self.lin1_a(x)
        x_b = self.lin1_b(x)
        x_c = self.lin1_c(x)
        x = torch.cat((x_a, x_b, x_c), 2)  # Longitud 9

        x1 = self.cnn3a(self.cnn2a(self.cnn1a(x)))
        x2 = self.cnn3b(self.cnn2b(self.cnn1b(x)))  # Longitud és 9
        x2 = torch.sigmoid(x2)
        x = torch.mul(x1, x2) + x
        x = self.lin2(x)

        x1 = self.cnn3a_2(self.cnn2a_2(self.cnn1a_2(x)))
        x2 = self.cnn3b_2(self.cnn2b_2(self.cnn1b_2(x)))  # Longitud és 9
        x2 = torch.sigmoid(x2)
        x = torch.mul(x1, x2) + x
        x = self.lin2_2(x)

        x1 = self.cnn3a_3(self.cnn2a_3(self.cnn1a_3(x)))
        x2 = self.cnn3b_3(self.cnn2b_3(self.cnn1b_3(x)))  # Longitud és 9
        x2 = torch.sigmoid(x2)
        x = torch.mul(x1, x2) + x
        x = self.lin2_3(x)

        return (x)


class Decoder(nn.Module):
    def __init__(self):
        super(Decoder, self).__init__()

        self.lin2 = nn.Linear(9, 9)
        self.cnn1a = nn.ConvTranspose1d(
            in_channels=1,
            out_channels=1,
            kernel_size=2,  # Kernel_size can't be greater than input size
            stride=1)
        self.cnn1b = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=2,
                                        stride=1)
        self.cnn2a = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=3,
                                        stride=1)
        self.cnn2b = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=3,
                                        stride=1)
        self.cnn3a = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=2,
                                        stride=1,
                                        padding=2)
        self.cnn3b = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=2,
                                        stride=1,
                                        padding=2)

        # Lout_cnvtrns ​=(Lin​−1)×stride−2×padding+dilation×(kernel_size−1)+output_padding+1
        # L1 = (9-1)*1 -2*0 +1*(2-1)+1 = 9+1+1 = 11
        # L2 = (11-1)*1 -2*0 +1*(3-1)+1 = 10+2+1 = 13
        # L3 = (13-1)*1 -2*3 +1*(3-1)+1 = 12-6+2+1 = 9

        self.lin2_2 = nn.Linear(9, 9)
        self.cnn1a_2 = nn.ConvTranspose1d(
            in_channels=1,
            out_channels=1,
            kernel_size=2,  # Kernel_size can't be greater than input size
            stride=1)
        self.cnn1b_2 = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=2,
                                        stride=1)
        self.cnn2a_2 = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=3,
                                        stride=1)
        self.cnn2b_2 = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=3,
                                        stride=1)
        self.cnn3a_2 = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=2,
                                        stride=1,
                                        padding=2)
        self.cnn3b_2 = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=2,
                                        stride=1,
                                        padding=2)

        self.lin2_3 = nn.Linear(9, 9)
        self.cnn1a_3 = nn.ConvTranspose1d(
            in_channels=1,
            out_channels=1,
            kernel_size=2,  # Kernel_size can't be greater than input size
            stride=1)
        self.cnn1b_3 = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=2,
                                        stride=1)
        self.cnn2a_3 = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=3,
                                        stride=1)
        self.cnn2b_3 = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=3,
                                        stride=1)
        self.cnn3a_3 = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=2,
                                        stride=1,
                                        padding=2)
        self.cnn3b_3 = nn.ConvTranspose1d(in_channels=1,
                                        out_channels=1,
                                        kernel_size=2,
                                        stride=1,
                                        padding=2)

        self.lin1_a = nn.Linear(3, 1)
        self.lin1_b = nn.Linear(3, 1)
        self.lin1_c = nn.Linear(3, 1)

    def forward(self, x):
        x = self.lin2(x)  # Long 9
        x1 = self.cnn3a(self.cnn2a(self.cnn1a(x)))
        x2 = self.cnn3b(self.cnn2b(self.cnn1b(x)))  # Longitud és 9
        x2 = torch.sigmoid(x2)
        x = torch.mul(x1, x2) + x

        x = self.lin2_2(x)  # Long 9
        x1 = self.cnn3a_2(self.cnn2a_2(self.cnn1a_2(x)))
        x2 = self.cnn3b_2(self.cnn2b_2(self.cnn1b_2(x)))  # Longitud és 9
        x2 = torch.sigmoid(x2)
        x = torch.mul(x1, x2) + x

        x = self.lin2_3(x)  # Long 9
        x1 = self.cnn3a_3(self.cnn2a_3(self.cnn1a_3(x)))
        x2 = self.cnn3b_3(self.cnn2b_3(self.cnn1b_3(x)))  # Longitud és 9
        x2 = torch.sigmoid(x2)
        x = torch.mul(x1, x2) + x

        x = torch.chunk(x, 3, dim=2)  # 3x3
        x_a = self.lin1_a(x[0])
        x_b = self.lin1_b(x[1])
        x_c = self.lin1_c(x[2])  # Ara tinc 3x1

        # Els preparo per poder aplicar el BCELoss
        x = torch.cat((x_a, x_b, x_c), 2)  # 1x3
        x = torch.sigmoid(10 * x)
        return x


class AutoEncoder(nn.Module):
    def __init__(self):
        super(AutoEncoder, self).__init__()

        self.enc = Encoder()
        self.dec = Decoder()

        bits_entrada = 3
        bits_sortida = 9
        Rm = math.log2(2)
        Rc = bits_entrada * 1.0 / bits_sortida
        EbN0 = 10**(2 / 10)
        self.noiseSigma = math.sqrt(1 / (2 * Rm * Rc * EbN0))

    def forward(self, x):
        mig = self.enc(x)
        mig = 2 * torch.sigmoid(100 * mig) - 1
        # Aquest factor de 100 l'hauria de fer més flexible, hauria de calcular el mínim valor de x i fer-ho adaptable, ja que si tot ja són propers a 1 no cal aquest factor.
        mig_noise = mig + self.noiseSigma * torch.randn(mig.shape)
        x = self.dec(mig_noise.float())
        return x, mig, mig_noise


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')

    bits_entrada = 3
    bits_mig = 9

    torch.manual_seed(0)
    # mod_enc = Encoder(bits_entrada, 6)
    # mod_enc.to(device)
    # mod_dec = Decoder(6, num_etiquetes)
    # mod_dec.to(device)
    autoenc = AutoEncoder()
    num_etiquetes = 2**bits_entrada
    #optim_enc = torch.optim.SGD(mod_enc.parameters(), lr=1e-3, weight_decay=1e-5)
    #optim_dec = torch.optim.Adam(mod_dec.parameters(), lr=1e-3, weight_decay=1e-5)
    optim_auto = torch.optim.SGD(autoenc.parameters(),
                                 lr=1e-3,
                                 weight_decay=1e-5)

    criterion = BCELoss()
    loss_detall = BCELoss(reduction='none')

    ordre = list(range(num_etiquetes))
    # Per cada etiqueta he de fer el seu vector d'entrada
    entrada = []
    for element in ordre:
        st = ''
        # He d'afegir tants 0 al davant com longitud
        for k in format(element, "b").zfill(bits_entrada):
            st = st + k + ','
        entrada.append(eval('[' + st + ']'))
    entrada_torch = torch.Tensor(entrada) * 2 - 1
    entrada_torch = entrada_torch.view([num_etiquetes, 1, bits_entrada])

    f = open("debugant.log", "w")
    for epoch in range(167500):
        autoenc.train()

        if (epoch % 50) == 0:
            f.write("\n**************\nEpoch: " + str(epoch) +
                    "\n**************\n")

        # optim_enc.zero_grad()
        # optim_dec.zero_grad()
        optim_auto.zero_grad()

        seq_true = (entrada_torch.view(entrada_torch.shape[0],
                                       entrada_torch.shape[2]) + 1) / 2
        seq_pred, mig, mig_noise = autoenc(entrada_torch)

        loss = criterion(seq_pred.view(seq_true.shape[0], seq_true.shape[1]),
                         seq_true)
        # L'entrada al BCE és (N,*) on N és el tamany del batch

        loss.backward()
        # optim_enc.step()
        # optim_dec.step()
        optim_auto.step()

        if (epoch % 50) == 0:
            f.write("\n***\nSeq. entrada: " + str(seq_true.detach().numpy()))
            f.write("\nMig: " + str(mig.detach().numpy()))
            # f.write("\nMig Noise: " + str(mig_noise.detach().numpy()))
            f.write("\nSeq. predita: " + str(seq_pred.detach().numpy()))
            perdua = loss_detall(
                seq_pred.view(seq_true.shape[0], seq_true.shape[1]), seq_true)
            f.write("\nPèrdua: " + str(perdua))
            if device.type == 'cuda':
                f.write(torch.cuda.get_device_name(0))
                f.write('\nMemory Usage:')
                f.write('\nAllocated:' +
                        str(round(torch.cuda.memory_allocated(0) /
                                  1024**3, 1)) + 'GB')
                f.write('\nCached:   ' +
                        str(round(torch.cuda.memory_cached(0) / 1024**3, 1)) +
                        'GB')

            print("Epoch: {}, Loss: {}".format(str(epoch), str(loss)))
    f.close()

    torch.save(
        {
            'epoch': epoch,
            'model_state_dict': autoenc.state_dict(),
            'optimizer_state_dict': optim_auto.state_dict(),
            'loss': loss,
            'bits_entrada': bits_entrada,
            'bits_mig': bits_mig,
        }, "AE_3GCNN.pth")
